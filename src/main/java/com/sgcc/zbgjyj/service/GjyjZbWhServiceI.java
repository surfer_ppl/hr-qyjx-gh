package com.sgcc.zbgjyj.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.sgcc.zbgjyj.dto.GjyjCxDto;

public interface GjyjZbWhServiceI {

    /**
     * 根据条件查询关键业绩维护表格维护数据
     * 
     * @param paramMap
     * @return
     */
    public List<GjyjCxDto> selectGjyjDataGrid(Map<String, Object> paramMap);

    /**
     * 导入关键业绩指标
     * 
     * @param year
     * @param workbook
     * @return
     */
    public String importGjyjZb(int year, Workbook workbook);


    /**
     * 一键启用指标
     * @param ids 表格中所有行的ID
     * @return
     */
    public void updateZtToStart(Integer[] ids);

    /**
     * 一键停用指标
     * @param ids 表格中所有行的ID
     * @return
     */
    public void updateZtToStop (Integer[] ids);
}
