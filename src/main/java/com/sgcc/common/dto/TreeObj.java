package com.sgcc.common.dto;

import java.util.List;

/**
 * 树结构对象（LayUi）
 * @author Edward
 * @date 2021年5月23日
 */
public class TreeObj {
    //对应组织单元ID
    private Integer id;
    //节点名称
    private String title;
    //子节点数据
    private List<TreeObj> children;
    
    public TreeObj() {
        
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public List<TreeObj> getChildren() {
        return children;
    }
    public void setChildren(List<TreeObj> children) {
        this.children = children;
    }
    
}
