package com.sgcc.common.pojo;

/**
 * 员工基本信息表实体类
 * 
 * @author Edward
 * @date 2021年9月5日
 */
public class RsYgJbxx {
    
    // id
    private Integer id;
    // 组织机构ID
    private Integer zzjgId;
    // 单位ID
    private Integer dwId;
    // 部门ID
    private Integer bmId;
    // 班组ID
    private Integer bzId;
    // 工号
    private Long gh;
    // 姓名
    private String xm;
    // 岗位名称
    private String gwMc;

    public RsYgJbxx() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getZzjgId() {
        return zzjgId;
    }

    public void setZzjgId(Integer zzjgId) {
        this.zzjgId = zzjgId;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public Integer getBmId() {
        return bmId;
    }

    public void setBmId(Integer bmId) {
        this.bmId = bmId;
    }

    public Integer getBzId() {
        return bzId;
    }

    public void setBzId(Integer bzId) {
        this.bzId = bzId;
    }

    public Long getGh() {
        return gh;
    }

    public void setGh(Long gh) {
        this.gh = gh;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getGwMc() {
        return gwMc;
    }

    public void setGwMc(String gwMc) {
        this.gwMc = gwMc;
    }

}
