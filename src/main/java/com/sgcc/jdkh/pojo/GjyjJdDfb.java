package com.sgcc.jdkh.pojo;

/**
 * 关键业绩季度打分表实体类 （T_JX_GJYJ_JDDFB）
 * 
 * @author Edward
 * @date 2021年5月15日
 */
public class GjyjJdDfb {
    // id
    private Integer id;
    // 指标ID
    private Integer zbId;
    // 季度
    private Integer jd;
    // 加分
    private Double jf;
    // 加分理由
    private String jfLy;
    // 扣分
    private Double kf;
    // 扣分理由
    private String kfLy;
    // 得分
    private Double df;
    // 备注
    private String bz;

    public GjyjJdDfb() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getZbId() {
        return zbId;
    }

    public void setZbId(Integer zbId) {
        this.zbId = zbId;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

    public Double getJf() {
        return jf;
    }

    public void setJf(Double jf) {
        this.jf = jf;
    }

    public String getJfLy() {
        return jfLy;
    }

    public void setJfLy(String jfLy) {
        this.jfLy = jfLy;
    }

    public Double getKf() {
        return kf;
    }

    public void setKf(Double kf) {
        this.kf = kf;
    }

    public String getKfLy() {
        return kfLy;
    }

    public void setKfLy(String kfLy) {
        this.kfLy = kfLy;
    }

    public Double getDf() {
        return df;
    }

    public void setDf(Double df) {
        this.df = df;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

}
