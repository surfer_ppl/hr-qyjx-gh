package com.sgcc.yxygkh.bzzkh.service;

/**
 * 班组长考核相关Srtvice
 * @author Edward
 * @date 2021年9月6日
 */
public interface BzzKhServiceI {
    
    /**
     * 考核设置初始化考核周期和考核结果表
     * @param bzId
     * @param zqLx
     */
    public void initKhZqAndKhJg(Integer bzId,Integer zqLx);
}
