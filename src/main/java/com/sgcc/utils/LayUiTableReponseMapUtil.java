package com.sgcc.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 获取符合LayUiTable格式的Map数据工具类
 * 
 * @author Edward
 * @date 2021年5月30日
 */
public class LayUiTableReponseMapUtil {

    /**
     * 获取符合LayUiTable格式的Map数据, 其中msg默认为"",code，默认为"0"
     * 
     * @param object
     *            通常为查询到的List数据
     * @param total
     *            总条数，用于计算分页
     * @return
     */
    public static Map<String, Object> getLyUITableReonseMap(Object object, Long total) {

        Map<String, Object> layUiResultMap = new HashMap<>();

        // layUiTbale查询默认成功状态码
        layUiResultMap.put("code", "0");
        // lauUi分页插件，默认总条数
        if(null != total) {
            layUiResultMap.put("count", total);
        }
        // layUiTbale查询默认取数据属性字段
        layUiResultMap.put("data", object);
        // layUiTbale默认取返回消息属性字段
        layUiResultMap.put("msg", "");

        return layUiResultMap;

    }
}
