package com.sgcc.jcyb.pojo;

/**
 * 奖惩月报违反劳动纪律处理备案表（T_JX_JCYB_WFLDJL）
 *
 * @Author Edward
 * @Date 2022/3/19
 */
public class JcYbWfLdJl {
    /**
     * 表ID
     */
    private Integer id;
    /**
     * 员工ID
     */
    private Integer ygId;
    /**
     * 姓名
     */
    private String xm;
    /**
     * 员工性质
     */
    private String ygXz;
    /**
     * 组织机构ID
     */
    private Integer zzJgId;
    /**
     * 组织机构名称
     */
    private String zzJgMc;
    /**
     * 单位ID
     */
    private Integer dwId;
    /**
     * 单位名称
     */
    private String dwMc;
    /**
     * 部门ID
     */
    private Integer bmId;
    /**
     * 部门名称
     */
    private String bmMc;
    /**
     * 惩处细则编号
     */
    private String cfXzBh;
    /**
     * 惩处细则
     */
    private String cfXz;
    /**
     * 影响程度
     */
    private String yxCd;
    /**
     * 经济损失程度
     */
    private String jjSsCd;
    /**
     * 责任相关程度
     */
    private String zrXgCd;
    /**
     * 违反劳动纪律类型
     */
    private String ldJlLx;
    /**
     * 劳动纪律具体情况
     */
    private String ldJlQk;
    /**
     * 组织处理情况
     */
    private String zzCfQk;

    /**
     * 经济处罚情况
     */
    private String jjCfQk;
    /**
     * 其他处理情况
     */
    private String qtCfQk;

    /**
     * 处分书地址编码
     */
    private Integer cfsId;
    /**
     * 年度
     */
    private Integer year;
    /**
     * 月度
     */
    private Integer yd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYgId() {
        return ygId;
    }

    public void setYgId(Integer ygId) {
        this.ygId = ygId;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getYgXz() {
        return ygXz;
    }

    public void setYgXz(String ygXz) {
        this.ygXz = ygXz;
    }

    public Integer getZzJgId() {
        return zzJgId;
    }

    public void setZzJgId(Integer zzJgId) {
        this.zzJgId = zzJgId;
    }

    public String getZzJgMc() {
        return zzJgMc;
    }

    public void setZzJgMc(String zzJgMc) {
        this.zzJgMc = zzJgMc;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public Integer getBmId() {
        return bmId;
    }

    public void setBmId(Integer bmId) {
        this.bmId = bmId;
    }

    public String getBmMc() {
        return bmMc;
    }

    public void setBmMc(String bmMc) {
        this.bmMc = bmMc;
    }

    public String getCfXzBh() {
        return cfXzBh;
    }

    public void setCfXzBh(String cfXzBh) {
        this.cfXzBh = cfXzBh;
    }

    public String getCfXz() {
        return cfXz;
    }

    public void setCfXz(String cfXz) {
        this.cfXz = cfXz;
    }

    public String getYxCd() {
        return yxCd;
    }

    public void setYxCd(String yxCd) {
        this.yxCd = yxCd;
    }

    public String getJjSsCd() {
        return jjSsCd;
    }

    public void setJjSsCd(String jjSsCd) {
        this.jjSsCd = jjSsCd;
    }

    public String getZrXgCd() {
        return zrXgCd;
    }

    public void setZrXgCd(String zrXgCd) {
        this.zrXgCd = zrXgCd;
    }

    public String getLdJlLx() {
        return ldJlLx;
    }

    public void setLdJlLx(String ldJlLx) {
        this.ldJlLx = ldJlLx;
    }

    public String getLdJlQk() {
        return ldJlQk;
    }

    public void setLdJlQk(String ldJlQk) {
        this.ldJlQk = ldJlQk;
    }

    public String getZzCfQk() {
        return zzCfQk;
    }

    public void setZzCfQk(String zzCfQk) {
        this.zzCfQk = zzCfQk;
    }

    public String getJjCfQk() {
        return jjCfQk;
    }

    public void setJjCfQk(String jjCfQk) {
        this.jjCfQk = jjCfQk;
    }

    public String getQtCfQk() {
        return qtCfQk;
    }

    public void setQtCfQk(String qtCfQk) {
        this.qtCfQk = qtCfQk;
    }

    public Integer getCfsId() {
        return cfsId;
    }

    public void setCfsId(Integer cfsId) {
        this.cfsId = cfsId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getYd() {
        return yd;
    }

    public void setYd(Integer yd) {
        this.yd = yd;
    }
}
