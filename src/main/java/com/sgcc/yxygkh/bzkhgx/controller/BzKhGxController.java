package com.sgcc.yxygkh.bzkhgx.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgcc.yxygkh.bzkhgx.dto.ByXx;
import com.sgcc.yxygkh.bzkhgx.dto.BzXx;
import com.sgcc.yxygkh.bzkhgx.dto.GqXx;
import com.sgcc.yxygkh.bzkhgx.service.BzKhGxServiceI;

/**
 * 工区班组考核关系维护
 * 
 * @author Terrans Force
 * @date 2021/08/30
 */
@RestController
@RequestMapping("/bzKhGx")
public class BzKhGxController {

    @Autowired
    BzKhGxServiceI bzKhGxService;

    /**
     * 添加班组
     * 
     * @param gqId
     * @param bzidArr
     * @return
     */
    @RequestMapping(value = "/addBz", method = RequestMethod.POST)
    public Map<String, String> addBz(Integer gqId, Integer[] bzidArr) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            bzKhGxService.insertBz(gqId, bzidArr);
            msgMap.put("flag", "true");
            msgMap.put("msg", "新增成功");
        } catch (RuntimeException e) {
            msgMap.put("flag", "flase");
            msgMap.put("msg", "新增失败");
            e.printStackTrace();
        }
        return msgMap;
    }

    /**
     * 删除班组
     * 
     * @param bzidArr
     * @return
     */
    @RequestMapping(value = "/removeBz", method = RequestMethod.POST)
    public Map<String, String> removeBz(Integer[] bzidArr) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            bzKhGxService.deleteBz(bzidArr);
            msgMap.put("flag", "true");
            msgMap.put("msg", "删除成功");
        } catch (RuntimeException e) {
            msgMap.put("flag", "flase");
            msgMap.put("msg", "删除失败");
            e.printStackTrace();
        }
        return msgMap;
    }

    /**
     * 修改工区名称
     * 
     * @param gqId
     * @param gqMc
     * @return
     */
    @RequestMapping(value = "/modifyGqMc", method = RequestMethod.POST)
    public Map<String, String> modifyGqMc(Integer gqId, String gqMc) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            bzKhGxService.updateGqMc(gqId, gqMc);
            msgMap.put("flag", "true");
            msgMap.put("msg", "修改成功");
        } catch (RuntimeException e) {
            msgMap.put("flag", "flase");
            msgMap.put("msg", "修改失败");
            e.printStackTrace();
        }
        return msgMap;
    }

    /**
     * 添加自定义工区
     * 
     * @param dwId
     * @param gqMc
     * @return
     */
    @RequestMapping(value = "/addZdyGq", method = RequestMethod.POST)
    public Map<String, String> addZdyGq(Integer dwId, String gqMc) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            bzKhGxService.insertZdyGq(dwId, gqMc);
            msgMap.put("flag", "true");
            msgMap.put("msg", "添加成功");
        } catch (RuntimeException e) {
            msgMap.put("flag", "flase");
            msgMap.put("msg", "添加失败");
            e.printStackTrace();
        }
        return msgMap;
    }

    /**
     * 根据单位ID查询工区信息
     * 
     * @param dwId
     * @return
     */
    @RequestMapping(value = "/getGqXxByDwId", method = RequestMethod.POST)
    public List<GqXx> getGqXxByDwId(Integer dwId) {
        List<GqXx> gqXxs = null;
        try {
            gqXxs = bzKhGxService.selectGqXxs(dwId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gqXxs;
    }

    /**
     * 根据工区ID查询班组信息
     * 
     * @param dwId
     * @return
     */
    @RequestMapping(value = "/getBzXxByGqId", method = RequestMethod.POST)
    public List<BzXx> getBzXxByGqId(Integer gqId) {
        List<BzXx> bzXxs = null;
        try {
            bzXxs = bzKhGxService.selectBzXxs(gqId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bzXxs;
    }

    /**
     * 根据班组ID查询班员信息
     * 
     * @param dwId
     * @return
     */
    @RequestMapping(value = "/getByXxByBzId", method = RequestMethod.POST)
    public List<ByXx> getByXxByBzId(Integer bzId) {
        List<ByXx> byXxs = null;
        try {
            byXxs = bzKhGxService.selectByXxs(bzId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byXxs;

    }
}
