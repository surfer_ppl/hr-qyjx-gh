package com.sgcc.zbzygzkh.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sgcc.utils.PoiUtil;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.common.mapper.SequenceMapper;
import com.sgcc.common.pojo.Sequence;
import com.sgcc.constval.ConstVal;
import com.sgcc.dwlxwh.dto.GkbmDto;
import com.sgcc.dwlxwh.mapper.DwlxWhMapper;
import com.sgcc.dwlxwh.pojo.DwjcWh;
import com.sgcc.dwlxwh.pojo.Dwlx;
import com.sgcc.utils.SeqUtil;
import com.sgcc.zbzygzkh.dto.GkbmAndDwsDto;
import com.sgcc.zbzygzkh.dto.ZygzKhQzCxDto;
import com.sgcc.zbzygzkh.mapper.ZygzKhWhMapper;
import com.sgcc.zbzygzkh.pojo.ZygzKhGx;
import com.sgcc.zbzygzkh.pojo.ZygzKhQz;
import com.sgcc.zbzygzkh.pojo.ZygzKhZb;
import com.sgcc.zbzygzkh.pojo.ZygzKhZbPz;
import com.sgcc.zbzygzkh.service.ZygzKhWhServiceI;

@Service
public class ZygzKhWhServiceImpl implements ZygzKhWhServiceI {

    @Autowired
    private DwlxWhMapper dwlxWhMapper;

    @Autowired
    private ZygzKhWhMapper zygzKhWhMapper;

    @Autowired
    private SequenceMapper sequenceMapper;


    /**
     * 查询专业工作考核DataGrid表头数据
     */
    @Override
    public List<Map<String, Object>> selectZygzKhQzDataGridTh(int year, int jd) {
        // 查询专业工作考核涉及到所有归口部门 GKBMBH（归口部门编号）GKBMID(归口部门ID) GKBMMC(归口部门名称)
        List<Map<String, Object>> zygzKhGkbmList = zygzKhWhMapper.selectZygzKhGkbm(year, jd);

        return zygzKhGkbmList;
    }

    /**
     * 验证专业工作考核是否按照季度导入过
     */
    @Override
    public Map<String, Object> checkZygzKhQzIsJd(int year) {
        List<Map<String, Object>> isByJd = zygzKhWhMapper.selectZygzKhIsByJd(year);
        return isByJd.get(0);
    }

    /**
     * 查询专业工作考核权重DataGrid表格数据
     */
    @Override
    public List<Map<String, Object>> selectZygzKhQzDataGrid(int year, int jd) {

        // 查询专业工作考核涉及到的单位类型和对应单位
        List<Map<String, Object>> zygzKhDwlxAndDwList = zygzKhWhMapper.selectZygzKhDwlxAndDw(year, jd);
        List<Map<String, Object>> zygzKhDwlxAndDwListNew = new LinkedList<>();

        // 查询专业工作考核权限（归口部门按照code排序）
        List<ZygzKhQzCxDto> zygzKhQzList = zygzKhWhMapper.selectZygzKhQz(year, jd);

        // 循环zygzKhDwlxAndDwList
        for (Map<String, Object> zygzKhDwlxAndDwMap : zygzKhDwlxAndDwList) {
            String dwlxAndDw = (String) zygzKhDwlxAndDwMap.get("DWLXDW");

            // 用来计数
            int index = 0;
            // 循环zygzKhQzList
            for (ZygzKhQzCxDto zygzKhQzCxDto : zygzKhQzList) {
                String dwlxDw = zygzKhQzCxDto.getDwlxDw();
                if (dwlxAndDw.equals(dwlxDw)) {
                    index++;
                    // 归口部门编号
                    String gkbmBh = "GKBMBH" + index;
                    // 获取权重
                    String qz = zygzKhQzCxDto.getQz();
                    //把归口部门编号和其对应的函数放入Map中
                    zygzKhDwlxAndDwMap.put(gkbmBh, qz);
                }

            }
            zygzKhDwlxAndDwListNew.add(zygzKhDwlxAndDwMap);
        }

        return zygzKhDwlxAndDwListNew;
    }

    /**
     * 读取Excle数据插入权重表数据
     */
    @Override
    public String importZygzKhQz(int year, int jd, int ndOrJd, Workbook workbook) {

        // row.getCell(1).setCellType(CellType.STRING); 已经过时
        DataFormatter dataFormatter = new DataFormatter();

        String msg = null;
        // 获取sheet页
        Sheet sheet = workbook.getSheetAt(0);
        // 读取第一行表头判断归口部门名称的正确性
        Row row0 = sheet.getRow(0);
        if (null == row0) {
            msg = "请勿导入空文件";
            return msg;
        }
        // 判断第二行是
        Row row1 = sheet.getRow(1);
        if (null == row1) {
            msg = "文件第二行没有数据，请查看是否没有录入数据";
            return msg;
        }

        // 定义三个个Map分别存放单位类型和单位简称还有归口部门
        Map<String, Integer> dwlxMap = new HashMap<>();
        Map<String, Integer> dwjcMap = new HashMap<>();
        Map<String, Integer> gkbmMap = new HashMap<>();
        // 定义一个List，归口部门有24个，给个初始容量
        List<Integer> gkbmIdList = new ArrayList<>(30);

        List<Dwlx> dwlxList = dwlxWhMapper.selectDwlx(year);
        for (Dwlx dwlx : dwlxList) {
            dwlxMap.put(dwlx.getDwlxMc(), dwlx.getId());
        }
        List<DwjcWh> dwjcList = dwlxWhMapper.selectDwjc(year, null);
        for (DwjcWh dwjcWh : dwjcList) {
            dwjcMap.put(dwjcWh.getDwjc(), dwjcWh.getDwId());
        }
        List<GkbmDto> gkbmList = dwlxWhMapper.selectGkmb();
        for (GkbmDto gkbmDto : gkbmList) {
            gkbmMap.put(gkbmDto.getGkbmMc(), gkbmDto.getId());
        }

        // 验证归口部门名称
        short lastCellNum = row0.getLastCellNum(); // 获取的不是下标是真实的列数
        for (int i = 2; i < lastCellNum; i++) {
            String cellVal = row0.getCell(i).getStringCellValue();
            if (gkbmMap.containsKey(cellVal)) {
                gkbmIdList.add(gkbmMap.get(cellVal));
            } else {
                msg = "第" + (i + 1) + "列归口部门名称(" + cellVal + ")有错误";
                return msg;
            }
        }
        // 验证单位分类名称和单位名称
        int lastRowNum = sheet.getLastRowNum();// 获取sheet内容的最后一行(下标从0开始)
        for (int i = 1; i < lastRowNum + 1; i++) {
            Row row = sheet.getRow(i);
            String dwlxMc = row.getCell(0).getStringCellValue();
            if (!dwlxMap.containsKey(dwlxMc)) {
                msg = "第" + (i + 1) + "行单位分类名称  (" + dwlxMc + ")有错误";
                return msg;
            }
            String dwMc = row.getCell(1).getStringCellValue();
            if (!dwjcMap.containsKey(dwMc.trim())) {
                msg = "第" + (i + 1) + "行单位名称(" + dwMc + ")有错误";
                return msg;
            }
        }

        List<ZygzKhQz> zygzKhQzList = new LinkedList<>();

        // 序列
        Sequence sequence = new Sequence();

        // 根据模板从第二行读取
        for (int i = 1; i < lastRowNum + 1; i++) {
            Row row = sheet.getRow(i);
            String dwlxMc = row.getCell(0).getStringCellValue();
            String dwMc = row.getCell(1).getStringCellValue();

            Integer dwlxId = dwlxMap.get(dwlxMc);
            Integer dwId = dwjcMap.get(dwMc);

            // 循环归口部门所在列
            for (int j = 2; j < lastCellNum; j++) {
                //Integer id = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                Integer gkbmId = gkbmIdList.get(j - 2);

                ZygzKhQz zygzKhQz = new ZygzKhQz();

                //zygzKhQz.setId(id);
                zygzKhQz.setDwId(dwId);
                zygzKhQz.setDwlxId(dwlxId);
                zygzKhQz.setGkbmId(gkbmId);

                String qz = null;
                Cell cell = row.getCell(j);
                if (null != cell) {
                    qz = dataFormatter.formatCellValue(row.getCell(j));
                    //qz = row.getCell(j).getStringCellValue();
                    zygzKhQz.setQz(Double.valueOf(qz));
                }

                //判断是否按照季度导
                if (jd != 0) {
                    zygzKhQz.setJd(jd);
                    zygzKhQz.setZt(1);
                } else {
                    zygzKhQz.setZt(0);
                }

                zygzKhQzList.add(zygzKhQz);
            }
        }
        //先删除后插入
        int effectedRow = zygzKhWhMapper.deleteZygzKhQz(year, jd, ndOrJd);
        int effectedRow_1 = zygzKhWhMapper.insertZygzKhQz(zygzKhQzList, ndOrJd);
        if (effectedRow_1 < 1) {
            msg = "导入数据失败，请重试";
        }
        return msg;
    }

    /**
     * 插入专业工作考核指标配置
     */
    @Override
    public void insertZygzKhZbpz(ZygzKhZbPz zygzKhZbPz) {
        // 获取ID
        Sequence sequence = new Sequence();
        Integer id = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);

        zygzKhZbPz.setId(id);

        zygzKhWhMapper.insertZygzKhZbpz(zygzKhZbPz);
    }

    /**
     * 删除专业工作考核指标
     */
    @Override
    public void deleteZygzKhZbpz(int[] ids) {
        zygzKhWhMapper.deleteZygzKhZbpz(ids);
    }

    /**
     * 根据年度查询指标配置
     */
    @Override
    public List<ZygzKhZbPz> selectZbPz(int year) {
        List<ZygzKhZbPz> zbPzList = zygzKhWhMapper.selectZbPz(year);
        return zbPzList;
    }

    /**
     * 生成专业工作考核评价指标
     */
    @Override
    public void createZygzkhZbPj(int year) {
        // 定义序列
        Sequence sequence = new Sequence();

        // 查询指标配置
        List<ZygzKhZbPz> zbPzList = zygzKhWhMapper.selectZbPz(year);

        // 查询GKBMID-DWIDS
        List<GkbmAndDwsDto> gkbmAndDwsList = zygzKhWhMapper.selectGkbmAndDws(year);

        List<ZygzKhZb> zygzKhZbList = new LinkedList<>();

        List<ZygzKhGx> zygzKhGxList = new LinkedList<>();

        // 循环归口部门和单位list gkbmid-dwids
        for (GkbmAndDwsDto gkbmAndDwsDto : gkbmAndDwsList) {
            Integer gkbmId = gkbmAndDwsDto.getGkbmId();
            String dwIds = gkbmAndDwsDto.getDwIds();

            // 分割dwIds
            String[] dwidArr = dwIds.split(",");

            // 循环配置好的指标
            for (ZygzKhZbPz zygzKhZbPz : zbPzList) {

                Integer zbId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                Integer zbpzId = zygzKhZbPz.getId();

                ZygzKhZb zygzKhZb = new ZygzKhZb();
                zygzKhZb.setId(zbId);
                zygzKhZb.setGkbmId(gkbmId);
                zygzKhZb.setZbpzId(zbpzId);
                zygzKhZb.setYear(year);

                zygzKhZbList.add(zygzKhZb);

                // 循环归口部门所对应的Id
                for (int i = 0; i < dwidArr.length; i++) {
                    // 考核关系Id
                    Integer khgxId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                    String dwidStr = dwidArr[i];
                    ZygzKhGx zygzKhGx = new ZygzKhGx();

                    zygzKhGx.setId(khgxId);
                    zygzKhGx.setZbId(zbId);
                    zygzKhGx.setDwId(Integer.valueOf(dwidStr));

                    zygzKhGxList.add(zygzKhGx);
                }
            }

        }

        //导入前先执行删除(先删除考核关系，后删除指标表)
        zygzKhWhMapper.deleteZygzkhGx(year);
        zygzKhWhMapper.deleteZygzkhZb(year);

        zygzKhWhMapper.insertZygzkhZb(zygzKhZbList);
        zygzKhWhMapper.insertZygzkhGx(zygzKhGxList);
    }

    /**
     * 导出专业工作考核
     */
    @Override
    public Workbook exportZygzQz(Workbook workbook, Integer year, Integer jd) {
        Sheet sheet = workbook.getSheetAt(0);
        Row row0 = sheet.getRow(0);
        CellStyle mbCellStyle = row0.getCell(0).getCellStyle();

        CellStyle borderStyle = PoiUtil.getBorderStyle(workbook, 3);

        //查询归口部门表头
        List<Map<String, Object>> gkbmThs = selectZygzKhQzDataGridTh(year, jd);
        //查寻到权重数据
        List<Map<String, Object>> qzDatas = selectZygzKhQzDataGrid(year, jd);

        //从第二行开始循环
        for (int i = 0; i < gkbmThs.size(); i++) {
            Cell cell = row0.createCell(2 + i);
            cell.setCellValue(String.valueOf(gkbmThs.get(i).get("GKBMMC")));
            cell.setCellStyle(mbCellStyle);
        }
        for (int i = 0; i < qzDatas.size(); i++) {
            Map<String, Object> qzData = qzDatas.get(i);
            Row row = sheet.createRow(1 + i);

            //创建固定的两列
            Cell cell0 = row.createCell(0);
            Cell cell1 = row.createCell(1);
            cell0.setCellValue((String) qzData.get("dwlxMc"));
            cell0.setCellStyle(borderStyle);
            cell1.setCellValue((String) qzData.get("dwMc"));
            cell1.setCellStyle(borderStyle);

            for (int j = 0; j < gkbmThs.size(); j++) {
                Cell cellN = row.createCell(2 + j);
                Object gkbmbbh = gkbmThs.get(j).get("GKBMBH");
                cellN.setCellValue((String) qzData.get(gkbmbbh));
                cellN.setCellStyle(borderStyle);
            }
        }
        return workbook;
    }

}
