package com.sgcc.common.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.sgcc.common.mapper.LoginMapper;
import com.sgcc.common.pojo.YxygRolePz;
import com.sgcc.common.service.YgXxServiceI;
import com.sgcc.constval.ConstVal;

@Service
public class YgXxServiceImpl implements YgXxServiceI {

    @Autowired
    LoginMapper loginMapper;

    /**
     * 获取登录人角色信息(一线员工)
     */
    @Override
    public List<Map<String, String>> selectRoleXx(Integer loginerId) {
        //定义一个List存放，角色信息
        List<Map<String, String>> roleList = new ArrayList<>();
        // 先查询角色配置信息
        List<YxygRolePz> rolePzs = loginMapper.selectRolePz();
        
        // 封装成Map
        Map<String, String> roleMap = new HashMap<>();
        for (YxygRolePz yxygRolePz : rolePzs) {
            String roleCode = yxygRolePz.getRoleCode();
            String roleName = yxygRolePz.getRoleName();
            roleMap.put(roleCode, roleName);
        }
        //从考核关系表中查
        List<Map<String, Object>> khgxList = loginMapper.selectKhgx(loginerId);
        for (Map<String, Object> map : khgxList) {
            String dwId = map.get("DWID").toString();
            String gqId = map.get("GQID").toString();
            String bzId = map.get("BZID").toString();
            // 判断是否空字符串
            if (StringUtils.hasLength(dwId)) {
                Map<String, String> role = new HashMap<>();
                role.put("roleOrgId", dwId);
                role.put("roleCode", ConstVal.ROLE_DWGLY);
                role.put("roleName", roleMap.get(ConstVal.ROLE_DWGLY) + "（" + map.get("DWMC").toString() + "）");
                roleList.add(role);
            }
            if (StringUtils.hasLength(gqId)) {
                Map<String, String> role = new HashMap<>();
                role.put("roleOrgId", gqId);
                role.put("roleCode", ConstVal.ROLE_GQGLY);
                role.put("roleName", roleMap.get(ConstVal.ROLE_GQGLY) + "（" + map.get("GQMC").toString() + "）");
                roleList.add(role);
            }
            if (StringUtils.hasLength(bzId)) {
                Map<String, String> role = new HashMap<>();
                role.put("roleOrgId", bzId);
                role.put("roleCode", ConstVal.ROLE_BZZ);
                role.put("roleName", roleMap.get(ConstVal.ROLE_BZZ) + "（" + map.get("BZMC").toString() + "）");
                roleList.add(role);
            }
        }
        // 如果没有角色，就去查是不是不同员工
        if(roleList.isEmpty()) {
            
        }

        return roleList;
    }

}
