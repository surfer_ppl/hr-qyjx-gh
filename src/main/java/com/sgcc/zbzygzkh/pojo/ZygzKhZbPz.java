package com.sgcc.zbzygzkh.pojo;

/**
 * 专业工作考核指标配置实体类（T_JX_ZYGZ_ZBPZ）
 * 
 * @author Edward
 *
 */
public class ZygzKhZbPz {
    // ID
    private Integer id;
    // 指标名称
    private String zbMc;
    // 加分或扣分项
    private Integer jfOrKf;
    // 加分上线
    private Integer jfSx;
    // 年度
    private Integer year;

    public ZygzKhZbPz() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public Integer getJfOrKf() {
        return jfOrKf;
    }

    public void setJfOrKf(Integer jfOrKf) {
        this.jfOrKf = jfOrKf;
    }

    public Integer getJfSx() {
        return jfSx;
    }

    public void setJfSx(Integer jfSx) {
        this.jfSx = jfSx;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

}
