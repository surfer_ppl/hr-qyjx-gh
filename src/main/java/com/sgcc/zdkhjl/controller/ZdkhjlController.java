package com.sgcc.zdkhjl.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.common.pojo.CommonCode;
import com.sgcc.constval.ConstVal;
import com.sgcc.utils.LayUiTableReponseMapUtil;
import com.sgcc.zdkhjl.dto.ZdkhjlDwMxDto;
import com.sgcc.zdkhjl.dto.ZdkhjlKbZsDto;
import com.sgcc.zdkhjl.pojo.ZdkhKbFg;
import com.sgcc.zdkhjl.pojo.ZdkhjlDwMx;
import com.sgcc.zdkhjl.pojo.ZdkhjlZtXm;
import com.sgcc.zdkhjl.service.ZdkhjlServiceI;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 重大考核激励项Controller
 *
 * @author Edward
 * @date 2022-3-1
 */
@RestController
@RequestMapping("/zdkhjl")
public class ZdkhjlController {

    @Autowired
    ZdkhjlServiceI zdkhjlService;

    /**
     * 导入重大专项考核激励看板数据
     */
    @RequestMapping(value = "/importZdkhjlSm", method = RequestMethod.POST)
    public Map<String, String> importZdkhjlSm(Integer year, Integer jd, MultipartFile file) {
        Map<String, String> msgMap = new HashMap<>();

        Workbook workbook = null;
        // 读取上传的Excel文件
        try {
            // 获取文件流
            InputStream fileStream = file.getInputStream();
            workbook = new XSSFWorkbook(fileStream);
            String msg = zdkhjlService.importZdkhjl(year, jd, workbook);
            // 有值证明数据有误
            if (StringUtils.hasLength(msg)) {
                msgMap.put("flag", "false");
                msgMap.put("msg", msg);
            } else {
                msgMap.put("flag", "true");
                msgMap.put("msg", "导入成功");
            }
        } catch (IOException e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "导入异常请重试");
            return msgMap;
        }

        return msgMap;
    }

    /**
     * 查询表格内容(LAYUI)
     *
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/queryGridData", method = RequestMethod.POST)
    public Map<String, Object> queryGridData(Integer year, Integer jd, Integer page, Integer limit) {

        PageHelper.startPage(page, limit);

        List<ZdkhjlDwMxDto> gridData = zdkhjlService.selectGridData(year, jd);

        PageInfo<ZdkhjlDwMxDto> pageInfo = new PageInfo<ZdkhjlDwMxDto>(gridData);
        long total = pageInfo.getTotal();
        Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(gridData, total);

        return resultMap;
    }

    /**
     * 查询看板展示
     *
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/queryKbzsData", method = RequestMethod.POST)
    public List<ZdkhjlKbZsDto> queryKbzsData(Integer year, Integer jd) {
        List<ZdkhjlKbZsDto> kbZsData = zdkhjlService.selectKbZsData(year, jd);
        return kbZsData;
    }

    /**
     * 查询专题明细根据ID
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/queryZtXmById", method = RequestMethod.POST)
    public ZdkhjlZtXm queryZtXmById(Integer id) {
        ZdkhjlZtXm zdkhjlZtXm = zdkhjlService.selectZtXmById(id);
        return zdkhjlZtXm;
    }

    /**
     * 查询单位明细根据ID
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/queryDwMxById", method = RequestMethod.POST)
    public ZdkhjlDwMx queryDwMxById(Integer id) {
        ZdkhjlDwMx zdkhjlDwMx = zdkhjlService.selectDwMxById(id);
        return zdkhjlDwMx;
    }

    /**
     * 查询已定看板风格
     *
     * @return
     */
    @RequestMapping(value = "/queryZdkhKbFg", method = RequestMethod.POST)
    public ZdkhKbFg queryZdkhKbFg() {
        ZdkhKbFg zdkhKbFg = zdkhjlService.selectFg();
        return zdkhKbFg;
    }

    /**
     * 保存看板风格
     * @param styleCode
     * @return
     */
    @RequestMapping(value = "/saveStyleCode", method = RequestMethod.POST)
    public Map<String, String> saveStyleCode(String styleCode) {
        Map<String, String> msgMap = new HashMap<>();
        try {
            zdkhjlService.updateKbFg(styleCode);
            msgMap.put("flag", "true");
            msgMap.put("msg", "保存成功");
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "保存失败");
            return msgMap;
        }
        return msgMap;
    }


}
