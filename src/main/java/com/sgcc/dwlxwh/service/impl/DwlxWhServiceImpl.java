package com.sgcc.dwlxwh.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.common.mapper.SequenceMapper;
import com.sgcc.common.pojo.Sequence;
import com.sgcc.constval.ConstVal;
import com.sgcc.dwlxwh.dto.DwjcWhDto;
import com.sgcc.dwlxwh.dto.DwlxCxDto;
import com.sgcc.dwlxwh.dto.GkbmDto;
import com.sgcc.dwlxwh.mapper.DwlxWhMapper;
import com.sgcc.dwlxwh.pojo.DwjcWh;
import com.sgcc.dwlxwh.pojo.Dwlx;
import com.sgcc.dwlxwh.service.DwlxWhServiceI;
import com.sgcc.utils.PoiUtil;
import com.sgcc.utils.SeqUtil;

@Service
public class DwlxWhServiceImpl implements DwlxWhServiceI {

    @Autowired
    private DwlxWhMapper dwlxWhMapper;
    
    @Autowired
    private SequenceMapper sequenceMapper;

    /**
     * 查询单位类型首页表格数据
     */
    @Override
    public List<DwlxCxDto> selectDwlxDataGrid(Integer year, Integer dwlxId) {
        List<DwlxCxDto> dataGrid = dwlxWhMapper.selectDwlxDataGrid(year, dwlxId);
        return dataGrid;
    }

    /**
     * 查询查看数据和单位类型维护
     */
    @Override
    public List<DwjcWhDto> selectDwjcWhDto(Integer year, Integer dwlxId) {
        List<DwjcWhDto> dWjcWhDtoList = dwlxWhMapper.selectDWjcWhDto(year, dwlxId);
        return dWjcWhDtoList;
    }

    /**
     * 导出单位类型维护数据
     * 
     * @param year
     * @return
     */
    public Workbook exportDwjcWh(Workbook workbook, Integer year) {
        List<DwjcWhDto> dWjcWhDtoList = dwlxWhMapper.selectDWjcWhDto(year, null);

        Sheet sheet = workbook.getSheetAt(0);

        // 获取单元格格式
        CellStyle cellStyle1 = PoiUtil.getBorderStyle(workbook, 1);

        CellStyle cellStyle3 = PoiUtil.getBorderStyle(workbook, 3);

        // 用于存储合并后新单元格的起始列
        int startIndex = 2;
        // 用于存储合并后新单元格的结束列
        int endIndex = 2;

        String dwlxMcTmp = "";
        for (int i = 0; i < dWjcWhDtoList.size(); i++) {
            DwjcWhDto dwjcWhDto = dWjcWhDtoList.get(i);

            // 获取单位类型名称
            String dwlxMc = dwjcWhDto.getDwlxMc();

            // 替换日期
            if (i == 0) {
                Row row = sheet.getRow(i);
                Cell cell = row.getCell(0);
                String cellVal = cell.getStringCellValue();
                String newCellVal = cellVal.replace("YYYY", String.valueOf(year));
                cell.setCellValue(newCellVal);
                dwlxMcTmp = dwlxMc;
            }

            Row row = sheet.createRow(i + 2);
            row.createCell(0).setCellValue(dwlxMc);
            row.createCell(1).setCellValue(dwjcWhDto.getDwQc());
            row.createCell(2).setCellValue(dwjcWhDto.getDwJc());

            // 添加单元格样式
            short lastCellNum = row.getLastCellNum();// 真实列，非下标
            for (int j = 0; j < lastCellNum; j++) {
                if (0 == j) {
                    row.getCell(j).setCellStyle(cellStyle3);
                } else {
                    row.getCell(j).setCellStyle(cellStyle1);
                }
            }

            // 根据单位名称进行判断，合并单位类型单元格
            if (i > 0) {
                if (dwlxMcTmp.equals(dwlxMc)) {
                    endIndex++;
                    // flag = false;
                } else {
                    dwlxMcTmp = dwlxMc;
                    PoiUtil.addMergedRegion(sheet, startIndex, endIndex, 0, 0);
                    // 更改开始行
                    endIndex++;
                    startIndex = endIndex;
                }
            }

        }

        return workbook;
    }

    /**
     * 添加单位类型和关联单位
     */
    @Override
    public void addDwlxAndGldw(String dwlxMc, Integer[] dwids, Integer year) throws RuntimeException {

        Sequence sequence = new Sequence();

        int id = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);

        Dwlx dwlx = new Dwlx();
        dwlx.setId(id);
        dwlx.setDwlxMc(dwlxMc);
        dwlx.setYear(year);

        List<DwjcWh> dwjcWhList = new LinkedList<>();

        // 查询组织单元表里面的简称作为默认的简称
        List<Map<String, Object>> pzjcMapList = dwlxWhMapper.selectPzjc(dwids);

        Map<Integer, String> idAndPzjcMap = new HashedMap<>();

        for (Map<String, Object> pzjcMap : pzjcMapList) {
            idAndPzjcMap.put((Integer)pzjcMap.get("id"), (String)pzjcMap.get("pzjc"));
        }

        for (int i = 0; i < dwids.length; i++) {
            int dwid = dwids[i];

            DwjcWh dwjcWh = new DwjcWh();
            int whId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
            dwjcWh.setId(whId);
            dwjcWh.setDwlxId(id);
            dwjcWh.setDwId(dwid);
            dwjcWh.setDwjc(idAndPzjcMap.get(dwid));
            dwjcWh.setYear(year);
            dwjcWhList.add(dwjcWh);
        }

        dwlxWhMapper.insertDwlx(dwlx);
        dwlxWhMapper.insertDwjc(dwjcWhList);

    }

    /**
     * 删除单位类型和关联单位
     */
    @Override
    public void deleteDwlxAndGldw(List<Integer> dwlxIdList) {
        dwlxWhMapper.deleteDwlx(dwlxIdList);
        dwlxWhMapper.deleteDwjc(dwlxIdList);
    }

    /**
     * 删除单位类型和关联单位
     */
    @Override
    public void editDwlxAndGldw(Integer dwlxId, Integer[] dwids, Integer year) {

        // 序列对象
        Sequence sequence = new Sequence();

        List<Integer> dwlxIdList = new LinkedList<>();
        dwlxIdList.add(dwlxId);
        dwlxWhMapper.deleteDwjc(dwlxIdList);

        // 查询组织单元表里面的简称作为默认的简称
        List<Map<String, Object>> pzjcMapList = dwlxWhMapper.selectPzjc(dwids);

        Map<Integer, String> idAndPzjcMap = new HashedMap<>();

        for (Map<String, Object> pzjcMap : pzjcMapList) {
            idAndPzjcMap.put((Integer)pzjcMap.get("id"), (String)pzjcMap.get("pzjc"));
        }

        List<DwjcWh> dwjcWhList = new LinkedList<>();

        for (int i = 0; i < dwids.length; i++) {
            int dwid = dwids[i];

            DwjcWh dwjcWh = new DwjcWh();
            int whId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
            dwjcWh.setId(whId);
            dwjcWh.setDwlxId(dwlxId);
            dwjcWh.setDwId(dwid);
            dwjcWh.setDwjc(idAndPzjcMap.get(dwid));
            dwjcWh.setYear(year);
            dwjcWhList.add(dwjcWh);
        }
        
        dwlxWhMapper.insertDwjc(dwjcWhList);

    }

    /**
     * 维护和修改单位简称
     */
    @Override
    public int updateDwjc(Integer[] ids, String[] dwjcs) {

        List<DwjcWh> dwjcWhList = new LinkedList<>();
        for (int i = 0; i < ids.length; i++) {
            DwjcWh dwjcWh = new DwjcWh();

            int id = ids[i];
            String dwjc = dwjcs[i];

            dwjcWh.setId(id);
            dwjcWh.setDwjc(dwjc);

            dwjcWhList.add(dwjcWh);
        }

        int effectedRow = dwlxWhMapper.updateDwjc(dwjcWhList);
        return effectedRow;
    }

    /**
     * 删除单位简称数据
     */
    @Override
    public int deleteDwjc(List<Integer> dwlxIdList) {
        int effectedRow = dwlxWhMapper.deleteDwjc(dwlxIdList);
        return effectedRow;
    }

    /**
     * 删除单位类型数据
     */
    @Override
    public int deleteDwlx(List<Integer> dwlxIdList) {
        int effectedRow = dwlxWhMapper.deleteDwlx(dwlxIdList);
        return effectedRow;
    }

    /**
     * 启用/停用 单位类型，修改单位类型状态
     */
    @Override
    public int updateDwlx(Integer year, Integer zt) {
        int effectedRow = dwlxWhMapper.updateDwlx(year, zt);
        return effectedRow;
    }

    /**
     * 查询单位类型数据，单表
     */
    @Override
    public List<Dwlx> selectDwlx(Integer year) {

        List<Dwlx> dwlxList = dwlxWhMapper.selectDwlx(year);

        return dwlxList;
    }

    /**
     * 查询单位简称数据，单表
     */
    @Override
    public List<DwjcWh> selectDwjc(Integer year,Integer dwlxId) {

        List<DwjcWh> dwjcList = dwlxWhMapper.selectDwjc(year,dwlxId);

        return dwjcList;
    }

    /**
     * 查询归口部门名称和对应的ID
     */
    @Override
    public List<GkbmDto> selectGkmb() {
        List<GkbmDto> gkmbList = dwlxWhMapper.selectGkmb();
        return gkmbList;
    }

}
