package com.sgcc.utils;

/**
 * 折算方法工具类
 * 
 * @author Edward
 * @date 2021年8月5日
 */
public class ZsUtil {

    /**
     * 季度考核得分折算公式（按照部门折算）
     * 
     * @param dflSx
     *            得分率上限
     * @param dflXx
     *            得分率下线
     * @param zgf
     *            最高分 （dfh 中 最高的）
     * @param zdf
     *            最低分 （dfh 中 最低的）
     * @param khfzH
     *            考核分值总和
     * @param dfh
     *            得分总和
     * @return
     */
    public static double jdKhDfZs(double dflSx, double dflXx, double zgf, double zdf, double khfzH, double dfh) {
        double zsdf = 0;
        if (zgf > zdf) {
            zsdf = (dflXx + ((dflSx - dflXx) * (dfh - zdf)) / (zgf - zdf)) * khfzH;
        }else {
            if(zgf >(dflSx * khfzH)) {
               zsdf = dflSx * khfzH;
            }else if (zdf < (dflXx * khfzH )) {
                zsdf = dflXx * khfzH;
            }else {
                zsdf = zgf;
            }
        }
        return zsdf;
    }
}
