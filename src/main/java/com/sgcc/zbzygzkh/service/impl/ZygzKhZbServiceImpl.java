package com.sgcc.zbzygzkh.service.impl;

import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.utils.PoiUtil;
import com.sgcc.zbzygzkh.dto.ZygzKhZbCxDto;
import com.sgcc.zbzygzkh.mapper.ZygzKhZbMapper;
import com.sgcc.zbzygzkh.service.ZygzKhZbServiceI;

@Service
public class ZygzKhZbServiceImpl implements ZygzKhZbServiceI {

    @Autowired
    private ZygzKhZbMapper zygzKhZbMapper;

    /**
     * 查询专业工作考核详情表格数据
     */
    @Override
    public List<ZygzKhZbCxDto> selectZygzKhZbDataGrid(int year, String zbMc) {
        List<ZygzKhZbCxDto> zygzKhZbDataGrid = zygzKhZbMapper.selectZygzKhZbDataGrid(year, zbMc);
        return zygzKhZbDataGrid;
    }

    /**
     * 导出专业工作考核评价数据
     */
    @Override
    public Workbook exportZygzKhZb(Workbook workbook, Integer year) {

        // 查询数据
        List<ZygzKhZbCxDto> zygzKhZbDataGrid = zygzKhZbMapper.selectZygzKhZbDataGrid(year, null);

        Sheet sheet = workbook.getSheetAt(0);
        // 获取单元格格式
        CellStyle cellStyle = PoiUtil.getBorderStyle(workbook, 3);

        for (int i = 0; i < zygzKhZbDataGrid.size(); i++) {
            ZygzKhZbCxDto zygzKhZbCxDto = zygzKhZbDataGrid.get(i);

            Row row = sheet.createRow(i + 1);

            row.createCell(0).setCellValue(zygzKhZbCxDto.getXh());
            row.createCell(1).setCellValue(zygzKhZbCxDto.getGkbmMc());
            row.createCell(2).setCellValue(zygzKhZbCxDto.getZbMc());
            row.createCell(3).setCellValue(zygzKhZbCxDto.getDwlxMc());
            row.createCell(4).setCellValue(zygzKhZbCxDto.getDwMc());
            row.createCell(5).setCellValue(zygzKhZbCxDto.getYear());

            Integer jfOrKf = zygzKhZbCxDto.getJfOrKf();
            String jfOrKfStr = null;
            if (jfOrKf == 0) {
                jfOrKfStr = "扣分";
            } else {
                jfOrKfStr = "加分";
            }
            row.createCell(6).setCellValue(jfOrKfStr);

            Integer jfSx = zygzKhZbCxDto.getJfSx();
            String jfSxStr = null;
            if (null == jfSx) {
                jfSxStr = "";
            } else {
                jfSxStr = String.valueOf(jfSx);
            }
            row.createCell(7).setCellValue(jfSxStr);

            // 循环设置CellStyle
            for (int j = 0; j < row.getLastCellNum(); j++) {
                row.getCell(j).setCellStyle(cellStyle);
            }

        }
        return workbook;

    }
}
