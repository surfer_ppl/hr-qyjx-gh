package com.sgcc.common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.common.dto.ElmTreeObj;
import com.sgcc.common.dto.TreeObj;
import com.sgcc.common.mapper.TreeMapper;
import com.sgcc.common.pojo.RsYgJbxx;
import com.sgcc.common.service.TreeServiceI;

@Service
public class TreeServiceImpl implements TreeServiceI {

    @Autowired
    private TreeMapper treeMapper;

    /**
     * 查询单位类型树
     */
    @Override
    public List<TreeObj> selectDwTree(Integer orgId) {

        List<TreeObj> treeObjList = new ArrayList<>();
        // 查询
        List<Map<String, Object>> dwTreeMapList = treeMapper.selectDwTree(orgId);

        for (Map<String, Object> dwTreeMap : dwTreeMapList) {

            Integer pid = (Integer) dwTreeMap.get("parentId");

            // 父ID为空，循环去查子
            if (null == pid || "".equals(pid)) {
                treeObjList.add(findChildren(dwTreeMap, dwTreeMapList));
            }
        }

        return treeObjList;
    }

    /**
     * 查询归口部门树
     */
    @Override
    public List<TreeObj> selectGkbmTree(Integer orgId) {
        List<TreeObj> treeObjList = new ArrayList<>();
        // 查询
        List<Map<String, Object>> dwTreeMapList = treeMapper.selectGkbmTree(orgId);

        for (Map<String, Object> dwTreeMap : dwTreeMapList) {

            Integer pid = (Integer) dwTreeMap.get("parentId");

            // 父ID为空，循环去查子
            if (null == pid || "".equals(pid)) {
                treeObjList.add(findChildren(dwTreeMap, dwTreeMapList));
            }
        }

        return treeObjList;
    }

    // 递归查询子部门
    private TreeObj findChildren(Map<String, Object> dwTreeMap, List<Map<String, Object>> dwTreeMapList) {

        TreeObj treeObj = new TreeObj();

        treeObj.setId((Integer) dwTreeMap.get("id"));
        treeObj.setTitle((String) dwTreeMap.get("deptName"));

        for (Map<String, Object> dwTreeMap1 : dwTreeMapList) {
            // 判断pid和当前id相等的单位
            if (String.valueOf(dwTreeMap.get("id")).equals(String.valueOf(dwTreeMap1.get("parentId")))) {
                if (treeObj.getChildren() == null) {
                    treeObj.setChildren(new ArrayList<>());
                }
                treeObj.getChildren().add(findChildren(dwTreeMap1, dwTreeMapList));
            }
        }
        return treeObj;

    }

    /**
     * 查询ElementUi树结构根节点数据（懒加载）
     */
    @Override
    public List<ElmTreeObj> selectElmZzRootTreeLazy(Integer spOrgId) {
        List<ElmTreeObj> rootTreeObjs = treeMapper.selectElmZzRootTreeLazy(spOrgId);
        for (ElmTreeObj elmTreeObj : rootTreeObjs) {
            if (elmTreeObj.getSons() == 0) {
                elmTreeObj.setLeaf(true);
            }
        }
        return rootTreeObjs;
    }

    /**
     * 查询ElementUi树结构子节点数据（懒加载）
     */
    @Override
    public List<ElmTreeObj> selectElmChildTreeLazy(Integer parentId) {
        List<ElmTreeObj> childTreeObjs = treeMapper.selectElmChildTreeLazy(parentId);
        for (ElmTreeObj elmTreeObj : childTreeObjs) {
            if (elmTreeObj.getSons() == 0) {
                elmTreeObj.setLeaf(true);
            }
        }
        return childTreeObjs;
    }

    /**
     * 查询ElementUi树结构子节点数据（懒加载,右侧组织树）
     */
    @Override
    public List<ElmTreeObj> selectElmChildTreeLazy_Right(Integer parentId, String spOrgCode, Integer level) {
        List<ElmTreeObj> childTreeObjs = treeMapper.selectElmChildTreeLazy_Right(parentId, spOrgCode, level);
        for (ElmTreeObj elmTreeObj : childTreeObjs) {
            if (elmTreeObj.getSons() == 0) {
                elmTreeObj.setLeaf(true);
            }
        }
        return childTreeObjs;
    }

    /**
     * 查询饿了么根节点（单位类型）
     */
    public List<Map<String, Object>> selectElmRootNode_Dwlx(Integer year) {
        List<Map<String, Object>> elmRootNode_Dwlxs = treeMapper.selectElmRootNode_Dwlx(year);
        return elmRootNode_Dwlxs;
    }

    /**
     * 查询饿了么子节点（单位）
     */
    public List<Map<String, Object>> selectElmChildNode_Dw(Integer year, Integer dwlxId) {
        List<Map<String, Object>> elmChildNode_Dws = treeMapper.selectElmChildNode_Dw(year, dwlxId);

        //对leaf进行处理
        for (Map<String, Object> dwMap : elmChildNode_Dws) {
            String leafString = dwMap.get("leaf").toString();
            if ("1".equals(leafString)) {
                dwMap.put("leaf", true);
            } else {
                dwMap.put("leaf", false);
            }
        }

        return elmChildNode_Dws;
    }

    /**
     * 根据组织树查询对应的人
     */
    @Override
    public List<RsYgJbxx> selectRsYgJbxx(Integer zzId, String xm) {
        List<RsYgJbxx> ygJbxxList = treeMapper.selectRsYgJbxx(zzId, xm);
        return ygJbxxList;
    }

}
