package com.sgcc.yxygkh.bzzkh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sgcc.yxygkh.bzzkh.pojo.KhRy;

/**
 * 班组长考核mapper
 * @author Edward
 * @date 2021年9月6日
 */
@Mapper
public interface BzzKhMapper {
    /**
     * 根据组织机构ID，查询人
     * @param zzjgId
     * @return
     */
    public List<KhRy> selectKhRy(Integer zzjgId);
}
