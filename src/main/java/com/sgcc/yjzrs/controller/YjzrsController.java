package com.sgcc.yjzrs.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgcc.yjzrs.service.YjzrsServiceI;

/**
 * 业绩责任书
 * @author Edward
 *
 */
@RestController
@RequestMapping("/yjzrs")
public class YjzrsController {
    
    
    @Autowired
    YjzrsServiceI yjzrsService;
    
    /**
     * 发布业绩责任书
     * @param year
     * @return
     */
    @RequestMapping(value = "/publishYjzrs",method = RequestMethod.POST)
    public Map<String, String> publishYjzrs(Integer year){
        Map<String, String> msgMap = new HashMap<>();
        try {
            yjzrsService.createDfbData(year);
            msgMap.put("flag", "true");
            msgMap.put("msg", "发布成功");
            return msgMap;
            
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "发布失败,请重试");
            return msgMap;
        }
    }
}
