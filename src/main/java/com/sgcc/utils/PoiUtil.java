package com.sgcc.utils;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;

/**
 * Poi工具类，获取单元格样式
 * 
 * @author Edward
 *
 */
public class PoiUtil {
    /**
     * 获取单元格样式
     * 
     * @param workbook
     * @param flag
     *            0：默认上下左右黑框；1：上下左右黑框,上下居中；2：上下左右黑框，左右居中；3：上下左右黑框，上下左右居中
     * @return
     */
    public static CellStyle getBorderStyle(Workbook workbook, int flag) {
        CellStyle cellStyle = workbook.createCellStyle();

        cellStyle.setBorderBottom(BorderStyle.THIN); // 下边框
        cellStyle.setBorderLeft(BorderStyle.THIN);// 左边框
        cellStyle.setBorderTop(BorderStyle.THIN);// 上边框
        cellStyle.setBorderRight(BorderStyle.THIN);// 右边框

        if (0 == flag) {

        } else if (1 == flag) {
            // 上下居中
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        } else if (2 == flag) {
            // 左右居中
            cellStyle.setAlignment(HorizontalAlignment.CENTER);
        } else if (3 == flag) {
            // 上下左右居中
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            cellStyle.setAlignment(HorizontalAlignment.CENTER);
        }
        return cellStyle;
    }

    /**
     * 合并单元格并居中并设置边框样式
     * 
     * @param sheet
     * @param firstRow
     *            开始行（传入下标）
     * @param lastRow
     *            结束行（传入下标）
     * @param startCell
     *            开始列（传入下标）
     * @param lastCell
     *            结束列（传入下标）
     */
    public static void addMergedRegion(Sheet sheet, int firstRow, int lastRow, int firstCol, int lastCol) {

        CellRangeAddress region = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);

        sheet.addMergedRegion(region);

        RegionUtil.setBorderTop(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderBottom(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderLeft(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderRight(BorderStyle.THIN, region, sheet);
    }

    /**
     * 添加下拉框并且添加下拉框机制
     */

}
