package com.sgcc.yjzrs.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface YjzrsMapper {

    /**
     * 修改业绩责任书状态
     *
     * @param year
     * @return
     */
    public int updateYjzrsZt(int year);

    /**
     * 插入关键业绩指标打分表
     *
     * @param jdArr
     * @param year
     * @return
     */
    public int insertGjyjZbJdDfb(@Param("jdArr") int[] jdArr, @Param("year") int year);

    /**
     * 插入专业工作考核打分表
     *
     * @param jdArr
     * @param year
     * @return
     */
    public int insertZygzKhJdDfb(@Param("jdArr") int[] jdArr, @Param("year") int year);

    /**
     * 插入安全工作考核打分表
     *
     * @param jdArr
     * @param year
     * @return
     */
    public int insertAqgzKhJdDfb(@Param("jdArr") int[] jdArr, @Param("year") int year);

}
