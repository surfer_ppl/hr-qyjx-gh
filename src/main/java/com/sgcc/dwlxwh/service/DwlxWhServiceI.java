package com.sgcc.dwlxwh.service;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.sgcc.dwlxwh.dto.DwjcWhDto;
import com.sgcc.dwlxwh.dto.DwlxCxDto;
import com.sgcc.dwlxwh.dto.GkbmDto;
import com.sgcc.dwlxwh.pojo.DwjcWh;
import com.sgcc.dwlxwh.pojo.Dwlx;

public interface DwlxWhServiceI {

    /**
     * 查询单位类型首页表格数据
     * 
     * @return
     */
    public List<DwlxCxDto> selectDwlxDataGrid(Integer year, Integer dwlxId);

    /**
     * 查询查看数据和单位类型维护
     * 
     * @param year
     * @param dwlxId
     * @return
     */
    public List<DwjcWhDto> selectDwjcWhDto(Integer year, Integer dwlxId);

    /**
     * 导出单位类型维护数据
     * 
     * @param year
     * @return
     */
    public Workbook exportDwjcWh(Workbook workbook, Integer year);

    /**
     * 新增单位类型和关联单位
     * 
     * @param dwlxMc
     * @param dwids
     * @param year
     * @return
     */
    public void addDwlxAndGldw(String dwlxMc, Integer[] dwids, Integer year);

    /**
     * 删除单位类型和关联单位
     * 
     * @param dwlxIdList
     */
    public void deleteDwlxAndGldw(List<Integer> dwlxIdList);

    /**
     * 修改单位类型和关联单位
     * 
     * @param dwlxId
     * @param dwids
     * @param year
     * @return
     */
    public void editDwlxAndGldw(Integer dwlxId, Integer[] dwids, Integer year);

    /**
     * 修改单位简称维护数据
     * 
     * @param ids
     * @param dwjcs
     * @return
     */
    public int updateDwjc(Integer[] ids, String[] dwjcs);

    /**
     * 删除单位简称数据
     * 
     * @param dwlxIdList
     * @return
     */
    public int deleteDwjc(List<Integer> dwlxIdList);

    /**
     * 删除单位类型数据
     * 
     * @param dwlxIdList
     * @return
     */
    public int deleteDwlx(List<Integer> dwlxIdList);

    /**
     * 启用/停用 单位类型，修改单位类型状态
     * 
     * @param year
     * @return
     */
    public int updateDwlx(Integer year, Integer zt);

    /**
     * 根据年度查询单位类型数据 单表
     * 
     * @param year
     * @return
     */
    public List<Dwlx> selectDwlx(Integer year);

    /**
     * 根据年度从简称维护表查询 单表
     * 
     * @param year
     * @return
     */
    public List<DwjcWh> selectDwjc(Integer year,Integer dwlxId);

    /**
     * 查询归口部门id,pzjc
     * 
     * @return
     */
    public List<GkbmDto> selectGkmb();

}
