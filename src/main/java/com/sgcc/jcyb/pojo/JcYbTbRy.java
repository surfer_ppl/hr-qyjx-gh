package com.sgcc.jcyb.pojo;

/**
 * 奖惩月报填报人员信息实体类（）
 *
 * @Author Edward
 * @Date 2022/3/22
 */
public class JcYbTbRy {
    private Integer id;
    /**
     * 员工ID
     */
    private Integer ygId;
    /**
     * 姓名
     */
    private String xm;
    /**
     * 员工性质
     */
    private String ygXz;
    /**
     * 组织机构ID
     */
    private Integer zzJgId;
    /**
     * 组织机构名称
     */
    private String zzJgMc;
    /**
     * 单位ID
     */
    private Integer dwId;
    /**
     * 单位名称
     */
    private String dwMc;
    /**
     * 部门ID
     */
    private Integer bmId;
    /**
     * 部门名称
     */
    private String bmMc;
    /**
     * 年
     */
    private Integer year;
    /**
     * 月
     */
    private Integer yd;
    /**
     * 区别是纪律处分还是劳动处分纪律处理（1 FLAG_JLCF 2 FLAG_LDJL）
     */
    private String tabFlag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYgId() {
        return ygId;
    }

    public void setYgId(Integer ygId) {
        this.ygId = ygId;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getYgXz() {
        return ygXz;
    }

    public void setYgXz(String ygXz) {
        this.ygXz = ygXz;
    }

    public Integer getZzJgId() {
        return zzJgId;
    }

    public void setZzJgId(Integer zzJgId) {
        this.zzJgId = zzJgId;
    }

    public String getZzJgMc() {
        return zzJgMc;
    }

    public void setZzJgMc(String zzJgMc) {
        this.zzJgMc = zzJgMc;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public Integer getBmId() {
        return bmId;
    }

    public void setBmId(Integer bmId) {
        this.bmId = bmId;
    }

    public String getBmMc() {
        return bmMc;
    }

    public void setBmMc(String bmMc) {
        this.bmMc = bmMc;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getYd() {
        return yd;
    }

    public void setYd(Integer yd) {
        this.yd = yd;
    }

    public String getTabFlag() {
        return tabFlag;
    }

    public void setTabFlag(String tabFlag) {
        this.tabFlag = tabFlag;
    }
}
