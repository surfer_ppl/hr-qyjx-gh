package com.sgcc.dwlxwh.dto;

/**
 * 单位类型查询Dto
 * 
 * @author Edward
 *
 */
public class DwlxCxDto {

    // 单位类型ID
    private int id;
    // 年份
    private int year;
    // 单位类型名称
    private String dwlxMc;
    //关联单位ID
    private String gldwId;
    // 关联单位 group_concat
    private String gldwMc;
    // 关联单位简称 group_concat
    private String gldwJc;
    // 状态，0停用，1启用
    private String zt;

    public DwlxCxDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }
    
    public String getGldwId() {
        return gldwId;
    }

    public void setGldwId(String gldwId) {
        this.gldwId = gldwId;
    }

    public String getGldwMc() {
        return gldwMc;
    }

    public void setGldwMc(String gldwMc) {
        this.gldwMc = gldwMc;
    }

    public String getGldwJc() {
        return gldwJc;
    }

    public void setGldwJc(String gldwJc) {
        this.gldwJc = gldwJc;
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt;
    }

}
