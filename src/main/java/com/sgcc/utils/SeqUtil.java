package com.sgcc.utils;



import com.sgcc.common.mapper.SequenceMapper;
import com.sgcc.common.pojo.Sequence;

/**
 * 获取序列值工具
 * 
 * @author Edward
 *
 */
public class SeqUtil {

    /**
     * 获取下一个序列值
     * 
     * @param dwlxWhMapper
     *            单位类型Mapper
     * 
     * @param seqName
     *            序列名称，例如SEQ_QYJX
     * @return Map<String, Object> String值为SEQVAL
     */
    /*public static Integer getNextSeqVal(DwlxWhMapper dwlxWhMapper, String seqName) {
        Map<String, Object> seqMap = dwlxWhMapper.updateNextSeqVal(seqName);
        Integer seqVal = (Integer)seqMap.get("SEQVAL");
        return seqVal;
    }*/
    
    public static Integer getNextSeqVal(SequenceMapper sequenceMapper,Sequence sequence, String seqName) {
        sequence.setSeqName(seqName);
        sequenceMapper.updateNextSeqVal(sequence);
        return sequence.getSeqVal();
    }
}
