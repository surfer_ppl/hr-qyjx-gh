package com.sgcc.zbgjyj.pojo;

/**
 * 关键业绩指标单位类型指标实体类（T_JX_GJYJ_DWLXZB）
 * 
 * @author Edward
 *
 */
public class GjyjDwlxZb {
    // ID
    private Integer id;
    // 指标名称
    private String zbMc;
    // 归集指标ID
    private Integer gjzbId;
    // 单位类型ID
    private Integer dwlxId;
    // 归口部门名称
    private Integer gkbmId;
    // 计量单位
    private String jlDw;
    // 是否月度监控
    private String sfYdJk;
    // 是否季度差异
    private String sfJdCy;
    // 考核周期
    private String khZq;

    public GjyjDwlxZb() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public Integer getGjzbId() {
        return gjzbId;
    }

    public void setGjzbId(Integer gjzbId) {
        this.gjzbId = gjzbId;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public Integer getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(Integer gkbmId) {
        this.gkbmId = gkbmId;
    }

    public String getJlDw() {
        return jlDw;
    }

    public void setJlDw(String jlDw) {
        this.jlDw = jlDw;
    }

    public String getSfYdJk() {
        return sfYdJk;
    }

    public void setSfYdJk(String sfYdJk) {
        this.sfYdJk = sfYdJk;
    }

    public String getSfJdCy() {
        return sfJdCy;
    }

    public void setSfJdCy(String sfJdCy) {
        this.sfJdCy = sfJdCy;
    }

    public String getKhZq() {
        return khZq;
    }

    public void setKhZq(String khZq) {
        this.khZq = khZq;
    }

    @Override
    public String toString() {
        return "GjyjDwlxZb [id=" + id + ", zbMc=" + zbMc + ", gjzbId=" + gjzbId + ", dwlxId=" + dwlxId + ", gkbmId="
            + gkbmId + ", jlDw=" + jlDw + ", sfYdJk=" + sfYdJk + ", sfJdCy=" + sfJdCy + ", khZq=" + khZq + "]";
    }
    
    
    
}
