package com.sgcc.dwlxwh.dto;

/**
 * 查询归口部门的Dto 查询省公司本部旗下的部门就是归口部
 * 
 * @author Edward
 *
 */
public class GkbmDto {
    private int id;
    private String gkbmMc;

    public GkbmDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGkbmMc() {
        return gkbmMc;
    }

    public void setGkbmMc(String gkbmMc) {
        this.gkbmMc = gkbmMc;
    }

}
