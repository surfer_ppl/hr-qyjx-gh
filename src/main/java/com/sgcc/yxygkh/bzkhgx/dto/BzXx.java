package com.sgcc.yxygkh.bzkhgx.dto;
/**
 * 班组信息
 * @author Terrans Force
 * @date 2021/08/30
 */
public class BzXx {
    // ID
    private Integer id;
    // 班组名称
    private String bzMc;
    // 工区名称
    private String gqMc;
    // 班组长姓名
    private String bzzXm;
    // 人资编码
    private String rzBm;
    // 部门名称
    private String bmMc;

    public BzXx() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBzMc() {
        return bzMc;
    }

    public void setBzMc(String bzMc) {
        this.bzMc = bzMc;
    }

    public String getGqMc() {
        return gqMc;
    }

    public void setGqMc(String gqMc) {
        this.gqMc = gqMc;
    }

    public String getBzzXm() {
        return bzzXm;
    }

    public void setBzzXm(String bzzXm) {
        this.bzzXm = bzzXm;
    }

    public String getRzBm() {
        return rzBm;
    }

    public void setRzBm(String rzBm) {
        this.rzBm = rzBm;
    }

    public String getBmMc() {
        return bmMc;
    }

    public void setBmMc(String bmMc) {
        this.bmMc = bmMc;
    }

}
