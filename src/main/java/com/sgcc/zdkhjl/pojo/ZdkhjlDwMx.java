package com.sgcc.zdkhjl.pojo;
/**
 * 重大考核激励项——单位明细实体类（T_JX_ZXKHJL_DWMX）
 *
 * @author Edward
 * @date 2022-3-1
 */
public class ZdkhjlDwMx {
    /**
     * ID
     */
    private Integer id;
    /**
     * 专题项目表ID
     */
    private Integer ztXmId;
    /**
     * 档位
     */
    private String level;
    /**
     * 单位ID
     */
    private Integer dwId;
    /**
     * 评价描述
     */
    private String pjMs;
    /**
     * 评价明细
     */
    private String pjMx;

    private Integer year;

    private Integer jd;

    public ZdkhjlDwMx() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getZtXmId() {
        return ztXmId;
    }

    public void setZtXmId(Integer ztXmId) {
        this.ztXmId = ztXmId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public String getPjMs() {
        return pjMs;
    }

    public void setPjMs(String pjMs) {
        this.pjMs = pjMs;
    }

    public String getPjMx() {
        return pjMx;
    }

    public void setPjMx(String pjMx) {
        this.pjMx = pjMx;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }
}
