package com.sgcc.common.service.impl;

import java.util.List;
import java.util.Map;

import com.sgcc.common.dto.Loginer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.common.mapper.LoginMapper;
import com.sgcc.common.service.LoginServiceI;

@Service
public class LoginServiceImpl implements LoginServiceI {
    
    @Autowired
    LoginMapper loginMapper;


    /**
     * 查询登录人
     */
    public List<Loginer> selectLoginer(String account, String passWrod){
        List<Loginer> loginers = loginMapper.selectLoginer(account, passWrod);
        return loginers;
    }

    /**
     * 查询待办信息
     */
    @Override
    public List<Map<String, Object>> selectDbXx() {
        List<Map<String,Object>> dbxxs = loginMapper.selectDbXx();
        return dbxxs;
    }

}
