package com.sgcc.zbzygzkh.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.utils.LayUiTableReponseMapUtil;
import com.sgcc.zbzygzkh.dto.ZygzKhZbCxDto;
import com.sgcc.zbzygzkh.service.ZygzKhZbServiceI;

@RestController
@RequestMapping("/zygzKhZb")
public class ZygzKhZbController {

    @Autowired
    private ZygzKhZbServiceI zygzKhZbService;

    /**
     * 查询专业工作考核详情表格数据
     * 
     * @param year
     * @return
     */
    @RequestMapping(value = "/getZygzKhZbDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getZygzKhZbDataGrid(Integer year, String zbMc, Integer page, Integer limit) {

        PageHelper.startPage(page, limit);

        List<ZygzKhZbCxDto> zygzKhZbDataGrid = zygzKhZbService.selectZygzKhZbDataGrid(year, zbMc);

        PageInfo<ZygzKhZbCxDto> pageInfo = new PageInfo<ZygzKhZbCxDto>(zygzKhZbDataGrid);

        long total = pageInfo.getTotal();

        Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(zygzKhZbDataGrid, total);

        return resultMap;
    }
    
    /**
     * 导出专业工作考核评价指标
     * @param year
     * @param response
     */
    @RequestMapping(value = "/exportZygzKhZb", method = RequestMethod.GET)
    public void exportZygzKhZb(Integer year, HttpServletResponse response) {
        
        //读取模板
        String filePath = "com/sgcc/zbzygzkh/controller/export_zygzKhZb.xlsx";
        
        URL resource = this.getClass().getClassLoader().getResource(filePath);
        InputStream openStream = null;
        Workbook workbook = null;
        
        try {
            openStream = resource.openStream();
            workbook = new XSSFWorkbook(openStream);
            zygzKhZbService.exportZygzKhZb(workbook, year);

            // 开启响应输出流
            ServletOutputStream outputStream = response.getOutputStream();
            // 设置文件名
            String fileName = "附件:专业工作考核指标.xlsx";

            // 设置mime类型
            String mimetype = "application/x-msdownload";
            response.setContentType(mimetype);
            response.addHeader("Content-Disposition",
                "attachment;filename=" + new String(fileName.getBytes("utf-8"), "ISO_8859_1"));
            workbook.write(outputStream);
            // 推出输出流，并关闭相关流
            outputStream.flush();
            workbook.close();
            outputStream.close();
            openStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
