package com.sgcc.common.dto;

/**
 * ElementUI 树结构实体类
 * 
 * @author Edward
 * @date 2021年8月17日
 */
public class ElmTreeObj {
    // 组织ID
    private Integer id;
    // 组织名称
    private String name;
    // 子节点个数
    private Integer sons;
    // 是否是叶子节点
    private Boolean leaf;
    // 是否禁用
    private Boolean disabled;
    // 节点性质
    private Integer xz;
    // 图标属性
    private String icon;
    // 展示输入框（用于解决修改）
    private Boolean showInput;

    public ElmTreeObj() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSons() {
        return sons;
    }

    public void setSons(Integer sons) {
        this.sons = sons;
    }

    public Boolean getLeaf() {
        return leaf;
    }

    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Integer getXz() {
        return xz;
    }

    public void setXz(Integer xz) {
        this.xz = xz;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getShowInput() {
        return showInput;
    }

    public void setShowInput(Boolean showInput) {
        this.showInput = showInput;
    }

}
