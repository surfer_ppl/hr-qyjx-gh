package com.sgcc.zbzygzkh.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.sgcc.zbzygzkh.pojo.ZygzKhZbPz;

public interface ZygzKhWhServiceI {

    /**
     * 查询专业工作考核DataGrid表头数据
     *
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectZygzKhQzDataGridTh(int year, int jd);

    /**
     * 验证专业工作考核是否按照季度导入过
     *
     * @param year
     * @return
     */
    public Map<String, Object> checkZygzKhQzIsJd(int year);

    /**
     * 查询专业工作考核DataGrid表格数据
     *
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectZygzKhQzDataGrid(int year, int jd);

    /**
     * 读取Excle数据插入权重表数据
     *
     * @param year
     * @param jd
     * @param ndOrJd
     * @param workbook
     * @return
     */
    public String importZygzKhQz(int year, int jd, int ndOrJd, Workbook workbook);

    /**
     * 根据年度查询指标配置
     *
     * @param year
     * @return
     */
    public List<ZygzKhZbPz> selectZbPz(int year);

    /**
     * 插入专业工作考核指标配置
     *
     * @param zygzKhZbPz
     * @return
     */
    public void insertZygzKhZbpz(ZygzKhZbPz zygzKhZbPz);

    /**
     * 删除专业工作考核指标配置
     *
     * @param ids
     */
    public void deleteZygzKhZbpz(int[] ids);

    /**
     * 生成专业工作考核评价指标
     *
     * @param year
     */
    public void createZygzkhZbPj(int year);

    /**
     * 导出专业工作考核权重
     * @param workbook
     * @param year
     * @param jd
     * @return
     */
    public Workbook exportZygzQz(Workbook workbook, Integer year, Integer jd);
}
