package com.sgcc.yxygkh.bzzkh.pojo;

/**
 * 考核人员实体类（T_JX_YXYG_KHRY）
 * 
 * @author Edward
 * @date 2021年9月6日
 */
public class KhRy {
    // id
    private Integer id;
    // 组织机构ID
    private Integer zzjgId;
    // 单位ID
    private Integer dwId;
    // 部门ID
    private Integer bmId;
    // 班组ID
    private Integer bzId;
    // 工区ID
    private Integer gqId;
    // 工号
    private Long gh;
    // 姓名
    private String xm;
    // 岗位名称
    private String gwMc;
    // 人员类型
    private Integer ryLx;
    // 考核班组ID
    private Integer khBzId;
    // 是否考核
    private Integer sfkh;
    // 不考核原因
    private String bkhYy;

    public KhRy() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getZzjgId() {
        return zzjgId;
    }

    public void setZzjgId(Integer zzjgId) {
        this.zzjgId = zzjgId;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public Integer getBmId() {
        return bmId;
    }

    public void setBmId(Integer bmId) {
        this.bmId = bmId;
    }

    public Integer getBzId() {
        return bzId;
    }

    public void setBzId(Integer bzId) {
        this.bzId = bzId;
    }

    public Integer getGqId() {
        return gqId;
    }

    public void setGqId(Integer gqId) {
        this.gqId = gqId;
    }

    public Long getGh() {
        return gh;
    }

    public void setGh(Long gh) {
        this.gh = gh;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getGwMc() {
        return gwMc;
    }

    public void setGwMc(String gwMc) {
        this.gwMc = gwMc;
    }

    public Integer getRyLx() {
        return ryLx;
    }

    public void setRyLx(Integer ryLx) {
        this.ryLx = ryLx;
    }

    public Integer getKhBzId() {
        return khBzId;
    }

    public void setKhBzId(Integer khBzId) {
        this.khBzId = khBzId;
    }

    public Integer getSfkh() {
        return sfkh;
    }

    public void setSfkh(Integer sfkh) {
        this.sfkh = sfkh;
    }

    public String getBkhYy() {
        return bkhYy;
    }

    public void setBkhYy(String bkhYy) {
        this.bkhYy = bkhYy;
    }

}
