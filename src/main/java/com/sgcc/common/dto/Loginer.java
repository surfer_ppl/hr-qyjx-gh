package com.sgcc.common.dto;

/**
 * 登录人信息
 * 
 * @author Terrans Force
 * @date 2021/08/31
 */
public class Loginer {
    // 登录人ID
    private Integer id;
    // 登录人姓名
    private String xm;
    // 角色编码
    private String roleCode;
    // 角色名称
    private String roleMc;
    // 组织名称
    private String deptName;

    public Loginer() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleMc() {
        return roleMc;
    }

    public void setRoleMc(String roleMc) {
        this.roleMc = roleMc;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

}
