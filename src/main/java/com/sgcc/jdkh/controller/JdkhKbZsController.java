package com.sgcc.jdkh.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgcc.dwlxwh.pojo.DwjcWh;
import com.sgcc.dwlxwh.pojo.Dwlx;
import com.sgcc.dwlxwh.service.DwlxWhServiceI;
import com.sgcc.jdkh.pojo.JdKhJkZs;
import com.sgcc.jdkh.service.JdKhJkZsServiceI;
import com.sgcc.jdkh.service.JdkhJkKbServiceI;
import com.sgcc.utils.LayUiTableReponseMapUtil;

/**
 * 看板展示——季度考核
 * 
 * @author Edward
 * @date 2021年5月23日
 */

@RestController
@RequestMapping("/jdkhKbZs")
public class JdkhKbZsController {
    @Autowired
    JdKhJkZsServiceI jdKhJkZsService;

    @Autowired
    JdkhJkKbServiceI jdkhJkKbService;

    @Autowired
    DwlxWhServiceI dwlxWhService;

    /**
     * 根据年份获取单位类型
     * 
     * @return
     */
    @RequestMapping(value = "/getDwlx", method = RequestMethod.POST)
    public List<Dwlx> getDwLx(Integer year) {
        List<Dwlx> resultList = dwlxWhService.selectDwlx(year);
        return resultList;
    }

    /**
     * 根据年份和单位类型获取单位简称信息
     * 
     * @return
     */
    @RequestMapping(value = "/getDw", method = RequestMethod.POST)
    public List<DwjcWh> getDw(Integer year, Integer dwlxId) {
        List<DwjcWh> resultList = dwlxWhService.selectDwjc(year, dwlxId);
        return resultList;
    }

    /**
     * 查询季度考核看板展示首页数据（柱状图数据）
     * 
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/getJdkhKbFirstPage", method = RequestMethod.POST)
    public List<JdKhJkZs> getJdkhKbFirstPage(Integer year, Integer jd, Integer dwlxId) {

        List<JdKhJkZs> resultList = null;
        try {
            resultList = jdKhJkZsService.selectJdKhJkZs(year, jd, dwlxId, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }

    /**
     * 查询季度考核看板单位得分明细
     * 
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/getJdkhKbDfMx", method = RequestMethod.POST)
    public Map<String, Object> getJdkhKbDfMx(Integer year, Integer jd, Integer dwId, Integer zbLx) {
        
        Map<String, Object> resultMap = null;
        List<?> resultList = null;

        try {
            // 关键业绩
            if (1 == zbLx) {
                resultList = jdkhJkKbService.selectGjyjKbData(year, jd, dwId);
                // 专业工作
            } else if (2 == zbLx) {
                resultList = jdkhJkKbService.selectZygzKbData(year, jd, dwId);
                // 安全工作
            } else if (3 == zbLx) {
                resultList = jdKhJkZsService.selectJdKhJkZs(year, jd, null, dwId);
                List<Map<String, Object>> aqgzKhList = new ArrayList<>();
                if(!resultList.isEmpty()) {
                    Map<String, Object> aqgzKhMap = new HashedMap<>();
                    JdKhJkZs jdKhJkZs_dw = (JdKhJkZs) resultList.get(0);
                    
                    aqgzKhMap.put("djZdf", jdKhJkZs_dw.getAqgzZdf());
                    aqgzKhMap.put("ljZdf", jdKhJkZs_dw.getAqgzZdfLj());
                    String[] aqgzDfXxArr = jdKhJkZs_dw.getAqgzDfXx().split(",");
                    
                    for (int i = 0; i < aqgzDfXxArr.length; i++) {
                        aqgzKhMap.put("HXZBFZB_"+(i+1), aqgzDfXxArr[i]);
                    }
                    
                    aqgzKhList.add(aqgzKhMap);
                }
                resultList = aqgzKhList;
            }
            
            resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultMap;
    }

}
