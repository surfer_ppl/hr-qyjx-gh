package com.sgcc.zbaqgzkh.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.utils.LayUiTableReponseMapUtil;
import com.sgcc.zbaqgzkh.dto.AqgzKhCxDto;
import com.sgcc.zbaqgzkh.pojo.AqgzKhDwlx;
import com.sgcc.zbaqgzkh.service.AqgzKhWhServiceI;

@RestController
@RequestMapping("/aqgzKhWh")
public class AqgzKhWhController {

    @Autowired
    private AqgzKhWhServiceI aqgzKhWhService;

    /**
     * 获取安全工作考核表格数据
     * 
     * @param year
     * @param dwlxId
     * @return
     */
    @RequestMapping(value = "/getAqgzKhDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getAqgzKhDataGrid(Integer year, Integer dwlxId, Integer page, Integer limit) {

        PageHelper.startPage(page, limit);

        List<AqgzKhCxDto> resultList = null;
        try {
            resultList = aqgzKhWhService.selectAqgzKhDataGrid(year, dwlxId);

            PageInfo<AqgzKhCxDto> pageInfo = new PageInfo<AqgzKhCxDto>(resultList);
            long total = pageInfo.getTotal();

            Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, total);

            return resultMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 查询安全工作考核单位类型(layUi格式的)
     * 
     * @param year
     * @return
     */
    @RequestMapping(value = "/getAqgzKhDwlxs", method = RequestMethod.POST)
    public Map<String, Object> getAqgzKhDwlxs(Integer year, Integer page, Integer limit) {

        PageHelper.startPage(page, limit);

        List<AqgzKhDwlx> resultList = null;
        try {
            resultList = aqgzKhWhService.selectAqgzKhDwlxs(year);

            PageInfo<AqgzKhDwlx> pageInfo = new PageInfo<AqgzKhDwlx>(resultList);
            long total = pageInfo.getTotal();

            Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, total);

            return resultMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 查询安全工作考核单位类型(普通格式的)
     * 
     * @param year
     * @return
     */
    @RequestMapping(value = "/getAqgzKhDwlxs_1", method = RequestMethod.POST)
    public List<AqgzKhDwlx> getAqgzKhDwlxs_1(Integer year) {

        List<AqgzKhDwlx> resultList = null;
        try {
            resultList = aqgzKhWhService.selectAqgzKhDwlxs(year);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }

    /**
     * 新增安全工作考核单位类型
     * 
     * @param year
     * @param dwlxId
     * @return
     */
    @RequestMapping(value = "/addAqgzKhDwlx", method = RequestMethod.POST)
    public Map<String, String> addAqgzKhDwlx(Integer year, String dwlxMc) {
        Map<String, String> msgMap = new HashMap<>();

        AqgzKhDwlx aqgzKhDwlx = new AqgzKhDwlx();
        aqgzKhDwlx.setDwlxmc(dwlxMc);
        aqgzKhDwlx.setYear(year);

        try {
            aqgzKhWhService.insertAqgzKhDwlx(aqgzKhDwlx);
            msgMap.put("flag", "true");
            msgMap.put("msg", "新增成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "新增失败,请重试");
            return msgMap;
        }
    }

    /**
     * 新增安全工作考核单位类型和对应单位
     * 
     * @param year
     * @param dwlxId
     * @return
     */
    @RequestMapping(value = "/addAqgzKhDwlxGl", method = RequestMethod.POST)
    public Map<String, String> addAqgzKhDwlxGl(Integer dwlxId, int[] dwIds) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            aqgzKhWhService.insertAqgzKhDwlxGl(dwlxId, dwIds);
            msgMap.put("flag", "true");
            msgMap.put("msg", "新增成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "新增失败,请重试");
            return msgMap;
        }
    }

    /**
     * 查询安全工作考核单位类型和对应单位
     * 
     * @param year
     * @return
     */
    @RequestMapping(value = "/getAqgzKhDwlxGl", method = RequestMethod.POST)
    public Map<String, Object> getAqgzKhDwlxGl(Integer year, Integer dwlxId, String dwMc, Integer page, Integer limit) {

        PageHelper.startPage(page, limit);

        List<Map<String, Object>> resultList = null;
        try {
            resultList = aqgzKhWhService.selectAqgzKhDwlxGl(year, dwlxId, dwMc);

            PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(resultList);
            long total = pageInfo.getTotal();

            Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, total);

            return resultMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 导入安全工作考核相关数据数据
     * 
     * @param year
     * @param file
     * @return
     */
    @RequestMapping(value = "/importAqgzKhWh", method = RequestMethod.POST)
    public Map<String, String> importAqgzKhWh(Integer year, Integer dwlxId, MultipartFile file) {

        Map<String, String> msgMap = new HashMap<>();

        Workbook workbook = null;

        try {
            // 获取文件流
            InputStream fileStream = file.getInputStream();
            if (null == fileStream) {
                msgMap.put("flag", "false");
                msgMap.put("msg", "文件读取失败，请重试");
                return msgMap;
            }
            // 读取上传的Excel文件
            workbook = new XSSFWorkbook(fileStream);
            String msg = aqgzKhWhService.imoprtAqgzKhData(year, dwlxId, workbook);
            // 有值证明数据有误
            if (StringUtils.hasLength(msg)) {
                msgMap.put("flag", "false");
                msgMap.put("msg", msg);
            } else {
                msgMap.put("flag", "true");
                msgMap.put("msg", "导入成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "导入异常请重试");
            return msgMap;
        }
        return msgMap;
    }

    /**
     * 导出关键业绩指标模板
     * 
     * @param year
     */
    @RequestMapping(value = "/exportAqgzKhMuBan", method = RequestMethod.GET)
    public void exportAqgzKhMuBan(Integer year, HttpServletResponse response) {
        // 读取模板
        String filePath = "com/sgcc/zbaqgzkh/controller/aqgzkhMuBan.xlsx";
        URL resource = this.getClass().getClassLoader().getResource(filePath);
        InputStream openStream = null;
        try {
            openStream = resource.openStream();

            // 开启响应输出流
            ServletOutputStream outputStream = response.getOutputStream();
            // 设置文件名
            String fileName = "附件：" + year + "年安全工作考核导入模板.xlsx";

            // 设置mime类型
            String mimetype = "application/x-msdownload";
            response.setContentType(mimetype);
            response.addHeader("Content-Disposition",
                "attachment;filename=" + new String(fileName.getBytes("utf-8"), "ISO_8859_1"));

            // 使用原始方法
            // 缓冲流
            /* int len = 0;
            byte[] flush = new byte[1024];
            while ((len = (openStream.read(flush))) != -1) {
                outputStream.write(flush, 0, len);
            }*/

            // 使用apach commons-io 下载
            IOUtils.copy(openStream, outputStream);

            // 关闭流
            outputStream.close();
            outputStream.flush();
            outputStream.close();

        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    /**
     * 查询安全工作考核父指标
     * 
     * @param year
     * @return
     */
    @RequestMapping(value = "/getAqgzKhFzb", method = RequestMethod.POST)
    public Map<String, Object> getAqgzKhFzb(Integer year) {

        List<Map<String,Object>> resultList = null;
        Map<String, Object> resultMap = null;
        try {
            resultList = aqgzKhWhService.selectAqgzKhFzb(year);

            resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultMap;

    }
}
