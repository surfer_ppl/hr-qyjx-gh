package com.sgcc.constval;

/**
 * 常量接口
 *
 * @Author Edward
 * @Date 2021/3/19
 */
public interface ConstVal {

    /**
     * 全员绩效SEQ名称"SEQ_QYJX"
     */
    public String QYJX_SEQ_NAME = "SEQ_QYJX";

    /**
     * 单位绩效管理员编码 HR_JX_DWLXGLY
     */
    public String ROLE_DWGLY = "HR_JX_DWJXGLY";

    /**
     * 工区绩效管理员编码 HR_JX_GQJXGLY
     */
    public String ROLE_GQGLY = "HR_JX_GQJXGLY";
    /**
     * 班组长编码 HR_JX_BZZ
     */
    public String ROLE_BZZ = "HR_JX_BZZ";
    /**
     * 部门绩效管理员编码 HR_JX_BMJXGLY
     */
    public String ROLE_BMGLY = "HR_JX_BMJXGLY";

    /**
     * 普通员工编码 HR_JX_PTYG
     */
    public String ROLE_PTYG = "HR_JX_PTYG";

    /**
     * 纪律处分备案(下拉框)
     */
    String JCYB_JLCF = "JCYB_JLCF";

    /**
     * 违反劳动纪律备案（下拉框）
     */
    String JCYB_LDJL = "JCYB_LDJL";

    /**
     * 影响程度
     */
    String JCYB_YXCD = "JCYB_YXCD";

    /**
     * 经济损失程度
     */
    String JCYB_JCSS = "JCYB_JJSSCD";

    /**
     * 责任相关程度
     */
    String JCYB_ZRXGCD = "JCYB_ZRXGCD";

    /**
     * 纪律处分备案(TAB)
     */
    String FLAG_JLCF = "FLAG_JLCF";

    /**
     * 违反劳动纪律备案（TAB）
     */
    String FLAG_LDJL = "FLAG_LDJL";

    /**
     * 重大专门项考核激励看板风格
     */
    String ZDKHJL_KBFG = "ZDKHJL_KBFG";


}
