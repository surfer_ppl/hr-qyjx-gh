package com.sgcc.zbzygzkh.pojo;

/**
 * 专业工作考核指标实体类（T_JX_ZYGZ_ZB）
 * 
 * @author Edward
 *
 */
public class ZygzKhZb {
    // ID
    private Integer id;
    // 指标名称
    private Integer zbpzId;
    // 归口部门ID
    private Integer gkbmId;
    // 年度
    private Integer year;

    public ZygzKhZb() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getZbpzId() {
        return zbpzId;
    }

    public void setZbpzId(Integer zbpzId) {
        this.zbpzId = zbpzId;
    }

    public Integer getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(Integer gkbmId) {
        this.gkbmId = gkbmId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

}
