package com.sgcc.zbgjyj.dto;

/**
 * 关键业绩查询Dto
 * 
 * @author Edward
 *
 */
public class GjyjCxDto {
    // 指标ID
    private Integer id;
    // 归集指标名称
    private String gjzbMc;
    // 指标名称
    private String zbMc;
    // 计量单位
    private String jlDw;
    // 是否月度监控
    private String sfydjk;
    // 是否季度差异
    private String sfjdcy;
    // 考核周期
    private String KhZq;
    // 考核分值
    private Double khFz;
    // 打分上限
    private Integer dfSx;
    // 单位名称
    private String dwMc;
    // 归口部门名称
    private String gkbmMc;
    // 状态
    private Integer zt;

    public GjyjCxDto() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGjzbMc() {
        return gjzbMc;
    }

    public void setGjzbMc(String gjzbMc) {
        this.gjzbMc = gjzbMc;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public String getJlDw() {
        return jlDw;
    }

    public void setJlDw(String jlDw) {
        this.jlDw = jlDw;
    }

    public String getSfydjk() {
        return sfydjk;
    }

    public void setSfydjk(String sfydjk) {
        this.sfydjk = sfydjk;
    }

    public String getSfjdcy() {
        return sfjdcy;
    }

    public void setSfjdcy(String sfjdcy) {
        this.sfjdcy = sfjdcy;
    }

    public String getKhZq() {
        return KhZq;
    }

    public void setKhZq(String khZq) {
        KhZq = khZq;
    }

    public Double getKhFz() {
        return khFz;
    }

    public void setKhFz(Double khFz) {
        this.khFz = khFz;
    }

    public Integer getDfSx() {
        return dfSx;
    }

    public void setDfSx(Integer dfSx) {
        this.dfSx = dfSx;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public String getGkbmMc() {
        return gkbmMc;
    }

    public void setGkbmMc(String gkbmMc) {
        this.gkbmMc = gkbmMc;
    }

    public Integer getZt() {
        return zt;
    }

    public void setZt(Integer zt) {
        this.zt = zt;
    }
}
