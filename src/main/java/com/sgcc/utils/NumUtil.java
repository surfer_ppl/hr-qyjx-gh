package com.sgcc.utils;

import java.util.regex.Pattern;

public class NumUtil {

    /**
     * 判断是否为数字
     * 
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }

    /**
     * 转换中文数字为阿拉伯数字
     * 
     * @param chineseNum
     * @return
     */
    public static int transNum(String chineseNum) {

        int num;

        switch (chineseNum) {
            case "一":
                num = 1;
                break;
            case "二":
                num = 2;
                break;
            case "三":
                num = 3;
                break;
            case "四":
                num = 4;
                break;
            case "五":
                num = 5;
                break;
            case "六":
                num = 6;
                break;
            case "七":
                num = 7;
                break;
            case "八":
                num = 8;
                break;
            case "九":
                num = 9;
                break;
            case "十":
                num = 10;
                break;

            default:
                num = 0;
                break;
        }
        return num;
    }
}
