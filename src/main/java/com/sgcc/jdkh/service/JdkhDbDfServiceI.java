package com.sgcc.jdkh.service;

import com.alibaba.fastjson.JSONArray;

public interface JdkhDbDfServiceI {
    
    /**
     * 保存待办打分数据——关键业绩指标
     * @param dbDfjSJsonArray
     */
    public void saveGjyjDbDf(JSONArray dbDfjSJsonArray);

    /**
     * 保存季度考核打分——专业工作考核
     * @param dbDfjSJsonArray
     */
    public void saveZygzDbDf(JSONArray dbDfjSJsonArray);
    
    /**
     * 保存季度考核打分——安全工作考核
     * @param dbDfjSJsonArray
     */
    public void saveAqgzDbDf(JSONArray dbDfjSJsonArray);
     
}
