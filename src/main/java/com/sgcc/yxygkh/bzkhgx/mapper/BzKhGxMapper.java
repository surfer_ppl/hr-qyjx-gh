package com.sgcc.yxygkh.bzkhgx.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.yxygkh.bzkhgx.dto.ByXx;
import com.sgcc.yxygkh.bzkhgx.dto.BzXx;
import com.sgcc.yxygkh.bzkhgx.dto.GqXx;

/**
 * 工区班组考核关系Mapper
 * 
 * @author Terrans Force
 * @date 2021/08/30
 */
@Mapper
public interface BzKhGxMapper {
    
    /**
     * 添加班组到工区
     * @param gqId
     * @param bzidArr
     * @return
     */
    public int insertBz(@Param("gqId") Integer gqId,@Param("bzidArr") Integer[] bzidArr);
    /**
     * 删除工区
     * @param bzidArr
     * @return
     */
    public int deleteBz(@Param("bzidArr") Integer[] bzidArr);
    
    /**
     * 修改工区名称
     * @return
     */
    public int updateGqMc(Integer gqId,String gqMc);
    
    /**
     * 添加自定义工区 
     * @return
     */
    public int insertZdyGq(Integer dwId,String gqMc);
    
    /**
     * 查询工区信息
     * @param dwId
     * @return
     */
    public List<GqXx> selectGqXxs(int dwId);
    
    /**
     * 查询班组信息
     * @param gqId
     * @return
     */
    public List<BzXx> selectBzXxs(int gqId);
    /**
     * 查询班员信息
     * @param bzId
     * @return
     */
    public List<ByXx> selectByXxs(int bzId);
}
