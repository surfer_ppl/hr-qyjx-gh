package com.sgcc.common.mapper;

import com.sgcc.common.pojo.CommonCode;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 公共代码查询
 * @Author Edward
 * @Date 2022/3/21
 */
@Mapper
public interface CommonCodeMapper {
    /**
     * 根据类型编码查询公共代码
     * @param codeTypeBm
     * @return
     */
    public List<CommonCode> selectCommonCode(String codeTypeBm);
}
