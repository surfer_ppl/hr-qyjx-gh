package com.sgcc.yxygkh.bzkhgx.dto;

/**
 * 班员信息
 * 
 * @author Terrans Force
 * @date 2021/08/30
 */
public class ByXx {
    
    // ID
    private Integer id;
    // 人资编码
    private Integer rzBm;
    // 班员姓名
    private String byXm;
    // 人员类型
    private Integer ryLx;
    // 部门名称
    private String bmMc;
    // 班组名称
    private String bzMc;
    // 考核班组名称
    private String khBzMc;
    // 是否考核
    private Integer sfKh;
    // 不考核原因
    private String bkhYy;
    // 考核人姓名
    private String khrXm;

    public ByXx() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRzBm() {
        return rzBm;
    }

    public void setRzBm(Integer rzBm) {
        this.rzBm = rzBm;
    }

    public String getByXm() {
        return byXm;
    }

    public void setByXm(String byXm) {
        this.byXm = byXm;
    }

    public Integer getRyLx() {
        return ryLx;
    }

    public void setRyLx(Integer ryLx) {
        this.ryLx = ryLx;
    }

    public String getBmMc() {
        return bmMc;
    }

    public void setBmMc(String bmMc) {
        this.bmMc = bmMc;
    }

    public String getBzMc() {
        return bzMc;
    }

    public void setBzMc(String bzMc) {
        this.bzMc = bzMc;
    }

    public String getKhBzMc() {
        return khBzMc;
    }

    public void setKhBzMc(String khBzMc) {
        this.khBzMc = khBzMc;
    }

    public Integer getSfKh() {
        return sfKh;
    }

    public void setSfKh(Integer sfKh) {
        this.sfKh = sfKh;
    }

    public String getBkhYy() {
        return bkhYy;
    }

    public void setBkhYy(String bkhYy) {
        this.bkhYy = bkhYy;
    }

    public String getKhrXm() {
        return khrXm;
    }

    public void setKhrXm(String khrXm) {
        this.khrXm = khrXm;
    }

}
