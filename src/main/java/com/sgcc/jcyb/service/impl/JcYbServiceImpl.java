package com.sgcc.jcyb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.sgcc.constval.ConstVal;
import com.sgcc.jcyb.mapper.JcYbMapper;
import com.sgcc.jcyb.pojo.JcYbCfZz;
import com.sgcc.jcyb.pojo.JcYbJlCf;
import com.sgcc.jcyb.pojo.JcYbTbRy;
import com.sgcc.jcyb.pojo.JcYbWfLdJl;
import com.sgcc.jcyb.service.JcYbServiceI;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Edward
 * @Date 2022/3/19
 */
@Service
public class JcYbServiceImpl implements JcYbServiceI {

    @Autowired
    JcYbMapper jcYbMapper;

    /**
     * 导入奖惩月报处罚准则
     */
    @Override
    public void importCfZz(Workbook workbook) {

        DataFormatter dataFormatter = new DataFormatter();
        Sheet sheet = workbook.getSheetAt(0);
        //根据模板从第二行开始循环
        int startRowNum = 1;
        int lastRowNum = sheet.getLastRowNum();
        List<JcYbCfZz> jcYbCfZzList = new ArrayList<JcYbCfZz>();
        for (int i = startRowNum; i < lastRowNum + 1; i++) {
            JcYbCfZz jcYbCfZz = new JcYbCfZz();
            Row row = sheet.getRow(i);
            String bh = dataFormatter.formatCellValue(row.getCell(0));
            String bhCn = row.getCell(1).getStringCellValue();
            String sm = row.getCell(2).getStringCellValue();
            jcYbCfZz.setBh(Integer.parseInt(bh));
            jcYbCfZz.setBhCn(bhCn);
            jcYbCfZz.setSm(sm);
            jcYbCfZzList.add(jcYbCfZz);
        }
        //先删除后插入
        jcYbMapper.deleteJcYbCfZz();
        jcYbMapper.insertJcYbCfZz(jcYbCfZzList);
    }

    /**
     * 查询处罚准则
     *
     * @return
     */
    public List<JcYbCfZz> selectJcYbCfZz() {
        List<JcYbCfZz> jcYbCfZzList = jcYbMapper.selectJcYbCfZz();
        return jcYbCfZzList;
    }

    /**
     * 查询填报人员信息
     * @param year
     * @param yd
     * @param tabFlag
     * @return
     */
    public List<JcYbTbRy> selectTbrys(Integer year,Integer yd,String tabFlag){
        List<JcYbTbRy> jcYbTbRyList = jcYbMapper.selectTbrys(year, yd, tabFlag);
        return  jcYbTbRyList;
    }

    /**
     * 查询纪律处分人员信息
     */
    public List<JcYbJlCf> selectJlcfs(Integer zzId, Integer year, Integer yd, String xm) {
        List<JcYbJlCf> jcYbJlCfs = jcYbMapper.selectJlcfs(zzId, year, yd, xm);
        return jcYbJlCfs;
    }

    /**
     * 查询违反劳动人员信息
     * @return
     */
    public List<JcYbWfLdJl> selectWfLdJls(Integer zzId, Integer year, Integer yd, String xm){
        List<JcYbWfLdJl> jcYbWfLdJlList = jcYbMapper.selectWfLdJls(zzId, year, yd, xm);
        return jcYbWfLdJlList;
    }

    /**
     * 保存填报人员数据
     * @param ygIds 字符串ids
     * @param tabLx TAB类型（FLAG_JLCF：纪律处分备案；FLAG_LDJL：违反劳动纪律备案；）
     * @param year 年
     * @param yd 月
     */
    public void saveTbryData(String ygIds, String tabLx,Integer year,Integer yd){
        String[] ygIdArr = ygIds.split(",");
        List<Integer> ygIdList = new ArrayList<>(ygIdArr.length);
        for (int i = 0; i < ygIdArr.length; i++) {
            String ydId = ygIdArr[i];
            if(StringUtils.hasLength(ydId)){
                ygIdList.add(Integer.parseInt(ydId));
            }
        }
        //保存纪律处分
        if (ConstVal.FLAG_JLCF.equals(tabLx)) {
            List<JcYbTbRy> jcYbTbRyList = jcYbMapper.selectJcybTbryFromJbxx(ygIdList,tabLx,year,yd);
            jcYbMapper.insertTbRy(jcYbTbRyList);
            jcYbMapper.insertJcYbJlCf(jcYbTbRyList);
            //保存违反劳动纪律
        } else if (ConstVal.FLAG_LDJL.equals(tabLx)) {
            List<JcYbTbRy> jcYbTbRyList = jcYbMapper.selectJcybTbryFromJbxx(ygIdList,tabLx,year,yd);
            jcYbMapper.insertTbRy(jcYbTbRyList);
            jcYbMapper.insertJcYbWfLdJl (jcYbTbRyList);
        }
    }

    /**
     * 保存修改的数据——纪律处分备案
     */
    public void saveJlCfData(List<JcYbJlCf> jcYbJlCfList) {
        jcYbMapper.updateJlCfData(jcYbJlCfList);
    }

    /**
     * 保存修改的数据——违反劳动纪律备案
     */
    public void saveWfLdData(List<JcYbWfLdJl> jcYbWfLdJlList) {
        jcYbMapper.updateWfLdData(jcYbWfLdJlList);
    }
}
