package com.sgcc.zbaqgzkh.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.sgcc.zbaqgzkh.dto.AqgzKhCxDto;
import com.sgcc.zbaqgzkh.pojo.AqgzKhDwlx;

/**
 * 安全工作考核维护页面相关
 * 
 * @author Edward
 *
 */
public interface AqgzKhWhServiceI {

    /**
     * 安全工作考核首页表格数据查询
     * 
     * @param year
     * @param dwlxId
     * @return
     */
    public List<AqgzKhCxDto> selectAqgzKhDataGrid(Integer year, Integer dwlxId);

    /**
     * 插入安全工作考核单位类型
     * 
     * @param aqgzKhDwlx
     */
    public void insertAqgzKhDwlx(AqgzKhDwlx aqgzKhDwlx);

    /**
     * 插入安全工作考核单位类型和对应单位
     * 
     * @param dwlxId
     * @param dwIds
     */
    public void insertAqgzKhDwlxGl(int dwlxId, int[] dwIds);

    /**
     * 查询安全工作考核单位类型和单位关联
     * 
     * @param year
     * @return
     */
    public List<Map<String, Object>> selectAqgzKhDwlxGl(Integer year, Integer dwlxId, String dwMc);

    /**
     * 读取Excle数据插入安全工作考核相关数据数据
     * 
     * @param year
     * @param workbook
     * @return 验证信息
     */
    public String imoprtAqgzKhData(int year, int dwlxId, Workbook workbook);

    /**
     * 查询安全工作考核单位类型
     * 
     * @param year
     * @return
     */
    public List<AqgzKhDwlx> selectAqgzKhDwlxs(Integer year);
    
    /**
     * 查询安全工作考核父指标
     * 
     * @param year
     * @return
     */
    public List<Map<String, Object>> selectAqgzKhFzb(Integer year);
}
