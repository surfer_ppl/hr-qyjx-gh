package com.sgcc.utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.apache.solr.common.params.SolrParams;

/**
 * solr操作工具类
 * 
 * @author 9527zhao
 * @date 2020/04/10
 */
public class SolrUtil {

    /**
     * 把对象集合加入到solr索引库
     * 
     * @param <T>
     * @param className
     *            全限定类名
     * @param fieldNames
     *            和solr域对应的字段名（xxx_xxx）,顺序要和实体类中属性顺序一直
     * @param dataList
     *            放入solr域的数据集合
     * @throws ClassNotFoundException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws IOException
     * @throws SolrServerException
     * @return
     * 
     */
    public static <T> void inputDataToSolr(HttpSolrClient solrClient, Class<T> tClass, String[] fieldNames,
        List<T> dataList) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException,
        SolrServerException, IOException {

        String[] fieldNamesNew = new String[fieldNames.length];
        // 对fieldNames进行处理，变成对应的驼峰形式
        for (int i = 0; i < fieldNames.length; i++) {
            String fieldName = fieldNames[i];
            // 根据“_”进行分割
            String[] singleStrArr = fieldName.split("_");
            StringBuffer fieldNameBuffer = new StringBuffer();
            if (singleStrArr.length > 1) {
                fieldNameBuffer.append(singleStrArr[0]);
                for (int j = 1; j < singleStrArr.length; j++) {
                    // 从第二个开始获取
                    String singleStr = singleStrArr[1];
                    String newStr = null;
                    if (singleStr.length() > 1) {
                        String upperCase = String.valueOf(singleStr.charAt(0)).toUpperCase();
                        // 从新拼装
                        newStr = upperCase + singleStr.substring(1, singleStr.length());
                    } else {
                        newStr = String.valueOf(singleStr.charAt(0)).toUpperCase();
                    }
                    fieldNameBuffer.append(newStr);
                }
            } else {
                fieldNameBuffer.append(singleStrArr[0]);
            }
            fieldNamesNew[i] = fieldNameBuffer.toString();
        }

        // 获取对象属性
        Field[] objFields = tClass.getDeclaredFields();
        // 生成新的fieldNamesNew，因为不是所有属性都要存入solr
        Field[] objFieldsNew = new Field[fieldNames.length];
        for (int i = 0; i < fieldNamesNew.length; i++) {
            String fieldName = fieldNamesNew[i];
            for (int j = 0; j < objFields.length; j++) {
                Field field = objFields[j];
                if (fieldName.equals(field.getName())) {
                    objFieldsNew[i] = objFields[j];
                }
            }
        }

        // 定义solrDocument集合
        List<SolrInputDocument> documents = new ArrayList<>();
        for (T data : dataList) {
            // 定义solrDocument
            SolrInputDocument document = new SolrInputDocument();
            // 循环要放入solr搜索域的字段
            for (int i = 0; i < fieldNames.length; i++) {
                Field field = objFieldsNew[i];
                field.setAccessible(true);
                document.addField(fieldNames[i], field.get(data));
            }
            documents.add(document);
        }
        solrClient.add(documents);
        solrClient.commit();
    }

    /**
     * 把单个对象加入到solr索引库
     * 
     * @param <T>
     * @param className
     *            全限定类名
     * @param fieldNames
     *            和solr域对应的字段名（xxx_xxx）,顺序要和实体类中属性顺序一直
     * @param dataList
     *            放入solr域的数据集合
     * @throws ClassNotFoundException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws IOException
     * @throws SolrServerException
     * @return
     * 
     */
    public static <T> void inputDataToSolr(HttpSolrClient solrClient, Class<T> tClass, String[] fieldNames, T data)
        throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, SolrServerException,
        IOException {

        String[] fieldNamesNew = new String[fieldNames.length];
        // 对fieldNames进行处理，变成对应的驼峰形式
        for (int i = 0; i < fieldNames.length; i++) {
            String fieldName = fieldNames[i];
            // 根据“_”进行分割
            String[] singleStrArr = fieldName.split("_");
            StringBuffer fieldNameBuffer = new StringBuffer();
            if (singleStrArr.length > 1) {
                fieldNameBuffer.append(singleStrArr[0]);
                for (int j = 1; j < singleStrArr.length; j++) {
                    // 从第二个开始获取
                    String singleStr = singleStrArr[j];
                    String newStr = null;
                    if (singleStr.length() > 1) {
                        String upperCase = String.valueOf(singleStr.charAt(0)).toUpperCase();
                        // 从新拼装
                        newStr = upperCase + singleStr.substring(1, singleStr.length());
                    } else {
                        newStr = String.valueOf(singleStr.charAt(0)).toUpperCase();
                    }
                    fieldNameBuffer.append(newStr);
                }
            } else {
                fieldNameBuffer.append(singleStrArr[0]);
            }
            fieldNamesNew[i] = fieldNameBuffer.toString();
        }

        // 获取对象属性
        Field[] objFields = tClass.getDeclaredFields();
        // 生成新的fieldNamesNew，因为不是所有属性都要存入solr
        Field[] objFieldsNew = new Field[fieldNames.length];
        for (int i = 0; i < fieldNamesNew.length; i++) {
            String fieldName = fieldNamesNew[i];
            for (int j = 0; j < objFields.length; j++) {
                Field field = objFields[j];
                if (fieldName.equals(field.getName())) {
                    objFieldsNew[i] = objFields[j];
                }
            }
        }
        // 定义solrDocument集合
        List<SolrInputDocument> documents = new ArrayList<>();
        // 定义solrDocument
        SolrInputDocument document = new SolrInputDocument();
        // 循环要放入solr搜索域的字段
        for (int i = 0; i < fieldNames.length; i++) {
            Field field = objFieldsNew[i];
            field.setAccessible(true);
            document.addField(fieldNames[i], field.get(data));
        }
        documents.add(document);

        solrClient.add(documents);
        solrClient.commit();
    }

    /**
     * 根据id删除多个solrdocument
     * 
     * @param solrClient
     * @param ids
     * @throws SolrServerException
     * @throws IOException
     */
    public static void deleteDataFromSolr(HttpSolrClient solrClient, List<String> ids)
        throws SolrServerException, IOException {
        // 根据id删除
        if (!ids.isEmpty()) {
            solrClient.deleteById(ids);
            solrClient.commit();
        }
    }

    /**
     * 根据id删除一个solrdocument
     * 
     * @param solrClient
     * @param ids
     * @throws SolrServerException
     * @throws IOException
     */
    public static void deleteDataFromSolr(HttpSolrClient solrClient, String id)
        throws SolrServerException, IOException {
        // 根据id删除
        solrClient.deleteById(id);
        solrClient.commit();
    }

    /**
    * 根据关键词查询（高亮显示）
    * 
    * @param solrClient
    * @param keyWord 
    *              查询关键词
    * @param df 
    *              默认搜索域
    * @param highlightFields 
    *              需高亮显示的字段
    * @param sortFields 
    *              排序字段，格式：String[] sort = {id ASC,xxx DESC,...}
    * @param start 
    *              分页起始行s
    * @param rows 
    *              每页多少行
    * @return
    * @throws SolrServerException
    * @throws IOException
    */
    public static SolrDocumentList selectDataFromSolrHithtLight(HttpSolrClient solrClient, String keyWord, String df,
        String[] highlightFields, String[] sortFields, Integer start, Integer rows)
        throws SolrServerException, IOException {
        // 创建查询对象
        SolrQuery query = new SolrQuery();
        // 设置默认搜索域
        query.set("df", df);
        // 设置搜索关键字或者词
        if (null != keyWord && keyWord.length() != 0) {
            query.setQuery(keyWord);
        } else {
            query.setQuery("*");
        }

        // 设置排序规则
        if (null != sortFields && sortFields.length != 0) {
            StringBuffer sortBuffer = new StringBuffer();
            for (int i = 0; i < sortFields.length; i++) {
                if (i == 0) {
                    sortBuffer.append(sortFields[0]);
                } else {
                    sortBuffer.append("," + sortFields[i]);
                }
            }
            query.setFacetSort("id DESC");
        }

        // 设置分页
        query.setStart(start);
        query.setRows(rows);

        if (null != highlightFields && highlightFields.length != 0) {
            // 开启高亮显示
            query.setHighlight(true);
            // 设置想要高亮显示的字段
            StringBuffer highlightBuffer = new StringBuffer();
            for (int i = 0; i < highlightFields.length; i++) {
                if (i == 0) {
                    highlightBuffer.append(highlightFields[0]);
                } else {
                    highlightBuffer.append("," + highlightFields[i]);
                }
            }
            query.addHighlightField(highlightBuffer.toString());
            // 设置高亮显示的格式
            query.setHighlightSimplePre("<font color='red'>");
            query.setHighlightSimplePost("</font>");
        }

        // 获取solr的响应
        QueryResponse response = solrClient.query(query);
        // 获取查询到的集合
        SolrDocumentList documentList = response.getResults();

        if (null != highlightFields && highlightFields.length != 0) {
            for (SolrDocument solrDocument : documentList) {
                // 第一个Map的键是文档的ID，第二个Map的键是高亮显示的字段名
                Map<String, Map<String, List<String>>> map = response.getHighlighting();
                // 拿到高亮的字段里面的内容集合，（如果没有给一个字段添加多个值的话其实只有一个值）
                Map<String, List<String>> map2 = map.get(solrDocument.get("id"));
                // 判断查询结果中具有高亮显示的字段
                for (int i = 0; i < highlightFields.length; i++) {
                    String highlightField = highlightFields[i];
                    if (map2.containsKey(highlightField)) {
                        List<String> list = map2.get(highlightField);
                        if (!list.isEmpty()) {
                            solrDocument.setField(highlightField, list.get(0));
                        }
                    }
                }
            }
        }

        return documentList;
    }

    /**
     * 根据关键词查询
     * 
     * @param solrClient
     * @param keyWord
     *            查询关键词
     * @param df
     *            默认搜索域
     * @param sortFields 
     *            排序字段，格式：String[] sort = {id ASC,xxx DESC,...}           
     * @param start
     *            分页起始行
     * @param rows
     *            每页多少行
     * @throws SolrServerException
     * @throws IOException
     */
    public static SolrDocumentList selectDataFromSolr(HttpSolrClient solrClient, String keyWord, String df,
        String[] sortFields, String start, String rows) throws SolrServerException, IOException {
        // 创建查询条件
        Map<String, String> selectMap = new HashMap<String, String>();

        // 设置搜索关键字或者词
        if (null != keyWord && keyWord.length() != 0) {
            selectMap.put("q", keyWord);
        } else {
            selectMap.put("q", "*");
        }

        // 设置默认搜索域
        selectMap.put("df", df);

        // 设置排序规则
        if (null != sortFields && sortFields.length != 0) {
            StringBuffer sortBuffer = new StringBuffer();
            for (int i = 0; i < sortFields.length; i++) {
                if (i == 0) {
                    sortBuffer.append(sortFields[0]);
                } else {
                    sortBuffer.append("," + sortFields[i]);
                }
            }
            selectMap.put("sort", sortBuffer.toString());
        }

        // 设置起始页
        selectMap.put("start", start);
        // 设置每页多少行
        selectMap.put("rows", rows);

        // 创建solr查询参数
        SolrParams solrParams = new MapSolrParams(selectMap);
        QueryResponse queryResponse = solrClient.query(solrParams);
        // 获取查询到的集合
        SolrDocumentList documentList = queryResponse.getResults();
        return documentList;
    }
}
