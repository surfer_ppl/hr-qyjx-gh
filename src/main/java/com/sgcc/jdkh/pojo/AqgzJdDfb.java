package com.sgcc.jdkh.pojo;

/**
 * 安全工作季度考核打分表
 * 
 * @author Edward
 * @date 2021年12月11日
 */
public class AqgzJdDfb {
    // ID
    private Integer id;
    // 考核关系ID
    private Integer khgxId;
    // 季度
    private Integer jd;
    // 扣分
    private Double kf;
    // 扣分理由
    private String kfLy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKhgxId() {
        return khgxId;
    }

    public void setKhgxId(Integer khgxId) {
        this.khgxId = khgxId;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

    public Double getKf() {
        return kf;
    }

    public void setKf(Double kf) {
        this.kf = kf;
    }

    public String getKfLy() {
        return kfLy;
    }

    public void setKfLy(String kfLy) {
        this.kfLy = kfLy;
    }

}
