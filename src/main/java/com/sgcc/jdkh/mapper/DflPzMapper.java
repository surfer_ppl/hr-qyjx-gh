package com.sgcc.jdkh.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.jdkh.pojo.DflPz;

/**
 * 得分率配置相关
 * 
 * @author Edward
 * @date 2021年10月6日
 */
@Mapper
public interface DflPzMapper {

    /**
     * 查询得分率配置
     * 
     * @param jdOrNd
     * @param year
     * @param dwlxId
     * @return
     */
    public List<DflPz> selectDflPzs(@Param("jdOrNd") Integer jdOrNd, @Param("year") Integer year,
        @Param("dwlxId") Integer dwlxId);

    /**
     * 新增得分率配置
     * 
     * @param paramsMap
     * @return
     */
    public int insertDflPz(Map<String, Object> paramsMap);
    
    /**
     * 删除得分率配置
     * @param idsArr
     * @return
     */
    public int deleteDflPz(@Param("idsArr") Integer[] idsArr);
}
