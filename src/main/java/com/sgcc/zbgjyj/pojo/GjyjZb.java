package com.sgcc.zbgjyj.pojo;

/**
 * 关键业绩指标实体类（T_JX_GJYJ_ZB）
 * 
 * @author Edward
 *
 */
public class GjyjZb {

    // ID
    private Integer id;
    // 单位类型指标ID
    private Integer dwlxZbId;
    // 单位ID
    private Integer dwId;
    // 考核分值
    private Double khFz;
    //打分上限
    private Integer dfSx;

    public GjyjZb() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwlxZbId() {
        return dwlxZbId;
    }

    public void setDwlxZbId(Integer dwlxZbId) {
        this.dwlxZbId = dwlxZbId;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public Double getKhFz() {
        return khFz;
    }

    public void setKhFz(Double khFz) {
        this.khFz = khFz;
    }

    public Integer getDfSx() {
        return dfSx;
    }

    public void setDfSx(Integer dfSx) {
        this.dfSx = dfSx;
    }

    @Override
    public String toString() {
        return "GjyjZb [id=" + id + ", dwlxZbId=" + dwlxZbId + ", dwId=" + dwId + ", khFz=" + khFz + ", dfSx=" + dfSx
            + "]";
    }
    
    
}
