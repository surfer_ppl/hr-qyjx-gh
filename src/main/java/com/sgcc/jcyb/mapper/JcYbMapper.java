package com.sgcc.jcyb.mapper;

import com.alibaba.fastjson.JSONArray;
import com.sgcc.jcyb.pojo.JcYbCfZz;
import com.sgcc.jcyb.pojo.JcYbJlCf;
import com.sgcc.jcyb.pojo.JcYbTbRy;
import com.sgcc.jcyb.pojo.JcYbWfLdJl;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 奖惩月报Dao层
 *
 * @Author Edward
 * @Date 2022/3/19
 */
@Mapper
public interface JcYbMapper {
    /**
     * 导入奖惩月报处罚准则
     *
     * @param jcYbCfZzList 奖惩月报处罚准则信息
     * @return 总执行数
     */
    public int insertJcYbCfZz(@Param("jcYbCfZzList") List<JcYbCfZz> jcYbCfZzList);

    /**
     * 删除处罚准则
     *
     * @return 总执行数
     */
    public int deleteJcYbCfZz();

    /**
     * 查询处罚准则
     *
     * @return
     */
    public List<JcYbCfZz> selectJcYbCfZz();

    /**
     * 从员工基本信息表查询需要的填报人员信息
     *
     * @param ygIdList
     * @return
     */
    public List<JcYbTbRy> selectJcybTbryFromJbxx(@Param("ygIdList") List<Integer> ygIdList, @Param("tabLx") String tabLx, @Param("year") Integer year, @Param("yd") Integer yd);

    /**
     * 插入填报人员信息
     *
     * @param jcYbTbRyList
     * @return
     */
    public int insertTbRy(@Param("jcYbTbRyList") List<JcYbTbRy> jcYbTbRyList);


    /**
     * 插入纪律处分人员信息(同填报人信息)
     *
     * @param jcYbTbRyList 纪律处分人员信息
     * @return 总执行数
     */
    public int insertJcYbJlCf(@Param("jcYbTbRyList") List<JcYbTbRy> jcYbTbRyList);


    /**
     * 插入违反劳动纪律人员信息(同填报人信息)
     *
     * @param jcYbTbRyList 纪律处分人员信息
     * @return 总执行数
     */
    public int insertJcYbWfLdJl(@Param("jcYbTbRyList") List<JcYbTbRy> jcYbTbRyList);

    /**
     * 查询填报人员信息
     *
     * @param year
     * @param yd
     * @param tabFlag
     * @return
     */
    public List<JcYbTbRy> selectTbrys(@Param("year") Integer year, @Param("yd") Integer yd, @Param("tabFlag") String tabFlag);


    /**
     * 查询纪律处分人员信息
     *
     * @param zzId 组织树ID
     * @param year 年
     * @param yd   月
     * @param xm   姓名
     * @return
     */
    public List<JcYbJlCf> selectJlcfs(@Param("zzId") Integer zzId, @Param("year") Integer year, @Param("yd") Integer yd, @Param("xm") String xm);

    /**
     * 查询违反劳动人员信息
     *
     * @param zzId 组织树ID
     * @param year 年
     * @param yd   月
     * @param xm   姓名
     * @return
     */
    public List<JcYbWfLdJl> selectWfLdJls(@Param("zzId") Integer zzId, @Param("year") Integer year, @Param("yd") Integer yd, @Param("xm") String xm);

    /**
     * 保存修改的数据——纪律处分备案
     *
     * @param jcYbJlCfList 要保存的数据
     * @return int 数据库执行总数
     */
    public int updateJlCfData(@Param("jcYbJlCfList") List<JcYbJlCf> jcYbJlCfList);

    /**
     * 保存修改的数据——违反劳动纪律备案
     *
     * @param jcYbWfLdJlList 要保存的数据
     * @return 数据库执行总数
     */
    public int updateWfLdData(@Param("jcYbWfLdJlList") List<JcYbWfLdJl> jcYbWfLdJlList);
}
