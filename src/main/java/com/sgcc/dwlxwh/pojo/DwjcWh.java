package com.sgcc.dwlxwh.pojo;

/**
 * 单位简称维护实体类（T_JX_DWJCWH）
 * 
 * @author Edward
 *
 */
public class DwjcWh {
    // ID
    private Integer id;
    // 单位ID
    private Integer dwId;
    // 单位类型ID
    private Integer dwlxId;
    // 单位简称
    private String dwjc;
    // 年度
    private Integer year;

    public DwjcWh() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public String getDwjc() {
        return dwjc;
    }

    public void setDwjc(String dwjc) {
        this.dwjc = dwjc;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    

}
