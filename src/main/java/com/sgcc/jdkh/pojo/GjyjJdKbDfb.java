package com.sgcc.jdkh.pojo;

/**
 * 关键业绩看板打分表实体类（T_JX_KB_GJYJ_JDDFB）
 * 
 * @author Edward
 * @date 2021/08/10
 */
public class GjyjJdKbDfb {
    // ID
    private Integer id;
    // 归口部门ID
    private Integer gkbmId;
    // 单位类型ID
    private Integer dwlxId;
    // 单位ID
    private Integer dwId;
    // 归集指标名称
    private String gjzbMc;
    // 指标名称
    private String zbMc;
    // 计量单位
    private String jlDw;
    // 考核周期
    private String khZq;
    // 考核分值
    private Double khFz;
    // 加分
    private Double jf;
    // 加分理由
    private String jfLy;
    // 扣分
    private Double kf;
    // 扣分理由
    private String kfLy;
    // 得分
    private Double df;
    // 备注
    private String bz;
    // 年度
    private Integer year;
    // 季度
    private Integer jd;

    public GjyjJdKbDfb() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(Integer gkbmId) {
        this.gkbmId = gkbmId;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public String getGjzbMc() {
        return gjzbMc;
    }

    public void setGjzbMc(String gjzbMc) {
        this.gjzbMc = gjzbMc;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public String getJlDw() {
        return jlDw;
    }

    public void setJlDw(String jlDw) {
        this.jlDw = jlDw;
    }

    public String getKhZq() {
        return khZq;
    }

    public void setKhZq(String khZq) {
        this.khZq = khZq;
    }

    public Double getKhFz() {
        return khFz;
    }

    public void setKhFz(Double khFz) {
        this.khFz = khFz;
    }

    public Double getJf() {
        return jf;
    }

    public void setJf(Double jf) {
        this.jf = jf;
    }

    public String getJfLy() {
        return jfLy;
    }

    public void setJfLy(String jfLy) {
        this.jfLy = jfLy;
    }

    public Double getKf() {
        return kf;
    }

    public void setKf(Double kf) {
        this.kf = kf;
    }

    public String getKfLy() {
        return kfLy;
    }

    public void setKfLy(String kfLy) {
        this.kfLy = kfLy;
    }

    public Double getDf() {
        return df;
    }

    public void setDf(Double df) {
        this.df = df;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

}
