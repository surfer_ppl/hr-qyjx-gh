package com.sgcc.zdkhjl.service.impl;

import com.sgcc.common.mapper.CommonCodeMapper;
import com.sgcc.common.mapper.SequenceMapper;
import com.sgcc.common.pojo.CommonCode;
import com.sgcc.common.pojo.Sequence;
import com.sgcc.constval.ConstVal;
import com.sgcc.dwlxwh.mapper.DwlxWhMapper;
import com.sgcc.dwlxwh.pojo.DwjcWh;
import com.sgcc.utils.SeqUtil;
import com.sgcc.zdkhjl.dto.ZdkhjlDwMxDto;
import com.sgcc.zdkhjl.dto.ZdkhjlKbZsDto;
import com.sgcc.zdkhjl.mapper.ZdkhjlMapper;
import com.sgcc.zdkhjl.pojo.ZdkhKbFg;
import com.sgcc.zdkhjl.pojo.ZdkhjlDwMx;
import com.sgcc.zdkhjl.pojo.ZdkhjlZtXm;
import com.sgcc.zdkhjl.service.ZdkhjlServiceI;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ZdkhjlServiceImpl implements ZdkhjlServiceI {

    @Autowired
    private SequenceMapper sequenceMapper;

    @Autowired
    private DwlxWhMapper dwlxWhMapper;

    @Autowired
    private ZdkhjlMapper zdkhjlMapper;


    /**
     * 导入重大考核激励
     */
    @Override
    public String importZdkhjl(int year, int jd, Workbook workbook) {
        String msg = null;

        // row.getCell(1).setCellType(CellType.STRING); 已经过时
        DataFormatter dataFormatter = new DataFormatter();
        //序列
        Sequence sequence = new Sequence();
        // 获取sheet页
        Sheet sheet = workbook.getSheetAt(0);

        //定义单位名称-单位ID Map
        Map<String, Integer> dwjcIdMap = new HashMap<>();
        List<DwjcWh> dwjcList = dwlxWhMapper.selectDwjc(year, null);
        for (DwjcWh dwjcWh : dwjcList) {
            dwjcIdMap.put(dwjcWh.getDwjc(), dwjcWh.getDwId());
        }
        //先验证公司的名称时候正确
        int startRowNum = 2;
        int lastRowNum = sheet.getLastRowNum();
        //从第3行循环到最后一行
        for (int i = startRowNum; i < lastRowNum + 1; i++) {
            Row row = sheet.getRow(i);
            String dwMc = row.getCell(5).getStringCellValue();
            if (StringUtils.hasLength(dwMc)) {
                if (!dwjcIdMap.containsKey(dwMc)) {
                    msg = "第" + i + "行(" + dwMc + "),名称不是规范名称,请查看";
                    //直接返回，不往下走了
                    return msg;
                }
            }
        }
        //定义存放数据集合
        List<ZdkhjlZtXm> zdkhjlZtXms = new ArrayList<>();
        List<ZdkhjlDwMx> zdkhjlDwMxs = new ArrayList<>(60);

        //id缓存
        Integer idTmp = null;
        //档位缓存
        String levelTmp = null;
        //从第3行循环到最后一行
        for (int i = startRowNum; i < lastRowNum + 1; i++) {
            Row row = sheet.getRow(i);
            //获取ID
            Integer id = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
            //实例化单位明细
            ZdkhjlDwMx zdkhjlDwMx = new ZdkhjlDwMx();
            zdkhjlDwMx.setId(id);
            zdkhjlDwMx.setYear(year);
            zdkhjlDwMx.setJd(jd);
            //实例化专题项目
            ZdkhjlZtXm zdkhjlZtXm = null;
            //以专题编号列为标准判断
            String ztBh = dataFormatter.formatCellValue(row.getCell(0));
            //有值证明是第一读取或者已经到达下一个编号
            if (StringUtils.hasLength(ztBh)) {
                //保存到缓存ID中
                idTmp = id;
                zdkhjlZtXm = new ZdkhjlZtXm();
                zdkhjlZtXm.setId(id);
                zdkhjlZtXm.setZtBh(Integer.parseInt(ztBh));//专题名称
                zdkhjlZtXm.setZtMc(row.getCell(1).getStringCellValue());//专题名称
                zdkhjlZtXm.setBzMs(row.getCell(2).getStringCellValue());//标准描述
                zdkhjlZtXm.setBzMx(row.getCell(3).getStringCellValue());//标砖明细
                zdkhjlZtXm.setYear(year);
                zdkhjlZtXm.setJd(jd);
                //放入集合中
                zdkhjlZtXms.add(zdkhjlZtXm);
                //关联ID
                zdkhjlDwMx.setZtXmId(id);
            } else {
                //获取已经存在的关联ID
                zdkhjlDwMx.setZtXmId(idTmp);
            }
            //处理档位的问题,如果存在证明是第一读取或者进入下一档
            String level = row.getCell(4).getStringCellValue().trim();
            if (StringUtils.hasLength(level)) {
                levelTmp = level;
                zdkhjlDwMx.setLevel(level);
            } else {
                zdkhjlDwMx.setLevel(levelTmp);
            }

            //从第六列开始循环，放入单位明细表

            String dwMc = row.getCell(5).getStringCellValue();
            if (StringUtils.hasLength(dwMc)) {
                //根据单位名称获取单位ID
                zdkhjlDwMx.setDwId(dwjcIdMap.get(dwMc));
            }
            String pjMs = row.getCell(6).getStringCellValue();
            String pjMx = row.getCell(7).getStringCellValue();

            zdkhjlDwMx.setPjMs(pjMs);
            zdkhjlDwMx.setPjMx(pjMx);
            zdkhjlDwMxs.add(zdkhjlDwMx);
        }

        //先删除
        zdkhjlMapper.deleteZdkhjlZtXm(year, jd);
        zdkhjlMapper.deleteZdkhjlDwMx(year, jd);
        //最后执行插入
        zdkhjlMapper.insertZdkhjlZtXm(zdkhjlZtXms);
        zdkhjlMapper.insertZdkhjlDwMx(zdkhjlDwMxs);

        return null;
    }

    /**
     * 查询表格内容
     */
    public List<ZdkhjlDwMxDto> selectGridData(Integer year, Integer jd) {
        List<ZdkhjlDwMxDto> gridDate = zdkhjlMapper.selectGridData(year, jd);
        return gridDate;
    }

    /**
     * 查询看板展示
     */
    public List<ZdkhjlKbZsDto> selectKbZsData(Integer year, Integer jd) {
        List<ZdkhjlKbZsDto> kbZsDate = zdkhjlMapper.selectKbZsData(year, jd);
        return kbZsDate;
    }

    /**
     * 查询专题明细根据ID
     *
     * @param id 表ID
     * @return ZdkhjlZtXm
     */
    public ZdkhjlZtXm selectZtXmById(Integer id) {
        ZdkhjlZtXm zdkhjlZtXm = zdkhjlMapper.selectZtXmById(id);
        return zdkhjlZtXm;
    }

    /**
     * 查询单位明细根据ID
     *
     * @param id 表ID
     * @return ZdkhjlDwMx
     */
    public ZdkhjlDwMx selectDwMxById(Integer id) {
        ZdkhjlDwMx zdkhjlDwMx = zdkhjlMapper.selectDwMxById(id);
        return zdkhjlDwMx;
    }

    /**
     * 查询已定看板风格
     */
    public ZdkhKbFg selectFg() {
        ZdkhKbFg zdkhKbFg = zdkhjlMapper.selectFg();
        return zdkhKbFg;
    }

    /**
     * 保存看板风格
     */
    public void updateKbFg(String styleCode){
        zdkhjlMapper.updateKbFg(styleCode);
    }
}
