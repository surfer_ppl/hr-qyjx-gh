package com.sgcc.jdkh.mapper;

import java.util.List;
import java.util.Map;

import com.sgcc.jdkh.dto.AqgzDbDfDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.jdkh.dto.GjyjDbDfDto;
import com.sgcc.jdkh.dto.ZygzDbDfDto;
import com.sgcc.jdkh.pojo.DflPz;
import com.sgcc.jdkh.pojo.ZsDf;

@Mapper
public interface JdkhMapper {

    /**
     * 查询关键业绩考核季度考核（待办打分，考核首页dataGrid）
     *
     * @param params
     * @return
     */
    public List<GjyjDbDfDto> selectGjyjJdkh(Map<String, String> params);

    /**
     * 查询专业工作季度考核（待办打分，考核首页dataGrid）
     *
     * @param params
     * @return
     */
    public List<ZygzDbDfDto> selectZygzJdkh(Map<String, String> params);

    /**
     * 查询安全工作季度考核（待办打分，考核首页dataGrid）
     *
     * @param params
     * @return
     */
    public List<AqgzDbDfDto> selectAqgzJdkh(Map<String, String> params);

    /**
     * 插入得分率配置
     *
     * @param dflPz
     * @return
     */
    public int insertDflPz(DflPz dflPz);

    /**
     * 插入季度考核待办
     *
     * @param year
     * @param jd
     * @return
     */
    public int insertJdkhDb(@Param("year") int year, @Param("jd") int jd);

    /**
     * 删除季度考核待办
     *
     * @param year
     * @param jd
     * @return
     */
    public int deleteJdkhDb(@Param("year") int year, @Param("jd") int jd);

    /**
     * 更新季度考核打分表——初始化DF字段
     *
     * @param year
     * @param jd
     * @return
     */
    public int updateInitJdkhDf(int year, int jd);

    /**
     * 更新季度考核打分表——更新DF字段
     *
     * @param id
     * @return
     */
    public int updateJdkhDf(int id);

    /**
     * 插入折算得分——初始化折算得分数据
     *
     * @return
     */
    public int insertInitZsDf(int year, int jd);
    
    /**
     * 维护折算配置表总得分字段，用于折算计算
     * @param year
     * @param jd
     * @param gkbmId
     * @return
     */
    public int whZdf(@Param("year") Integer year, @Param("jd") Integer jd, @Param("gkbmId") Integer gkbmId);
    
    /**
     * 删除折算得分
     *
     * @param year
     * @param jd
     * @param gkbmId
     * @return
     */
    public int deleteZsDf(@Param("year") Integer year, @Param("jd") Integer jd, @Param("gkbmId") Integer gkbmId);

    /**
     * 插入折算得分(更新计算得到的折算分)
     *
     * @param zsDfsList
     * @return
     */
    public int updateZsDf(@Param("zsDfsList") List<ZsDf> zsDfsList);

    /**
     * 查询最高分和最低分
     *
     * @param year
     * @param jd
     * @param gkbmId
     * @return
     */
    public List<Map<String, Object>> selectZgfAndZdf(@Param("year") Integer year, @Param("jd") Integer jd,
        @Param("gkbmId") Integer gkbmId, @Param("dwlxMc") String dwlxMc);

    /**
     * 查询得分率
     *
     * @param year
     * @param jd
     * @return
     */
    public List<DflPz> selectDfl(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询需要折算的数据
     *
     * @param year
     * @param jd
     * @param gkbmId
     * @return
     */
    public List<ZsDf> selectZsData(@Param("year") Integer year, @Param("jd") Integer jd,
        @Param("gkbmId") Integer gkbmId);

}
