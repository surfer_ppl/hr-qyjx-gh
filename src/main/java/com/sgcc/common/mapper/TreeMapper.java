package com.sgcc.common.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.common.dto.ElmTreeObj;
import com.sgcc.common.pojo.RsYgJbxx;

@Mapper
public interface TreeMapper {

    /**
     * 获取单位树结构
     * 
     * @param orgId
     *            组织角色ID
     * @return
     */
    public List<Map<String, Object>> selectDwTree(@Param("orgId") Integer orgId);

    /**
     * 查询归口部门树
     * 
     * @param orgId
     * @return
     */
    public List<Map<String, Object>> selectGkbmTree(@Param("orgId") Integer orgId);

    /**
     * 查询ElementUi树结构根节点数据（懒加载）
     * 
     * @param spOrgId
     * @return
     */
    public List<ElmTreeObj> selectElmZzRootTreeLazy(Integer spOrgId);

    /**
     * 查询ElementUi树结构子节点数据（懒加载）
     * 
     * @param spOrgId
     * @return
     */
    public List<ElmTreeObj> selectElmChildTreeLazy(Integer parentId);

    /**
     * 查询ElementUi树结构子节点数据（懒加载）
     * 
     * @param spOrgId
     * @return
     */
    public List<ElmTreeObj> selectElmChildTreeLazy_Right(@Param("parentId") Integer parentId,
        @Param("spOrgCode") String spOrgCode, @Param("level") Integer level);

    /**
     * 查询饿了么根节点（单位类型）
     * 
     * @param year
     * @return
     */
    public List<Map<String, Object>> selectElmRootNode_Dwlx(Integer year);

    /**
     * 查询饿了么子节点（单位）
     * 
     * @param year
     * @param dwlxId
     * @return
     */
    public List<Map<String, Object>> selectElmChildNode_Dw(Integer year, Integer dwlxId);

    /**
     * 根据组织树查询对应的人
     * 
     * @param zzId
     * @param xm
     * @return
     */
    public List<RsYgJbxx> selectRsYgJbxx(@Param("zzId") Integer zzId, @Param("xm") String xm);

}
