package com.sgcc.common.service;

import com.sgcc.common.pojo.CommonCode;

import java.util.List;

/**
 * @Author Edward
 * @Date 2022/3/21
 */
public interface CommonCodeServiceI {
    /**
     * 根据类型编码查询公共代码
     * @param codeTypeBm
     * @return
     */
    public List<CommonCode> selectCommonCode(String codeTypeBm);
}
