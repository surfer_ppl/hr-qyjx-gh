package com.sgcc.zbaqgzkh.pojo;

/**
 * 安全工作考核指标表（T_JX_AQGZ_ZB）
 * 
 * @author Edward
 *
 */
public class AqgzKhZb {
    // ID
    private Integer id;
    /* // 单位ID
    private Integer dwId;
    // 单位ID
    private Integer dwlxId;*/
    // 指标名称
    private String zbMc;
    // 父指标Id
    private Integer parentId;
    // 归口部门ID
    private Integer gkbmId;
    // 考核扣分标准
    private String khKfBz;
    // 考核周期
    private String khZq;

    public AqgzKhZb() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /* public Integer getDwId() {
        return dwId;
    }
    
    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }
    
    public Integer getDwlxId() {
        return dwlxId;
    }
    
    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }*/

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(Integer gkbmId) {
        this.gkbmId = gkbmId;
    }

    public String getKhKfBz() {
        return khKfBz;
    }

    public void setKhKfBz(String khKfBz) {
        this.khKfBz = khKfBz;
    }

    public String getKhZq() {
        return khZq;
    }

    public void setKhZq(String khZq) {
        this.khZq = khZq;
    }

}
