package com.sgcc.jdkh.dto;

/**
 * 关键业绩待办打分Dto
 * 
 * @author Edward
 * @date 2021年5月15日
 */
public class GjyjDbDfDto {
    // ID
    private Integer id;
    //归集指标名称
    private String gjzbMc;
    //指标名称
    private String zbMc;
    //计量单位
    private String jlDw;
    //考核周期
    private String khZq;
    // 归口部门名称
    private String gkbmMc;
    // 单位类型名称
    private String dwlxMc;
    // 单位名称
    private String dwMc;
    // 考核分值
    private Double khFz;
    // 加分
    private Double jf;
    // 加分理由
    private String jfLy;
    // 扣分
    private Double kf;
    // 扣分理由
    private String kfLy;
    // 得分
    private Double df;
    //备注
    private String bz;

    public GjyjDbDfDto() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGjzbMc() {
        return gjzbMc;
    }

    public void setGjzbMc(String gjzbMc) {
        this.gjzbMc = gjzbMc;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public String getJlDw() {
        return jlDw;
    }

    public void setJlDw(String jlDw) {
        this.jlDw = jlDw;
    }

    public String getKhZq() {
        return khZq;
    }

    public void setKhZq(String khZq) {
        this.khZq = khZq;
    }

    public String getGkbmMc() {
        return gkbmMc;
    }

    public void setGkbmMc(String gkbmMc) {
        this.gkbmMc = gkbmMc;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public Double getKhFz() {
        return khFz;
    }

    public void setKhFz(Double khFz) {
        this.khFz = khFz;
    }

    public Double getJf() {
        return jf;
    }

    public void setJf(Double jf) {
        this.jf = jf;
    }

    public String getJfLy() {
        return jfLy;
    }

    public void setJfLy(String jfLy) {
        this.jfLy = jfLy;
    }

    public Double getKf() {
        return kf;
    }

    public void setKf(Double kf) {
        this.kf = kf;
    }

    public String getKfLy() {
        return kfLy;
    }

    public void setKfLy(String kfLy) {
        this.kfLy = kfLy;
    }

    public Double getDf() {
        return df;
    }

    public void setDf(Double df) {
        this.df = df;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }  
}
