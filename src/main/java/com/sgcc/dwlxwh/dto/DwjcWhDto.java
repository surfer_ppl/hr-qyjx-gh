package com.sgcc.dwlxwh.dto;

/**
 * 单位简称维护查询Dto
 * 
 * @author Edward
 *
 */
public class DwjcWhDto {

    // ID
    private int id;
    // 年份
    private int year;
    // 单位类型名称
    private String dwlxMc;
    // 单位全称
    private String dwQc;

    // 单位名称备份
    private String dwJcBak;

    // 单位简称
    private String dwJc;

    public DwjcWhDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public String getDwQc() {
        return dwQc;
    }

    public void setDwQc(String dwQc) {
        this.dwQc = dwQc;
    }

    public String getDwJcBak() {
        return dwJcBak;
    }

    public void setDwJcBak(String dwJcBak) {
        this.dwJcBak = dwJcBak;
    }

    public String getDwJc() {
        return dwJc;
    }

    public void setDwJc(String dwJc) {
        this.dwJc = dwJc;
    }

}
