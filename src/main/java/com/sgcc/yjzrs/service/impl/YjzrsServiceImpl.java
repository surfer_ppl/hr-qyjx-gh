package com.sgcc.yjzrs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.yjzrs.mapper.YjzrsMapper;
import com.sgcc.yjzrs.service.YjzrsServiceI;

@Service
public class YjzrsServiceImpl implements YjzrsServiceI {
    
    @Autowired
    private YjzrsMapper yjzrsMapper;
    
    /**
     * 根据年度创建打分表数据
     */
    @Override
    public void createDfbData(int year) {
        
        //四个季度
        int[] jdArr = {1,2,3,4};
        
        //生成打分表数据
        yjzrsMapper.insertGjyjZbJdDfb(jdArr, year);
        yjzrsMapper.insertZygzKhJdDfb(jdArr, year); 
        yjzrsMapper.insertAqgzKhJdDfb(jdArr, year);
    }

}
