package com.sgcc.jcyb.pojo;

/**
 * 奖惩月报违规违纪行为处罚准则实例类（T_JX_JCYB_CFZZ）
 * @Author Edward
 * @Date 2022/3/19
 */
public class JcYbCfZz {
    /**
     * 表ID
     */
    private Integer id;
    /**
     * 编号
     */
    private Integer bh;
    /**
     * 中文编号
     */
    private String bhCn;
    /**
     * 说明（准则）
     */
    private String sm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBh() {
        return bh;
    }

    public void setBh(Integer bh) {
        this.bh = bh;
    }

    public String getBhCn() {
        return bhCn;
    }

    public void setBhCn(String bhCn) {
        this.bhCn = bhCn;
    }

    public String getSm() {
        return sm;
    }

    public void setSm(String sm) {
        this.sm = sm;
    }
}
