package com.sgcc.zdkhjl.pojo;

/**
 * 看板分格实体类（T_JX_ZXKHJL_KBFG）
 * @Author Edward
 * @Date 2022/3/21
 */
public class ZdkhKbFg {
    private Integer id;
    private String fgBh;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFgBh() {
        return fgBh;
    }

    public void setFgBh(String fgBh) {
        this.fgBh = fgBh;
    }
}
