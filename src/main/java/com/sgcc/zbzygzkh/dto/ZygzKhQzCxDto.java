package com.sgcc.zbzygzkh.dto;

public class ZygzKhQzCxDto {
    // 单位类型ID-单位ID
    private String dwlxDw;
    // id
    private Integer id;
    // 单位类型ID
    private Integer dwlxId;
    // 单位类型名称
    private String dwlxMc;
    // 单位ID
    private Integer dwId;
    // 单位名称
    private String dwMc;
    // 归口部门ID
    private String gkbmId;
    // 归口部门名称
    private String gkbmMc;
    // 权重
    private String qz;
    // 季度
    private Integer jd;

    public ZygzKhQzCxDto() {

    }

    public String getDwlxDw() {
        return dwlxDw;
    }

    public void setDwlxDw(String dwlxDw) {
        this.dwlxDw = dwlxDw;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public String getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(String gkbmId) {
        this.gkbmId = gkbmId;
    }

    public String getGkbmMc() {
        return gkbmMc;
    }

    public void setGkbmMc(String gkbmMc) {
        this.gkbmMc = gkbmMc;
    }

    public String getQz() {
        return qz;
    }

    public void setQz(String qz) {
        this.qz = qz;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

}
