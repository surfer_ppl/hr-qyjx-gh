package com.sgcc.jdkh.service;

import java.util.List;
import java.util.Map;

import com.sgcc.jdkh.pojo.DflPz;

/**
 * 得分率配置接口
 * 
 * @author Edward
 * @date 2021年10月6日
 */
public interface DflPzServiceI {
    /**
     * 查询得分率配置
     * @param jdOrNd
     * @param year
     * @param dwlxId
     * @return
     */
    public List<DflPz> selectDflPzs(Integer jdOrNd, Integer year, Integer dwlxId);
    
    
    /**
     * 新增得分率配置
     * 
     * @param paramsMap
     * @return
     */
    public void insertDflPz(Map<String, Object> paramsMap);
    
    /**
     * 删除得分率配置
     * @param idsArr
     * @return
     */
    public void deleteDflPz(Integer[] idsArr);
}
