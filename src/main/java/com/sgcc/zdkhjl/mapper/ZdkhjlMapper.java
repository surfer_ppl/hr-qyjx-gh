package com.sgcc.zdkhjl.mapper;

import com.sgcc.zdkhjl.dto.ZdkhjlDwMxDto;
import com.sgcc.zdkhjl.dto.ZdkhjlKbZsDto;
import com.sgcc.zdkhjl.pojo.ZdkhKbFg;
import com.sgcc.zdkhjl.pojo.ZdkhjlDwMx;
import com.sgcc.zdkhjl.pojo.ZdkhjlZtXm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 重大考核激励Mapper
 *
 * @Author Edward
 * @Date 2022/3/19
 */
@Mapper
public interface ZdkhjlMapper {

    /**
     * 插入重大考核奖励专题项目表
     *
     * @param zdkhjlZtXms
     * @return
     */
    public int insertZdkhjlZtXm(@Param("zdkhjlZtXms") List<ZdkhjlZtXm> zdkhjlZtXms);

    /**
     * 插入重大考核奖励单位明细表
     *
     * @param zdkhjlDwMxs
     * @return
     */
    public int insertZdkhjlDwMx(@Param("zdkhjlDwMxs") List<ZdkhjlDwMx> zdkhjlDwMxs);

    /**
     * 删除重大考核奖励专题项目表
     *
     * @param year
     * @param jd
     * @return
     */
    public int deleteZdkhjlZtXm(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 删除重大考核奖励单位明细表
     *
     * @param year 年度
     * @param jd   季度
     * @return 总执行数
     */
    public int deleteZdkhjlDwMx(@Param("year") Integer year, @Param("jd") Integer jd);


    /**
     * 查询表格内容
     *
     * @param year
     * @param jd
     * @return
     */
    public List<ZdkhjlDwMxDto> selectGridData(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询看板展示
     *
     * @param year
     * @param jd
     * @return
     */
    public List<ZdkhjlKbZsDto> selectKbZsData(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询专题明细根据ID
     *
     * @param id 季度
     * @return List<ZdkhjlZtXm>
     */
    public ZdkhjlZtXm selectZtXmById(@Param("id") Integer id);

    /**
     * 查询单位明细根据ID
     *
     * @param id
     * @return
     */
    public ZdkhjlDwMx selectDwMxById(@Param("id") Integer id);

    /**
     * 查询已定看板风格
     *
     * @return ZdkhKbFg
     */
    public ZdkhKbFg selectFg();

    /**
     * 保存看板风格
     * @param styleCode
     * @return
     */
    public int updateKbFg(String styleCode);



}
