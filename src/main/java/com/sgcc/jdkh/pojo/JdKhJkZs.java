package com.sgcc.jdkh.pojo;

/**
 * 季度考核监控展示实体类（T_JX_KB_JDJK）
 * 
 * @author Edward
 * @date 2021/08/10
 */
public class JdKhJkZs {

    // ID
    private Integer id;
    // 单位ID
    private Integer dwId;
    // 单位名称
    private String dwMc;
    // 单位类型ID
    private Integer dwlxId;
    // 单位类型名称
    private String dwlxMc;
    // 合计得分
    private Double hjDf;
    // 合计排名
    private Integer hjPm;
    // 小计得分
    private Double xjDf;
    // 小计排名
    private Integer xjPm;
    // 关键业绩总加分值
    private Double zjfz;
    // 关键业绩总减分值
    private Double zkfz;
    // 关键业绩得分详情
    private String zdfBz;
    // 专业工作考核得分（使用权重计算）
    private Double zygzZdf;
    // 专业工作考核得分排名
    private Integer zygzZdfPm;
    // 安全工作考核总扣分
    private Double aqgzZdf;
    // 安全工作考核，每个父指标的得分
    private String aqgzDfXx;
    // 安全工作考核总扣分累计
    private Double aqgzZdfLj;
    // 季度考核得分折算（合计*0.1）
    private Double jdkhDfZs;
    // 季度考核得分累计
    private Double jdkhDfZsLj;
    // 年度
    private Integer year;
    // 季度
    private Integer jd;
    // 发布状态（0未发布，1发布）
    private Integer zt;

    public JdKhJkZs() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public Double getHjDf() {
        return hjDf;
    }

    public void setHjDf(Double hjDf) {
        this.hjDf = hjDf;
    }

    public Integer getHjPm() {
        return hjPm;
    }

    public void setHjPm(Integer hjPm) {
        this.hjPm = hjPm;
    }

    public Double getXjDf() {
        return xjDf;
    }

    public void setXjDf(Double xjDf) {
        this.xjDf = xjDf;
    }

    public Integer getXjPm() {
        return xjPm;
    }

    public void setXjPm(Integer xjPm) {
        this.xjPm = xjPm;
    }

    public Double getZjfz() {
        return zjfz;
    }

    public void setZjfz(Double zjfz) {
        this.zjfz = zjfz;
    }

    public Double getZkfz() {
        return zkfz;
    }

    public void setZkfz(Double zkfz) {
        this.zkfz = zkfz;
    }

    public String getZdfBz() {
        return zdfBz;
    }

    public void setZdfBz(String zdfBz) {
        this.zdfBz = zdfBz;
    }

    public Double getZygzZdf() {
        return zygzZdf;
    }

    public void setZygzZdf(Double zygzZdf) {
        this.zygzZdf = zygzZdf;
    }

    public Integer getZygzZdfPm() {
        return zygzZdfPm;
    }

    public void setZygzZdfPm(Integer zygzZdfPm) {
        this.zygzZdfPm = zygzZdfPm;
    }

    public Double getAqgzZdf() {
        return aqgzZdf;
    }

    public void setAqgzZdf(Double aqgzZdf) {
        this.aqgzZdf = aqgzZdf;
    }

    public String getAqgzDfXx() {
        return aqgzDfXx;
    }

    public void setAqgzDfXx(String aqgzDfXx) {
        this.aqgzDfXx = aqgzDfXx;
    }

    public Double getAqgzZdfLj() {
        return aqgzZdfLj;
    }

    public void setAqgzZdfLj(Double aqgzZdfLj) {
        this.aqgzZdfLj = aqgzZdfLj;
    }

    public Double getJdkhDfZs() {
        return jdkhDfZs;
    }

    public void setJdkhDfZs(Double jdkhDfZs) {
        this.jdkhDfZs = jdkhDfZs;
    }

    public Double getJdkhDfZsLj() {
        return jdkhDfZsLj;
    }

    public void setJdkhDfZsLj(Double jdkhDfZsLj) {
        this.jdkhDfZsLj = jdkhDfZsLj;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

    public Integer getZt() {
        return zt;
    }

    public void setZt(Integer zt) {
        this.zt = zt;
    }

}
