package com.sgcc.zbgjyj.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.zbgjyj.dto.GjyjCxDto;
import com.sgcc.zbgjyj.pojo.GjyjDwlxZb;
import com.sgcc.zbgjyj.pojo.GjyjGjzb;
import com.sgcc.zbgjyj.pojo.GjyjZb;

@Mapper
public interface GjyjZbwhMapper {
    
    
    /**
     * 根据条件查询关键业绩维护表格维护数据
     * @param paramsMap 参数集合
     * @return
     */
    public List<GjyjCxDto> selectGjyjDataGrid(Map<String, Object> paramsMap);
    
    /**
     * 插入关键业绩指标归集指标
     * @param gjyjGjzbList
     * @return
     */
    public int insertGjyjGjzb(@Param("gjyjGjzbList") List<GjyjGjzb> gjyjGjzbList);

    /**
     * 插入关键业绩指标单位类型指标
     * @param gjyjDwlxZbList
     * @return
     */
    public int insertGjYjDwlxZb(@Param("gjyjDwlxZbList") List<GjyjDwlxZb> gjyjDwlxZbList);

    /**
     * 插入关键业绩指标单位指标
     * @param gjyjZbList
     * @return
     */
    public int insertGjYjZb(@Param("gjyjZbList") List<GjyjZb> gjyjZbList);
    
    /**
     * 删除关键业绩指标单位指标
     * @param year
     * @return
     */
    public int deleteGjYjZb(Integer year);
    
    /**
     * 删除关键业绩指标单位类型指标
     * @param year
     * @return
     */
    public int deleteGjyjGjzb(Integer year);
    
    /**
     * 删除关键业绩指标归集指标
     * @param year
     * @return
     */
    public int deleteGjYjDwlxZb(Integer year);

    /**
     * 一键启用指标
     * @param ids 表格中所有行的ID
     * @return
     */
    public int updateZtToStart(Integer[] ids);

    /**
     * 一键停用指标
     * @param ids 表格中所有行的ID
     * @return
     */
    public int updateZtToStop (Integer[] ids);

}
