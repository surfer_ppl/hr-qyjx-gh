package com.sgcc.common.pojo;

public class Sequence {
    private String seqName;
    private Integer seqVal;

    public Sequence() {

    }

    public String getSeqName() {
        return seqName;
    }

    public void setSeqName(String seqName) {
        this.seqName = seqName;
    }

    public Integer getSeqVal() {
        return seqVal;
    }

    public void setSeqVal(Integer seqVal) {
        this.seqVal = seqVal;
    }
}
