package com.sgcc;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;

import javax.annotation.security.RunAs;

@RunWith(SpringRunner.class)
@SpringBootTest
class HrQyjxGhApplicationTests {

    @Autowired
    Jedis jedis;

    @Test
    void contextLoads() {
    }

    @Test
    void redisTest(){
        String s = jedis.get("key1");
        System.out.println(s);
    }

}
