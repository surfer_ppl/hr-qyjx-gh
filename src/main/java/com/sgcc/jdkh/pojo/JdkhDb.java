package com.sgcc.jdkh.pojo;

/**
 * 季度考核待办部门实体类 （T_JX_JDKHDBBM）
 * 
 * @author Edward
 *
 */
public class JdkhDb {
    //id
    private Integer id;
    //归口部门Id
    private Integer gkbmId;
    //归口部门Mc
    private Integer gkbmMc;
    //年份
    private Integer year;
    //季度
    private Integer jd;

    public JdkhDb() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(Integer gkbmId) {
        this.gkbmId = gkbmId;
    }

    public Integer getGkbmMc() {
        return gkbmMc;
    }

    public void setGkbmMc(Integer gkbmMc) {
        this.gkbmMc = gkbmMc;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

}
