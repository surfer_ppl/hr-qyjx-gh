package com.sgcc.zbzygzkh.pojo;

/**
 * 专业工作考核权重实体类（T_JX_ZYGZ_QZ）
 * 
 * @author Edward
 *
 */
public class ZygzKhQz {
    // ID
    private Integer id;
    // 单位类型ID
    private Integer dwlxId;
    // 单位ID
    private Integer dwId;
    // 归口部门ID
    private Integer gkbmId;
    // 权重
    private Double qz;
    // 季度
    private Integer jd;
    // 状态 0：不按照季度导入 1：按照季度导入
    private Integer zt;

    public ZygzKhQz() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public Integer getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(Integer gkbmId) {
        this.gkbmId = gkbmId;
    }

    public Double getQz() {
        return qz;
    }

    public void setQz(Double qz) {
        this.qz = qz;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

    public Integer getZt() {
        return zt;
    }

    public void setZt(Integer zt) {
        this.zt = zt;
    }

}
