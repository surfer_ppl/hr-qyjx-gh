package com.sgcc.zbzygzkh.service;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.sgcc.zbzygzkh.dto.ZygzKhZbCxDto;

/**
 * 专业工作考核指标评价Service
 * 
 * @author Edward
 * @date 2021年6月20日
 */
public interface ZygzKhZbServiceI {
    /**
     * 查询专业工作考核详情表格数据
     * 
     * @param year
     * @param zbMc
     * @return
     */
    public List<ZygzKhZbCxDto> selectZygzKhZbDataGrid(int year,String zbMc);
    
    /**
     * 导出专业工作考核评价数据
     * @param workbook
     * @param year
     * @return
     */
    public Workbook exportZygzKhZb(Workbook workbook, Integer year);
}
