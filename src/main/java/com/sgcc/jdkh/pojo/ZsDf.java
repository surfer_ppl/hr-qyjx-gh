package com.sgcc.jdkh.pojo;

/**
 * 折算得分实体类 (T_JX_ZSDF)
 * 
 * @author Edward
 *
 */
public class ZsDf {
    //ID
    private Integer id;
    //单位类型Id
    private Integer dwlxId;
    //归口部门ID
    private Integer gkbmId;
    //单位ID
    private Integer dwId;
    //考核分值总分
    private Double khfzZf;
    //总得分
    private Double zdf;
    //折算分
    private Double zsf;
    
    public ZsDf() {
        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public Integer getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(Integer gkbmId) {
        this.gkbmId = gkbmId;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public Double getKhfzZf() {
        return khfzZf;
    }

    public void setKhfzZf(Double khfzZf) {
        this.khfzZf = khfzZf;
    }

    public Double getZdf() {
        return zdf;
    }

    public void setZdf(Double zdf) {
        this.zdf = zdf;
    }

    public Double getZsf() {
        return zsf;
    }

    public void setZsf(Double zsf) {
        this.zsf = zsf;
    }
    
}
