package com.sgcc.zbzygzkh.dto;

/**
 * 专业工作考核指标详情查询
 * 
 * @author Edward
 *
 */
public class ZygzKhZbCxDto {
    private Integer xh;
    private String gkbmMc;
    private String dwlxMc;
    private String dwMc;
    private String zbMc;
    private Integer jfOrKf;
    private Integer jfSx;
    private Integer year;

    public ZygzKhZbCxDto() {

    }

    public Integer getXh() {
        return xh;
    }

    public void setXh(Integer xh) {
        this.xh = xh;
    }

    public String getGkbmMc() {
        return gkbmMc;
    }

    public void setGkbmMc(String gkbmMc) {
        this.gkbmMc = gkbmMc;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public Integer getJfOrKf() {
        return jfOrKf;
    }

    public void setJfOrKf(Integer jfOrKf) {
        this.jfOrKf = jfOrKf;
    }

    public Integer getJfSx() {
        return jfSx;
    }

    public void setJfSx(Integer jfSx) {
        this.jfSx = jfSx;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

}
