package com.sgcc.zbgjyj.service.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.sgcc.common.mapper.SequenceMapper;
import com.sgcc.common.pojo.Sequence;
import com.sgcc.constval.ConstVal;
import com.sgcc.dwlxwh.dto.GkbmDto;
import com.sgcc.dwlxwh.mapper.DwlxWhMapper;
import com.sgcc.dwlxwh.pojo.DwjcWh;
import com.sgcc.dwlxwh.pojo.Dwlx;
import com.sgcc.utils.SeqUtil;
import com.sgcc.zbgjyj.dto.GjyjCxDto;
import com.sgcc.zbgjyj.mapper.GjyjZbwhMapper;
import com.sgcc.zbgjyj.pojo.GjyjDwlxZb;
import com.sgcc.zbgjyj.pojo.GjyjGjzb;
import com.sgcc.zbgjyj.pojo.GjyjZb;
import com.sgcc.zbgjyj.service.GjyjZbWhServiceI;

@Service
public class GjyjZbWhServiceImpl implements GjyjZbWhServiceI {

    @Autowired
    private DwlxWhMapper dwlxWhMapper;

    @Autowired
    private GjyjZbwhMapper gjyjZbwhMapper;

    @Autowired
    private SequenceMapper sequenceMapper;

    /**
     * 根据条件查询关键业绩维护表格维护数据
     */
    @Override
    public List<GjyjCxDto> selectGjyjDataGrid(Map<String, Object> paramsMap) {
        List<GjyjCxDto> gjyjDataGrid = gjyjZbwhMapper.selectGjyjDataGrid(paramsMap);
        return gjyjDataGrid;
    }

    /**
     * 导入关键业绩指标
     */
    @Override
    public String importGjyjZb(int year, Workbook workbook) {

        // row.getCell(1).setCellType(CellType.STRING); 已经过时
        DataFormatter dataFormatter = new DataFormatter();

        String msg = null;
        // 获取sheet页
        Sheet sheet0 = workbook.getSheetAt(0);

        Row row0 = sheet0.getRow(0);
        if (null == row0) {
            msg = "请勿导入空文件";
            return msg;
        }
        // 判断第四行
        Row row3 = sheet0.getRow(3);
        if (null == row3) {
            msg = "文件第四行没有数据，请查看是否录入数据";
            return msg;
        }

        // 定义三个个Map分别存放单位类型和单位简称还有归口部门
        Map<String, Integer> dwlxMap = new HashMap<>();
        Map<String, Integer> dwjcIdMap = new HashMap<>();
        Map<String, Integer> dwjcDwlxMap = new HashMap<>();
        Map<String, Integer> gkbmMap = new HashMap<>();

        List<Dwlx> dwlxList = dwlxWhMapper.selectDwlx(year);

        if (dwlxList.isEmpty()) {
            msg = "请查看单位类型数据是否维护！";
            return msg;
        }

        for (Dwlx dwlx : dwlxList) {
            dwlxMap.put(dwlx.getDwlxMc(), dwlx.getId());
        }
        List<DwjcWh> dwjcList = dwlxWhMapper.selectDwjc(year, null);
        for (DwjcWh dwjcWh : dwjcList) {
            dwjcIdMap.put(dwjcWh.getDwjc(), dwjcWh.getDwId());
        }
        for (DwjcWh dwjcWh : dwjcList) {
            dwjcDwlxMap.put(dwjcWh.getDwjc(), dwjcWh.getDwlxId());
        }
        List<GkbmDto> gkbmList = dwlxWhMapper.selectGkmb();
        for (GkbmDto gkbmDto : gkbmList) {
            gkbmMap.put(gkbmDto.getGkbmMc(), gkbmDto.getId());
        }

        // 验证sheet页单位类型名称和单位名称对不对
        int totalSheetNum = workbook.getNumberOfSheets();

        for (int i = 0; i < totalSheetNum; i++) {
            // 第一个sheet页为供电公司类
            if (i == 0) {
                // 判断sheet页的名称（单位类型名称）
                String sheetName = sheet0.getSheetName().trim();

                if (!dwlxMap.containsKey(sheetName)) {
                    msg = "单位类型名称（" + sheetName + "）不正确";
                    return msg;
                }

                Row row1 = sheet0.getRow(1);
                short lastCellNum = row1.getLastCellNum();
                // 判断13家供电公司的简称
                for (int j = 9; j < lastCellNum; j++) {
                    String dwjc = row1.getCell(j).getStringCellValue();
                    if (StringUtils.hasLength(dwjc)) {
                        if (!dwjcIdMap.containsKey(dwjc.trim())) {
                            msg = "第" + (i + 1) + "个sheet页单位名称（" + dwjc + "）不正确";
                            return msg;
                        }
                    } else {
                        msg = "第" + (i + 1) + "个sheet页单位名称不能为空";
                        return msg;
                    }
                }

                // 判断归口部门名称是否都正确
                int lastRowNum = sheet0.getLastRowNum();
                for (int j = 3; j < lastRowNum + 1; j++) {
                    String gkbmMc = sheet0.getRow(j).getCell(8).getStringCellValue();
                    if (StringUtils.hasLength(gkbmMc)) {
                        if (!gkbmMap.containsKey(gkbmMc.trim())) {
                            msg = "第" + (i + 1) + "个sheet页面归口部门名称（" + gkbmMc + "）不正确";
                            return msg;
                        }
                    } else {
                        msg = "第" + (i + 1) + "个sheet页面归口部门名称有空值";
                        return msg;
                    }
                }
            } else {
                // 判断sheet页的名称(单位名称)
                Sheet sheet = workbook.getSheetAt(i);
                String sheetName = sheet.getSheetName().trim();
                if (!dwjcIdMap.containsKey(sheetName)) {
                    msg = "单位名称（" + sheetName + "）不正确";
                    return msg;
                }

                // 判断归口部门名称是否都正确
                int lastRowNum = sheet.getLastRowNum();
                for (int j = 2; j < lastRowNum + 1; j++) {
                    Row row = sheet.getRow(j);
                    String gkbmMc = null;
                    if (null == row) {
                        continue;
                    } else {
                        gkbmMc = row.getCell(8).getStringCellValue();
                        System.out.println(gkbmMc);
                    }

                    if (StringUtils.hasLength(gkbmMc)) {
                        if (!gkbmMap.containsKey(gkbmMc)) {
                            msg = "第" + (i + 1) + "个sheet页面归口部门名称（" + gkbmMc + "）不正确";
                            return msg;
                        }
                    } else {
                        msg = "第" + (i + 1) + "个sheet页面归口部门名称有空值";
                        return msg;
                    }
                }

            }
        }

        // 创建三个List分别存放导入的数据
        List<GjyjGjzb> gjyjGjzbList = new LinkedList<>();
        List<GjyjDwlxZb> gjyjDwlxZbList = new LinkedList<>();
        List<GjyjZb> gjyjZbList = new LinkedList<>();

        // 定义一个Map用来存放除供电公司类之外的单位类型的归集指标名称和ID
        Map<String, Integer> gjzbGjidMap = new HashMap<>();
        // 定义一个Map用来存放单位类型名称和单位类型指标
        Map<String, Integer> dwlxzbDwlxIdMap = new HashMap<>();
        // 定义一个Map用来存放指标名称和归集指标ID
        Set<Integer> gjzbIdSet = new HashSet<>();
        Map<String, Set<Integer>> dwlxzbGjzbId = new HashMap<>();

        // 序列
        Sequence sequence = new Sequence();

        for (int i = 0; i < totalSheetNum; i++) {
            // 第一个sheet页为供电公司类
            if (i == 0) {
                // 获取单位类型名称
                String sheetName = sheet0.getSheetName().trim();
                // 获取最后一列是第几列
                Row row1 = sheet0.getRow(1);
                short lastCellNum = row1.getLastCellNum();
                // 获取最后一行是第几行
                int lastRowNum = sheet0.getLastRowNum();
                // 获取单位类型ID
                int dwlxId = dwlxMap.get(sheetName);

                Integer gjzbId = null;

                for (int j = 3; j < lastRowNum + 1; j++) {
                    Row row = sheet0.getRow(j);

                    String gjzbMc = row.getCell(1).getStringCellValue();// 归集指标名称
                    String zbMc = row.getCell(2).getStringCellValue();// 指标名称
                    String jlDw = row.getCell(3).getStringCellValue();// 计量单位
                    String sfYdJk = row.getCell(4).getStringCellValue();// 是否月度监控
                    String sfJdCy = row.getCell(5).getStringCellValue();// 是否季度差异
                    String khZq = row.getCell(6).getStringCellValue();// 考核周期
                    String khQz = dataFormatter.formatCellValue(row.getCell(7));
                    // String khQz = row.getCell(7).getStringCellValue();// 考核权重分值
                    String gkBmMc = row.getCell(8).getStringCellValue().trim();// 归口部门名称
                    // 首先插入T_JX_GJZB,判断当前gjzbMc是否有值
                    if (StringUtils.hasLength(gjzbMc)) {

                        GjyjGjzb gjzb = new GjyjGjzb();
                        gjzbId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);

                        gjzb.setId(gjzbId);
                        gjzb.setDwlxId(dwlxId);
                        gjzb.setGjzbMc(gjzbMc);

                        gjyjGjzbList.add(gjzb);
                    }
                    // 第二插入T_JX_DWLXZB
                    GjyjDwlxZb dwlxZb = new GjyjDwlxZb();
                    Integer dwlxZbId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);

                    dwlxZb.setId(dwlxZbId);
                    dwlxZb.setZbMc(zbMc);
                    dwlxZb.setGjzbId(gjzbId);
                    dwlxZb.setDwlxId(dwlxId);
                    dwlxZb.setJlDw(jlDw);
                    dwlxZb.setSfYdJk(sfYdJk);
                    dwlxZb.setSfJdCy(sfJdCy);
                    dwlxZb.setKhZq(khZq);
                    dwlxZb.setGkbmId(gkbmMap.get(gkBmMc));

                    gjyjDwlxZbList.add(dwlxZb);

                    // 循环是单位的列
                    for (int k = 9; k < lastCellNum; k++) {
                        String dfSx = dataFormatter.formatCellValue(row.getCell(k));
                        // String dfSx = row.getCell(k).getStringCellValue();
                        Integer id = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                        String dwjc = row1.getCell(k).getStringCellValue().trim();
                        Integer dwid = dwjcIdMap.get(dwjc);

                        GjyjZb zb = new GjyjZb();
                        zb.setId(id);
                        zb.setDwlxZbId(dwlxZbId);
                        zb.setDwId(dwid);
                        if (StringUtils.hasLength(khQz)) {
                            zb.setKhFz(Double.valueOf(khQz));
                        }
                        if (StringUtils.hasLength(dfSx)) {
                            zb.setDfSx(Integer.valueOf(dfSx));
                        }
                        gjyjZbList.add(zb);
                    }
                }
            } else {
                // 根据sheet页单位名称获取单位类型
                Sheet sheet = workbook.getSheetAt(i);
                String sheetName = sheet.getSheetName().trim();
                Integer dwlxId = dwjcDwlxMap.get(sheetName.trim());

                // 获取最后一行是第几行
                int lastRowNum = sheet.getLastRowNum();

                Integer gjzbId = null;
                for (int j = 2; j < lastRowNum + 1; j++) {
                    Row row = sheet.getRow(j);

                    if (null == row) {
                        continue;
                    }

                    String gjzbMc = row.getCell(1).getStringCellValue();// 归集指标名称
                    String zbMc = row.getCell(2).getStringCellValue();// 指标名称
                    String jlDw = row.getCell(3).getStringCellValue();// 计量单位
                    String sfYdJk = row.getCell(4).getStringCellValue();// 是否月度监控
                    String sfJdCy = row.getCell(5).getStringCellValue();// 是否季度差异
                    String khZq = row.getCell(6).getStringCellValue();// 考核周期
                    String khQz = dataFormatter.formatCellValue(row.getCell(7));
                    // String khQz = row.getCell(7).getStringCellValue();// 考核权重分值
                    String gkBmMc = row.getCell(8).getStringCellValue();// 归口部门名称
                    String dfSx = dataFormatter.formatCellValue(row.getCell(9));
                    // String dfSx = row.getCell(9).getStringCellValue();// 打分上线

                    // 归集指标名称能取到值,并且在集合中不存在
                    if (StringUtils.hasLength(gjzbMc)) {
                        if (gjzbGjidMap.containsKey(gjzbMc.trim())) {
                            gjzbId = gjzbGjidMap.get(gjzbMc.trim());
                        } else {
                            gjzbId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                            gjzbGjidMap.put(gjzbMc.trim(), gjzbId);

                            // 首先插入T_JX_GJZB
                            GjyjGjzb gjzb = new GjyjGjzb();
                            gjzb.setId(gjzbId);
                            gjzb.setDwlxId(dwlxId);
                            gjzb.setGjzbMc(gjzbMc);
                            gjyjGjzbList.add(gjzb);
                        }

                    }

                    Integer dwlxZbId = null;
                    // 判断指标名称
                    if (dwlxzbDwlxIdMap.containsKey(zbMc.trim())) {
                        // 判断该条单位类型指标的归集指标是否已经存在，不存在证明是不同的单位类型指标
                        Set<Integer> gjzbIds = dwlxzbGjzbId.get(zbMc.trim());
                        // 判断已经存在的单位类型指标的归集ID是否和当前相等
                        if (!gjzbIds.contains(gjzbId)) {
                            dwlxZbId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                            dwlxzbDwlxIdMap.put(zbMc.trim(), dwlxZbId);
                            gjzbIdSet.add(gjzbId);
                            dwlxzbGjzbId.put(zbMc.trim(), gjzbIdSet);

                            // 第二插入T_JX_DWLXZB
                            GjyjDwlxZb dwlxZb = new GjyjDwlxZb();

                            dwlxZb.setId(dwlxZbId);
                            dwlxZb.setZbMc(zbMc);
                            dwlxZb.setGjzbId(gjzbId);
                            dwlxZb.setDwlxId(dwlxId);
                            dwlxZb.setJlDw(jlDw);
                            dwlxZb.setSfYdJk(sfYdJk);
                            dwlxZb.setSfJdCy(sfJdCy);
                            dwlxZb.setKhZq(khZq);
                            dwlxZb.setGkbmId(gkbmMap.get(gkBmMc.trim()));
                            gjyjDwlxZbList.add(dwlxZb);
                        } else {
                            dwlxZbId = dwlxzbDwlxIdMap.get(zbMc.trim());
                        }
                    } else {
                        dwlxZbId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                        dwlxzbDwlxIdMap.put(zbMc.trim(), dwlxZbId);
                        gjzbIdSet.add(gjzbId);
                        dwlxzbGjzbId.put(zbMc.trim(), gjzbIdSet);

                        // 第二插入T_JX_DWLXZB
                        GjyjDwlxZb dwlxZb = new GjyjDwlxZb();

                        dwlxZb.setId(dwlxZbId);
                        dwlxZb.setZbMc(zbMc);
                        dwlxZb.setGjzbId(gjzbId);
                        dwlxZb.setDwlxId(dwlxId);
                        dwlxZb.setJlDw(jlDw);
                        dwlxZb.setSfYdJk(sfYdJk);
                        dwlxZb.setSfJdCy(sfJdCy);
                        dwlxZb.setKhZq(khZq);
                        dwlxZb.setGkbmId(gkbmMap.get(gkBmMc.trim()));
                        gjyjDwlxZbList.add(dwlxZb);
                    }

                    Integer zbid = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                    Integer dwid = dwjcIdMap.get(sheetName);

                    GjyjZb zb = new GjyjZb();
                    zb.setId(zbid);
                    zb.setDwlxZbId(dwlxZbId);
                    zb.setDwId(dwid);
                    if (StringUtils.hasLength(khQz)) {
                        zb.setKhFz(Double.valueOf(khQz));
                    }
                    if (StringUtils.hasLength(dfSx)) {
                        zb.setDfSx(Integer.valueOf(dfSx));
                    }
                    gjyjZbList.add(zb);
                }
            }
        }

        // 先执行删除
        gjyjZbwhMapper.deleteGjYjZb(year);
        gjyjZbwhMapper.deleteGjYjDwlxZb(year);
        gjyjZbwhMapper.deleteGjyjGjzb(year);
        // 后执行插入
        gjyjZbwhMapper.insertGjyjGjzb(gjyjGjzbList);
        gjyjZbwhMapper.insertGjYjDwlxZb(gjyjDwlxZbList);
        gjyjZbwhMapper.insertGjYjZb(gjyjZbList);

        return msg;
    }


    /**
     * 一键启用指标
     */
    @Override
    public void updateZtToStart(Integer[] ids) {
        gjyjZbwhMapper.updateZtToStart(ids);
    }

    /**
     * 一键停用指标
     */
    @Override
    public void updateZtToStop(Integer[] ids) {
        gjyjZbwhMapper.updateZtToStop(ids);
    }
}
