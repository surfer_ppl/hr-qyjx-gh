package com.sgcc.yxygkh.edmpms.service;

import java.util.List;
import java.util.Map;

/**
 * 各种票据
 * @author Edward
 * @date 2021年9月23日
 */
public interface EdmPmsServiceI {
    public List<Map<String, Object>> selectTest();
}
