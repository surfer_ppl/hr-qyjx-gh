package com.sgcc.jdkh.pojo;

/**
 * 得分率配置实体类（T_JX_DFLPZ）
 * 
 * @author Edward
 * @date 2021年5月23日
 */
public class DflPz {
    private Integer id;
    private Integer year;
    private Integer jd;
    // 单位类型ID
    private Integer dwlxId;
    // 单位类型Mc
    private String dwlxMc;
    // 得分率上限
    private Double dflSx;
    // 得分率下限
    private Double dflXx;

    public DflPz() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public Double getDflSx() {
        return dflSx;
    }

    public void setDflSx(Double dflSx) {
        this.dflSx = dflSx;
    }

    public Double getDflXx() {
        return dflXx;
    }

    public void setDflXx(Double dflXx) {
        this.dflXx = dflXx;
    }

}
