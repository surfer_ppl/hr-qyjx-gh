package com.sgcc.dwlxwh.pojo;

/**
 * 单位类型实体类（T_JX_DWLX）
 * 
 * @author Edward
 *
 */
public class Dwlx {
    // ID
    private Integer id;
    // 单位类型名称
    private String dwlxMc;
    // 年度
    private Integer year;
    // 状态（0停用，1启用）
    private Integer zt;
    // 业绩责任书状态
    private Integer yjzrsZt;

    public Dwlx() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getZt() {
        return zt;
    }

    public void setZt(Integer zt) {
        this.zt = zt;
    }

    public Integer getYjzrsZt() {
        return yjzrsZt;
    }

    public void setYjzrsZt(Integer yjzrsZt) {
        this.yjzrsZt = yjzrsZt;
    }
}
