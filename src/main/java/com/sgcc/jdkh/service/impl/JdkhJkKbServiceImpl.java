package com.sgcc.jdkh.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.jdkh.mapper.JdkhJkKbMapper;
import com.sgcc.jdkh.service.JdkhJkKbServiceI;

/**
 * 看板展示细项相关
 * 
 * @author Edward
 * @date 2021年12月6日
 */
@Service
public class JdkhJkKbServiceImpl implements JdkhJkKbServiceI {

    @Autowired
    JdkhJkKbMapper jdkhJkKbMapper;

    /*
     * 查询关键业绩看板数据
     */
    @Override
    public List<Map<String, Object>> selectGjyjKbData(Integer year, Integer jd, Integer dwid) {
        List<Map<String, Object>> gjyjKbDatas = jdkhJkKbMapper.selectGjyjKbData(year, jd, dwid);
        return gjyjKbDatas;
    }

    /*
     * 查询专业工作考核看板数据
     */
    @Override
    public List<Map<String, Object>> selectZygzKbData(Integer year, Integer jd, Integer dwid) {
        List<Map<String, Object>> zygzKbDatas = jdkhJkKbMapper.selectZygzKbData(year, jd, dwid);
        return zygzKbDatas;
    }


}
