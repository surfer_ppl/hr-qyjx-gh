package com.sgcc.jdkh.service;

import java.util.List;

import com.sgcc.jdkh.pojo.JdKhJkZs;

/**
 * 季度考核监控展示接口
 * 
 * @author Edward
 * @date 2021/08/10
 */
public interface JdKhJkZsServiceI {
    
    /**
     * 查询季度考核监控展示
     * @param year
     * @param jd
     * @return
     */
    public List<JdKhJkZs> selectJdKhJkZs(Integer year, Integer jd, Integer dwlxId, Integer dwId) throws Exception; 
    
    /**
     * 启动季度考核同步
     * @param year
     * @param jd
     */
    public void onJdKhTb(Integer year,Integer jd);

}
