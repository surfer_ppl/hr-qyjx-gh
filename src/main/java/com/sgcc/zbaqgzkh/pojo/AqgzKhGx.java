package com.sgcc.zbaqgzkh.pojo;

/**
 * 安全工作考核指标关联表  T_JX_AQGZ_KHGX
 * @author Edward
 *
 */
public class AqgzKhGx {
    /**
     * id
     */
    private Integer id;
    /**
     * 指标ID
     */
    private Integer zbId;
    /**
     * 单位类型ID
     */
    private Integer dwlxId;
    /**
     * 单位ID
     */
    private Integer dwId;
    
    
    public AqgzKhGx() {
   
    }


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getZbId() {
        return zbId;
    }


    public void setZbId(Integer zbId) {
        this.zbId = zbId;
    }


    public Integer getDwlxId() {
        return dwlxId;
    }


    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }


    public Integer getDwId() {
        return dwId;
    }


    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }


    
}
