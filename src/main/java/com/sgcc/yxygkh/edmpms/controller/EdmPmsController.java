package com.sgcc.yxygkh.edmpms.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgcc.yxygkh.edmpms.service.EdmPmsServiceI;

/**
 * 各类票据
 * 
 * @author Edward
 * @date 2021年9月23日
 */

@RestController
@RequestMapping("/edmpms")
public class EdmPmsController {

    @Autowired
    EdmPmsServiceI edmPmsService;

    /**
     * 查询一线员工票据信息
     * 
     * @return
     */
    @RequestMapping(value = "/getYxYgPjXx", method = RequestMethod.POST)
    public List<Map<String, Object>> getYxYgPjXx() {

        List<Map<String, Object>> selectTest = edmPmsService.selectTest();
        return selectTest;

    }

}
