package com.sgcc.jcyb.service;

import com.alibaba.fastjson.JSONArray;
import com.sgcc.jcyb.pojo.JcYbCfZz;
import com.sgcc.jcyb.pojo.JcYbJlCf;
import com.sgcc.jcyb.pojo.JcYbTbRy;
import com.sgcc.jcyb.pojo.JcYbWfLdJl;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * 奖惩月报服务接口
 *
 * @Author Edward
 * @Date 2022/3/19
 */
public interface JcYbServiceI {
    /**
     * 导入奖惩月报处罚准则
     *
     * @param workbook workbook
     */
    public void importCfZz(Workbook workbook);

    /**
     * 查询处罚准则
     * @return
     */
    public List<JcYbCfZz> selectJcYbCfZz();


    /**
     * 查询填报人员信息
     * @param year
     * @param yd
     * @param tabFlag
     * @return
     */
    public List<JcYbTbRy> selectTbrys(Integer year, Integer yd, String tabFlag);

    /**
     * 查询纪律处分人员信息
     * @param zzId 组织树ID
     * @param year 年
     * @param yd 月
     * @param xm 姓名
     * @return
     */
    public List<JcYbJlCf> selectJlcfs(Integer zzId, Integer year, Integer yd, String xm);

    /**
     * 查询违反劳动人员信息
     * @param zzId 组织树ID
     * @param year 年
     * @param yd 月
     * @param xm 姓名
     * @return
     */
    public List<JcYbWfLdJl> selectWfLdJls(Integer zzId, Integer year, Integer yd, String xm);

    /**
     * 保存填报人员数据
     * @param ygIds 字符串ids
     * @param tabLx TAB类型（FLAG_JLCF：纪律处分备案；FLAG_LDJL：违反劳动纪律备案；）
     * @param year 年
     * @param yd 月
     */
    public void saveTbryData(String ygIds, String tabLx,Integer year,Integer yd);

    /**
     * 保存修改的数据——纪律处分备案
     *
     * @param jcYbJlCfList 要保存的数据
     */
    public void saveJlCfData(List<JcYbJlCf> jcYbJlCfList);

    /**
     * 保存修改的数据——违反劳动纪律备案
     *
     * @param jcYbWfLdJlList 要保存的数据
     */
    public void saveWfLdData(List<JcYbWfLdJl> jcYbWfLdJlList);
}
