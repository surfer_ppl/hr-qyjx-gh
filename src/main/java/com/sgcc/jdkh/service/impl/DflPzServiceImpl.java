package com.sgcc.jdkh.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.jdkh.mapper.DflPzMapper;
import com.sgcc.jdkh.pojo.DflPz;
import com.sgcc.jdkh.service.DflPzServiceI;

@Service
public class DflPzServiceImpl
implements DflPzServiceI {

    @Autowired
    private DflPzMapper dflPzMapper;

    /**
     * 查询得分率配置
     */
    @Override
    public List<DflPz> selectDflPzs(Integer jdOrNd, Integer year, Integer dwlxId) {

        List<DflPz> dflPzs = dflPzMapper.selectDflPzs(jdOrNd, year, dwlxId);

        return dflPzs;
    }
    
    /**
     * 新增得分率配置
     */
    @Override
    public void insertDflPz(Map<String, Object> paramsMap) {
        dflPzMapper.insertDflPz(paramsMap);
    }
    
    /**
     * 删除得分率配置
     */
    @Override
    public void deleteDflPz(Integer[] idsArr) {
        dflPzMapper.deleteDflPz(idsArr);
    }
    
    

    
    

}
