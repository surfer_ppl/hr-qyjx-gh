package com.sgcc.common.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.common.pojo.Sequence;


/**
 * 获取Sequence
 * 
 * @author Edward
 * @date 2021年6月8日
 */
@Mapper
public interface SequenceMapper {
    /**
     * 获取下一个序列的值
     * 
     * @param sequence
     *            序列名称，例如SEQ_QYJX
     * @return
     */
    // public Map<String, Object> updateNextSeqVal(String seqName);
    public Integer updateNextSeqVal(@Param("sequence") Sequence sequence);
}
