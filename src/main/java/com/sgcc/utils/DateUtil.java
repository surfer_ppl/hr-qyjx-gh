package com.sgcc.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;

public class DateUtil {
    /**
     * 显示年月日时分秒，例如 2020-08-11 09:51:53
     */
    public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /**
     * 仅显示年月日，例如 2020-08-11
     */
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    /**
     * 仅显示时分秒，例如 09:51:53
     */
    public static final String TIME_PATTERN = "HH:mm:ss";

    /**
     * 春天;
     */
    public static final Integer SPRING = 1;

    /**
     * 夏天;
     */
    public static final Integer SUMMER = 2;

    /**
     * 秋天;
     */
    public static final Integer AUTUMN = 3;

    /**
     * 冬天;
     */
    public static final Integer WINTER = 4;

    /**
     * 星期日;
     */
    public static final String SUNDAY = "星期日";

    /**
     * 星期一;
     */
    public static final String MONDAY = "星期一";

    /**
     * 星期二;
     */
    public static final String TUESDAY = "星期二";

    /**
     * 星期三;
     */
    public static final String WEDNESDAY = "星期三";

    /**
     * 星期四;
     */
    public static final String THURSDAY = "星期四";

    /**
     * 星期五;
     */
    public static final String FRIDAY = "星期五";

    /**
     * 星期六;
     */
    public static final String SATURDAY = "星期六";

    /**
     * 将时间转换为固定格式的字符串
     * 
     * @param temporal
     *            需要转化的日期时间
     * @param pattern
     *            时间格式
     * @return
     */
    public static String format(TemporalAccessor temporal, String pattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        String formatStr = dateTimeFormatter.format(temporal);
        return formatStr;
    }

    /**
     * 获取当前日期和时间字符串
     * 
     * @return
     */
    public static String getLocalDateTimeStr() {
        String format = format(LocalDateTime.now(), DATETIME_PATTERN);
        return format;
    }

    /**
     * 获取当前日期字符串
     * 
     * @return
     */
    public static String getLocalDateStr() {
        String format = format(LocalDate.now(), DATE_PATTERN);
        return format;
    }

    /**
     * 获取当前时间字符串
     * 
     * @return
     */
    public static String getLocalTimeStr() {
        String format = format(LocalTime.now(), TIME_PATTERN);
        return format;
    }

    /**
     * 获取当前日期星期字符串
     * 
     * @return String 例如：星期二
     */
    public static String getDayOfWeekStr() {
        String format = format(LocalDate.now(), "E");
        return format;
    }

    /**
     * 获取指定日期是星期几
     * 
     * @param localDate
     *            日期
     * @return
     */
    public static String getDayOfWeekStr(LocalDate localDate) {
        String[] weekOfDays = {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY};
        int dayOfWeek = localDate.getDayOfWeek().getValue() - 1;
        return weekOfDays[dayOfWeek];
    }

    /**
     * 日期时间字符串转换为日期时间
     * 
     * @param localDateTimeStr
     *            日期时间字符串
     * @param pattern
     *            格式
     * @return LocalDateTime 日期时间对象
     */
    public static LocalDateTime parseLocalDateTime(String localDateTimeStr, String pattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime localDateTime = LocalDateTime.parse(localDateTimeStr, dateTimeFormatter);
        return localDateTime;
    }

    /**
     * 日期字符串转换为日期
     * 
     * @param localDateTimeStr
     *            日期字符串
     * @param pattern
     *            格式
     * @return LocalDate 日期对象
     */
    public static LocalDate parseLocalDate(String localDateStr, String pattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        LocalDate localDate = LocalDate.parse(localDateStr, dateTimeFormatter);
        return localDate;
    }

    /**
     * 根据chronoUnit计算两个日期时间之间的相隔日期时间
     * 
     * @param start
     *            开始日期时间
     * @param end
     *            结束日期时间
     * @param chronoUnit
     *            日期时间单位 /ˈkrɑːnoʊ/
     * @return
     */
    public static long getChronoUnitBetween(LocalDateTime start, LocalDateTime end, ChronoUnit chronoUnit) {
        return Math.abs(start.until(end, chronoUnit));
    }

    /**
     * 根据chronoUnit计算两个日期之间相隔年数或月数或天数
     * 
     * @param start
     *            开始日期
     * @param end
     *            结束日期
     * @param chronoUnit
     *            日期时间单位,(ChronoUnit.YEARS,ChronoUnit.MONTHS,ChronoUnit.WEEKS,ChronoUnit.DAYS)
     * @return
     */
    public static long getChronoUnitBetween(LocalDate start, LocalDate end, ChronoUnit chronoUnit) {
        return Math.abs(start.until(end, chronoUnit));
    }

    /**
     * 获取指定日期时间加上指定数量日期时间单位之后的日期时间
     * 
     * @param localDateTime
     *            日期时间
     * @param num
     *            数量
     * @param chronoUnit
     *            日期时间单位(ChronoUnit.YEARS,ChronoUnit.MONTHS,ChronoUnit.WEEKS,ChronoUnit.DAYS)
     * @return
     */
    public static LocalDateTime plus(LocalDateTime localDateTime, int num, ChronoUnit chronoUnit) {
        LocalDateTime plus = localDateTime.plus(num, chronoUnit);
        return plus;
    }

    /**
     * 获取指定日期加上指定数量日期单位之后的日期
     * 
     * @param localDate
     *            日期
     * @param num
     *            数量
     * @param chronoUnit
     *            日期时间单位 (ChronoUnit.YEARS,ChronoUnit.MONTHS,ChronoUnit.WEEKS,ChronoUnit.DAYS)
     * @return
     */
    public static LocalDate plus(LocalDate localDate, int num, ChronoUnit chronoUnit) {
        LocalDate plus = localDate.plus(num, chronoUnit);
        return plus;
    }

    /**
     * 获取指定日期时间减去指定数量日期时间单位之后的日期时间
     * 
     * @param localDateTime
     *            日期时间
     * @param num
     *            数量
     * @param chronoUnit
     *            日期时间单位
     * @return
     */
    public static LocalDateTime minus(LocalDateTime localDateTime, int num, ChronoUnit chronoUnit) {
        LocalDateTime minus = localDateTime.minus(num, chronoUnit);
        return minus;
    }

    /**
     * 获取指定日期加上指定数量日期单位之后的日期
     * 
     * @param localDate
     *            日期
     * @param num
     *            数量
     * @param chronoUnit
     *            日期时间单位 (ChronoUnit.YEARS,ChronoUnit.MONTHS,ChronoUnit.WEEKS,ChronoUnit.DAYS)
     * @return
     */
    public static LocalDate minus(LocalDate localDate, int num, ChronoUnit chronoUnit) {
        LocalDate minus = localDate.minus(num, chronoUnit);
        return minus;
    }

}
