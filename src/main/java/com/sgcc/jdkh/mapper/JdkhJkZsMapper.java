package com.sgcc.jdkh.mapper;

import java.util.List;
import java.util.Map;

import com.sgcc.jdkh.pojo.GjyjJdKbDfb;
import com.sgcc.jdkh.pojo.JdKhJkZs;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 季度考核监控展示Mapper
 * 
 * @author Edward
 * @date 2021年6月18日
 */

@Mapper
public interface JdkhJkZsMapper {

    /**
     * 查询季度考核监控展示
     * 
     * @param year
     * @param jd
     * @return
     */
    public List<JdKhJkZs> selectJdKhJkZs(@Param("year") Integer year, @Param("jd") Integer jd,
        @Param("dwlxId") Integer dwlxId, @Param("dwId") Integer dwId);

    /**
     * 初始化监控表，用来插入
     * 
     * @param year
     * @param jd
     * @return
     */
    public List<JdKhJkZs> initJkzs(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 删除季度考核监控
     * 
     * @param year
     * @param jd
     * @return
     */
    public int deleteJkZs(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 插入季度考核监控
     * 
     * @param jdKhJkZsList
     * @return
     */
    public int insertJkZs(@Param("jdKhJkZsList") List<JdKhJkZs> jdKhJkZsList);

    /**
     * 删除键业绩指标看版表数据
     * 
     * @param year
     * @param jd
     * @return
     */
    public int deleteGjyjKbData(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 同步关键业绩打分表数据到看版表
     * 
     * @param year
     * @param jd
     * @return
     */
    public int insertGjyjKbData(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 删除专业工作考核看板数据
     * 
     * @param year
     * @param jd
     * @return
     */
    public int deleteZygzKhKbData(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 同步专业工作考核打分表数据到看版表
     * 
     * @param year
     * @param jd
     * @return
     */
    public int insertZygzKhKbData(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询小计得分
     * 
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectXjDf(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询关键业绩看板打分表数据
     * 
     * @param year
     * @param jd
     * @return
     */
    public List<GjyjJdKbDfb> selectGjyjKbDfData(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询关键业绩总加分和总扣分
     * 
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectGjyjZjfAndZkf(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询专业工作考核总得分，根据权重进行计算
     * 
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectZygzKhZdf(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询安全工作考核总得分
     * 
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectAqGzZdf(@Param("year") Integer year, @Param("jd") Integer jd);

    /**
     * 查询安全工作考核各个父指标得分细项
     * 
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectAqGzFzbDf(@Param("year") Integer year, @Param("jd") Integer jd);
}
