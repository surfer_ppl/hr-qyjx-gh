package com.sgcc.jdkh.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 季度考核监控看板Mapper
 * 
 * @author Edward
 * @date 2021年12月6日
 */

@Mapper
public interface JdkhJkKbMapper {

    /**
     * 查询关键业绩看板数据
     * 
     * @return
     */
    public List<Map<String, Object>> selectGjyjKbData(@Param("year") Integer year, @Param("jd") Integer jd,
        @Param("dwid") Integer dwid);

    /**
     * 查询专业工作考核看板数据
     * 
     * @return
     */
    public List<Map<String, Object>> selectZygzKbData(@Param("year") Integer year, @Param("jd") Integer jd,
        @Param("dwid") Integer dwid);
    

}
