package com.sgcc.zbaqgzkh.dto;

/**
 * 安全工作考核首页表格数据查询
 * 
 * @author Edward
 *
 */
public class AqgzKhCxDto {
    // id
    private Integer id;
    // 父指标名称
    private String fzbMc;
    // 指标名称
    private String zbMc;
    // 归口部门名称
    private String gkbmMc;
    // 单位类型名称
    private String dwlxMc;
    // 考核扣分标准
    private String khKfBz;
    // 考核周期
    private String khZq;

    public AqgzKhCxDto() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFzbMc() {
        return fzbMc;
    }

    public void setFzbMc(String fzbMc) {
        this.fzbMc = fzbMc;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public String getGkbmMc() {
        return gkbmMc;
    }

    public void setGkbmMc(String gkbmMc) {
        this.gkbmMc = gkbmMc;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public String getKhKfBz() {
        return khKfBz;
    }

    public void setKhKfBz(String khKfBz) {
        this.khKfBz = khKfBz;
    }

    public String getKhZq() {
        return khZq;
    }

    public void setKhZq(String khZq) {
        this.khZq = khZq;
    }

}
