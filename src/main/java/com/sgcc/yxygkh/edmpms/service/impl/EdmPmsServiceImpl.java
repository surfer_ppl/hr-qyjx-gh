package com.sgcc.yxygkh.edmpms.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.yxygkh.edmpms.mapper.EdmPmsMapper;
import com.sgcc.yxygkh.edmpms.service.EdmPmsServiceI;

@Service
public class EdmPmsServiceImpl implements EdmPmsServiceI {
    
    @Autowired
    EdmPmsMapper edmPmsMapper;
    
    @Override
    public List<Map<String, Object>> selectTest() {
        
        List<Map<String,Object>> selectTest = edmPmsMapper.selectTest();
        
        return selectTest;
    }

}
