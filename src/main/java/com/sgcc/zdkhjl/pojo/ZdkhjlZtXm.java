package com.sgcc.zdkhjl.pojo;

/**
 * 重大考核激励项——专题项目实体类（T_JX_ZXKHJL_ZTXM）
 *
 * @author Edward
 * @date 2022-3-1
 */
public class ZdkhjlZtXm {
    /**
     * ID
     */
    private Integer id;
    /**
     * 专题编号
     */
    private Integer ztBh;
    /**
     * 专题名称
     */
    private String ztMc;
    /**
     * 标准描述
     */
    private String bzMs;

    /**
     * 标准明细
     */
    private String bzMx;

    /**
     * 年度
     */
    private Integer year;

    /**
     * 季度
     */
    private Integer jd;


    public ZdkhjlZtXm() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getZtBh() {
        return ztBh;
    }

    public void setZtBh(Integer ztBh) {
        this.ztBh = ztBh;
    }

    public String getZtMc() {
        return ztMc;
    }

    public void setZtMc(String ztMc) {
        this.ztMc = ztMc;
    }

    public String getBzMs() {
        return bzMs;
    }

    public void setBzMs(String bzMs) {
        this.bzMs = bzMs;
    }

    public String getBzMx() {
        return bzMx;
    }

    public void setBzMx(String bzMx) {
        this.bzMx = bzMx;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer nd) {
        this.year = nd;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }
}
