package com.sgcc.common.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.sgcc.common.dto.Loginer;
import com.sgcc.common.pojo.YxygRolePz;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LoginMapper {
    /**
     * 查询登录人
     * 
     * @param account 账号
     * @param passWrod 密码
     * @return
     */
    public List<Loginer> selectLoginer(@Param("account") String account,@Param("passWrod") String passWrod);











    /***
     * 查询角色配置信息（一线员工）
     * 
     * @return
     */
    public List<YxygRolePz> selectRolePz();

    /**
     * 查询登录人考核关系（一线员工）
     * 
     * @param loginerId
     * @return
     */
    public List<Map<String, Object>> selectKhgx(Integer loginerId);
    
    /**
     * 查询待办信息
     * @return
     */
    public List<Map<String, Object>> selectDbXx();
}
