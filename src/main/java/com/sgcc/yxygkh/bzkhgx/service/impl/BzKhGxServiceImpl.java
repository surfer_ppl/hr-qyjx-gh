package com.sgcc.yxygkh.bzkhgx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.yxygkh.bzkhgx.dto.ByXx;
import com.sgcc.yxygkh.bzkhgx.dto.BzXx;
import com.sgcc.yxygkh.bzkhgx.dto.GqXx;
import com.sgcc.yxygkh.bzkhgx.mapper.BzKhGxMapper;
import com.sgcc.yxygkh.bzkhgx.service.BzKhGxServiceI;

/**
 * 工区班组考核关系实现类
 * 
 * @author Terrans Force
 * @date 2021/08/30
 */
@Service
public class BzKhGxServiceImpl implements BzKhGxServiceI {

    @Autowired
    BzKhGxMapper bzKhGxMapper;

    /**
     * 添加班组到工区
     */
    @Override
    public void insertBz(Integer gqId,Integer[] bzidArr) {
        bzKhGxMapper.insertBz(gqId, bzidArr);
    }

    /**
     * 删除工区
     */
    @Override
    public void deleteBz(Integer[] bzidArr) {
        bzKhGxMapper.deleteBz(bzidArr);
    }

    /**
     * 修改工区名称
     */
    @Override
    public void updateGqMc(Integer gqId,String gqMc) {
        bzKhGxMapper.updateGqMc(gqId, gqMc);
    }
    
    /**
     * 添加自定义工区 
     */
    @Override
    public void insertZdyGq(Integer dwId,String gqMc) {
        bzKhGxMapper.insertZdyGq(dwId, gqMc);
    }
    
    /**
     * 查询工区信息
     * 
     * @param dwId
     * @return
     */
    @Override
    public List<GqXx> selectGqXxs(int dwId) {
        List<GqXx> gqXxs = bzKhGxMapper.selectGqXxs(dwId);

        return gqXxs;
    }

    /**
     * 查询班组信息
     * 
     * @param gqId
     * @return
     */
    @Override
    public List<BzXx> selectBzXxs(int gqId) {
        List<BzXx> bzXxs = bzKhGxMapper.selectBzXxs(gqId);
        return bzXxs;
    }

    /**
     * 查询班员信息
     * 
     * @param bzId
     * @return
     */
    @Override
    public List<ByXx> selectByXxs(int bzId) {
        List<ByXx> byXxs = bzKhGxMapper.selectByXxs(bzId);
        return byXxs;

    }
}
