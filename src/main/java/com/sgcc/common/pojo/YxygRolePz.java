package com.sgcc.common.pojo;

/**
 * 一线与员工角色配置（T_JX_YXYG_ROLEPZ）
 * 
 * @author Edward
 * @date 2021年9月21日
 */
public class YxygRolePz {
    // 主键ID
    private Integer id;
    // 角色编码
    private String roleCode;
    // 角色名称
    private String roleName;

    public YxygRolePz() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
