package com.sgcc.yxygkh.bzkhgx.dto;

/**
 * 工区信息
 * 
 * @author Terrans Force
 * @date 2021/08/30
 */
public class GqXx {
    // ID
    private Integer id;
    // 工区名称
    private String gqMc;
    // 人资编码
    private Integer rzBm;
    // 姓名
    private String gqFzrXm;
    // 部门名称
    private String bmMc;
    // 岗位名称
    private String gwMc;

    public GqXx() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGqMc() {
        return gqMc;
    }

    public void setGqMc(String gqMc) {
        this.gqMc = gqMc;
    }

    public Integer getRzBm() {
        return rzBm;
    }

    public void setRzBm(Integer rzBm) {
        this.rzBm = rzBm;
    }

    public String getGqFzrXm() {
        return gqFzrXm;
    }

    public void setGqFzrXm(String gqFzrXm) {
        this.gqFzrXm = gqFzrXm;
    }

    public String getBmMc() {
        return bmMc;
    }

    public void setBmMc(String bmMc) {
        this.bmMc = bmMc;
    }

    public String getGwMc() {
        return gwMc;
    }

    public void setGwMc(String gwMc) {
        this.gwMc = gwMc;
    }

}
