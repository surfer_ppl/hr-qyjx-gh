package com.sgcc.zbzygzkh.pojo;

/**
 * 专业工作考核关系实体类（T_JX_ZYGZ_KHGX）
 * @author Edward
 *
 */
public class ZygzKhGx {
    //ID
    Integer id;
    //指标ID
    Integer zbId;
    //单位ID
    Integer dwId;
    
    public ZygzKhGx() {
        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getZbId() {
        return zbId;
    }

    public void setZbId(Integer zbId) {
        this.zbId = zbId;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }
}
