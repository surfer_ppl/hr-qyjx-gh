package com.sgcc.yxygkh.bzzkh.pojo;

/**
 * 考核周期实体类（T_JX_YXYG_KHZQ）
 * @author Edward
 * @date 2021年9月6日
 */
public class KhZq {
    private Integer id;
    // 工区ID
    private Integer gqId;
    // 班组ID
    private Integer bzId;
    // 周期类型（1，季度，2月度）
    private Integer zqLx;
    // 考核周期（01 02 1 2 ）
    private String khZq;
    // 考核状态（0 未完成,1已完成）
    private Integer khZt;
    // 考核人ID
    private Integer khrId;
    // 考核完成时间
    private String khwcSj;
    // 年度
    private Integer year;

    public KhZq() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGqId() {
        return gqId;
    }

    public void setGqId(Integer gqId) {
        this.gqId = gqId;
    }

    public Integer getBzId() {
        return bzId;
    }

    public void setBzId(Integer bzId) {
        this.bzId = bzId;
    }

    public Integer getZqLx() {
        return zqLx;
    }

    public void setZqLx(Integer zqLx) {
        this.zqLx = zqLx;
    }

    public String getKhZq() {
        return khZq;
    }

    public void setKhZq(String khZq) {
        this.khZq = khZq;
    }

    public Integer getKhZt() {
        return khZt;
    }

    public void setKhZt(Integer khZt) {
        this.khZt = khZt;
    }

    public Integer getKhrId() {
        return khrId;
    }

    public void setKhrId(Integer khrId) {
        this.khrId = khrId;
    }

    public String getKhwcSj() {
        return khwcSj;
    }

    public void setKhwcSj(String khwcSj) {
        this.khwcSj = khwcSj;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

}
