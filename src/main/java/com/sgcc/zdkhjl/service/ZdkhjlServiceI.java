package com.sgcc.zdkhjl.service;

import com.sgcc.common.pojo.CommonCode;
import com.sgcc.zdkhjl.dto.ZdkhjlDwMxDto;
import com.sgcc.zdkhjl.dto.ZdkhjlKbZsDto;
import com.sgcc.zdkhjl.pojo.ZdkhKbFg;
import com.sgcc.zdkhjl.pojo.ZdkhjlDwMx;
import com.sgcc.zdkhjl.pojo.ZdkhjlZtXm;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * 重大考核激励项Service
 *
 * @author Edward
 * @date 2022-3-1
 */
public interface ZdkhjlServiceI {
    /**
     * 导入重大考核激励
     *
     * @param year
     * @param workbook
     * @return
     */
    public String importZdkhjl(int year, int jd, Workbook workbook);

    /**
     * 查询表格内容
     *
     * @param year
     * @param jd
     * @return
     */
    public List<ZdkhjlDwMxDto> selectGridData(Integer year, Integer jd);

    /**
     * 查询看板展示
     *
     * @param year
     * @param jd
     * @return
     */
    public List<ZdkhjlKbZsDto> selectKbZsData(Integer year, Integer jd);


    /**
     * 查询专题明细根据ID
     *
     * @param id 表ID
     * @return ZdkhjlZtXm
     */
    public ZdkhjlZtXm selectZtXmById(Integer id);

    /**
     * 查询单位明细根据ID
     *
     * @param id 表ID
     * @return ZdkhjlDwMx
     */
    public ZdkhjlDwMx selectDwMxById(Integer id);


    /**
     * 查询已定看板风格
     *
     * @return ZdkhKbFg
     */
    public ZdkhKbFg selectFg();

    /**
     * 保存看板风格
     * @param styleCode
     * @return
     */
    public void updateKbFg(String styleCode);


}
