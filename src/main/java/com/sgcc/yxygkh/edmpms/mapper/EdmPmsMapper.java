package com.sgcc.yxygkh.edmpms.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.dynamic.datasource.annotation.DS;

/**
 * 各种票据的查询
 * @author Edward
 * @date 2021年9月23日
 */

@DS("db_oracle")
@Mapper
public interface EdmPmsMapper {
    
    public List<Map<String, Object>> selectTest();
}
