package com.sgcc.zbgjyj.pojo;

/**
 * 关键业绩归集指标实体类（T_JX_GJYJ_GJZB）
 * 
 * @author Edward
 * 
 */
public class GjyjGjzb {

    // ID
    private Integer id;
    // 单位类型ID
    private Integer dwlxId;
    // 归集指标名称
    private String gjzbMc;

    public GjyjGjzb() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public String getGjzbMc() {
        return gjzbMc;
    }

    public void setGjzbMc(String gjzbMc) {
        this.gjzbMc = gjzbMc;
    }

    @Override
    public String toString() {
        return "GjyjGjzb [id=" + id + ", dwlxId=" + dwlxId + ", gjzbMc=" + gjzbMc + "]";
    }
    
    

}
