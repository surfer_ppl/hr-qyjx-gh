package com.sgcc.jdkh.dto;

/**
 * 安全工作考核打分Dto
 * @author Edward
 * @date 2021年7月1日
 */
public class AqgzDbDfDto {
    // ID
    private Integer id;
    //父指标名称
    private String fzbMc;
    // 指标名称
    private String zbMc;
    // 归口部门名称
    private String gkbmMc;
    // 单位类型名称
    private String dwlxMc;
    // 单位名称
    private String dwMc;
    //考核周期
    private String khZq;
    // 扣分
    private Double kf;
    // 扣分理由
    private String kfLy;
    
    public AqgzDbDfDto() {

    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getFzbMc() {
        return fzbMc;
    }
    public void setFzbMc(String fzbMc) {
        this.fzbMc = fzbMc;
    }
    public String getZbMc() {
        return zbMc;
    }
    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }
    public String getGkbmMc() {
        return gkbmMc;
    }
    public void setGkbmMc(String gkbmMc) {
        this.gkbmMc = gkbmMc;
    }
    public String getDwlxMc() {
        return dwlxMc;
    }
    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }
    public String getDwMc() {
        return dwMc;
    }
    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }
    public String getKhZq() {
        return khZq;
    }
    public void setKhZq(String khZq) {
        this.khZq = khZq;
    }
    public Double getKf() {
        return kf;
    }
    public void setKf(Double kf) {
        this.kf = kf;
    }
    public String getKfLy() {
        return kfLy;
    }
    public void setKfLy(String kfLy) {
        this.kfLy = kfLy;
    }
}
