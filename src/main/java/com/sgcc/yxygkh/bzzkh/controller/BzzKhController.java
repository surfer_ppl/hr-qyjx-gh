package com.sgcc.yxygkh.bzzkh.controller;

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 班组长相关操作
 * 
 * @author Edward
 * @date 2021年9月6日
 */
@RestController
@RequestMapping("/bzzKh")
public class BzzKhController {
    
    /**
     * 班组长考核设置
     * @param bzId 班组ID
     * @param zqLx 周期类型（1，季度，2，月度）
     * @return
     */
    public Map<String, String> bzzKhSz(Integer bzId,Integer zqLx) {
        Map<String, String> msgMap = new HashedMap<>();
        
        return msgMap;
    }
}
