package com.sgcc.jdkh.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.jdkh.pojo.JdKhJkZs;
import com.sgcc.jdkh.service.JdKhJkZsServiceI;
import com.sgcc.utils.LayUiTableReponseMapUtil;

/**
 * 监控展示——季度考核
 * 
 * @author Edward
 * @date 2021年5月23日
 */
@RestController
@RequestMapping("/jdkhJkZs")
public class JdkhJkZsController {

    @Autowired
    JdKhJkZsServiceI jdKhJkZsService;

    /**
     * 查询季度考核监控数据
     * 
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/getJdkhJkZsDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getJdkhJkZsDataGrid(Integer year, Integer jd, Integer dwlxId, Integer page,
        Integer limit) {

        PageHelper.startPage(page, limit);

        List<JdKhJkZs> resultList = null;
        try {
            resultList = jdKhJkZsService.selectJdKhJkZs(year, jd, dwlxId,null);

            PageInfo<JdKhJkZs> pageInfo = new PageInfo<JdKhJkZs>(resultList);
            long total = pageInfo.getTotal();

            Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, total);

            return resultMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 同步季度考核监控数据
     * 
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/tbJdkhJkData", method = RequestMethod.POST)
    public Map<String, String> tbJdkhJkData(Integer year, Integer jd) {
        Map<String, String> msgMap = new HashMap<>();

        try {

            // 执行同步
            jdKhJkZsService.onJdKhTb(year, jd);

            msgMap.put("flag", "true");
            msgMap.put("msg", "同步成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "同步失败，请重试！");
            return msgMap;
        }

    }

}
