package com.sgcc.common.service.impl;

import com.sgcc.common.mapper.CommonCodeMapper;
import com.sgcc.common.pojo.CommonCode;
import com.sgcc.common.service.CommonCodeServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Edward
 * @Date 2022/3/21
 */
@Service
public class CommonCodeServiceImpl implements CommonCodeServiceI {

    @Autowired
    CommonCodeMapper commonCodeMapper;

    /**
     * 根据类型编码查询公共代码
     * @param codeTypeBm
     * @return
     */
    public List<CommonCode> selectCommonCode(String codeTypeBm){
        List<CommonCode> commonCodes = commonCodeMapper.selectCommonCode(codeTypeBm);
        return  commonCodes;
    }
}
