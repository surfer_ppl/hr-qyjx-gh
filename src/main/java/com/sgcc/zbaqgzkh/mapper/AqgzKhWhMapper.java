package com.sgcc.zbaqgzkh.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.zbaqgzkh.dto.AqgzKhCxDto;
import com.sgcc.zbaqgzkh.dto.DwlxAndDwsDto;
import com.sgcc.zbaqgzkh.pojo.AqgzKhDwlx;
import com.sgcc.zbaqgzkh.pojo.AqgzKhDwlxGl;
import com.sgcc.zbaqgzkh.pojo.AqgzKhFzb;
import com.sgcc.zbaqgzkh.pojo.AqgzKhGx;
import com.sgcc.zbaqgzkh.pojo.AqgzKhZb;

/**
 * 安全工作考核页面维护相关
 * 
 * @author Edward
 *
 */
@Mapper
public interface AqgzKhWhMapper {

    /**
     * 安全工作考核首页表格数据查询
     * 
     * @param year
     * @param dwlxId
     * @return
     */
    public List<AqgzKhCxDto> selectAqgzKhDataGrid(@Param("year") Integer year, Integer dwlxId);

    /**
     * 查询安全工作考核单位类型
     * 
     * @param year
     * @return
     */
    public List<AqgzKhDwlx> selectAqgzKhDwlxs(@Param("year") Integer year);

    /**
     * 插入安全工作考核单位类型
     * 
     * @param aqgzKhDwlxList
     * @return
     */
    public int insertAqgzKhDwlx(AqgzKhDwlx aqgzKhDwlx);

    /**
     * 插入安全工作考核单位类型和单位关联
     * 
     * @param aqgzKhDwlxglList
     * @return
     */
    public int insertAqgzKhDwlxGl(@Param("aqgzKhDwlxGlList") List<AqgzKhDwlxGl> aqgzKhDwlxGlList);

    /**
     * 查询安全工作考核单位类型和单位关联
     * 
     * @param year
     * @return
     */
    public List<Map<String, Object>> selectAqgzKhDwlxGl(@Param("year") Integer year, @Param("dwlxId") Integer dwlxId,
        @Param("dwMc") String dwMc);

    /**
     * 根据单位类型查对应的单位 dwlxId-dwIds
     * 
     * @param dwlxId
     * @return
     */
    public DwlxAndDwsDto selectDwlxAndDws(int year, int dwlxId);

    /**
     * 插入安全工作考核父指标
     * 
     * @param aqgzKhFzbList
     * @return
     */
    public int insertAqgzKhFzb(@Param("aqgzKhFzbList") List<AqgzKhFzb> aqgzKhFzbList);

    /**
     * 插入安全工作考核指标
     * 
     * @param aqgzKhZbList
     * @return
     */
    public int insertAqgzKhZb(@Param("aqgzKhZbList") List<AqgzKhZb> aqgzKhZbList);

    /**
     * 插入安全工作考核关系
     * 
     * @param aqgzKhZbglList
     * @return
     */
    public int insertAqgzKhGx(@Param("aqgzKhGxList") List<AqgzKhGx> aqgzKhGxList);

    /**
     * 查询安全工作考核父指标
     * 
     * @param year
     * @return
     */
    public List<Map<String, Object>> selectAqgzKhFzb(Integer year);
}
