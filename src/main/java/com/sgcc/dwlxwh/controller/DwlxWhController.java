package com.sgcc.dwlxwh.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.dwlxwh.dto.DwjcWhDto;
import com.sgcc.dwlxwh.dto.DwlxCxDto;
import com.sgcc.dwlxwh.pojo.DwjcWh;
import com.sgcc.dwlxwh.pojo.Dwlx;
import com.sgcc.dwlxwh.service.DwlxWhServiceI;
import com.sgcc.utils.LayUiTableReponseMapUtil;
/**
 * 单位类型维护Controller
 *
 * @Author Edward
 * @Date 2021/6/10
 */
@RestController
@RequestMapping("/dwlxWh")
public class DwlxWhController {

    @Autowired
    DwlxWhServiceI dwlxWhService;

    /**
     * 根据年度查询单位类型
     * 
     * @param year
     * @return
     */
    @RequestMapping(value = "/getDwlx", method = RequestMethod.POST)
    public List<Dwlx> getDwlx(Integer year) {
        List<Dwlx> dwlxList = dwlxWhService.selectDwlx(year);

        return dwlxList;
    }

    /**
     * 单位类型维护首页查询
     * 
     * @param year
     * @return
     */
    @RequestMapping(value = "/getDwlxDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getDwlxDataGrid(Integer year, Integer dwlxId, Integer page, Integer limit) {

        PageHelper.startPage(page, limit);

        List<DwlxCxDto> dwlxDataGrid = dwlxWhService.selectDwlxDataGrid(year, dwlxId);

        PageInfo<DwlxCxDto> pageInfo = new PageInfo<DwlxCxDto>(dwlxDataGrid);
        long total = pageInfo.getTotal();

        // 查询Tbale使用LayUI固定的返回格式
        Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(dwlxDataGrid, total);

        return resultMap;
    }

    /**
     * 添加单位类型和关联单位
     * 
     * @param dwlxMc
     * @param dwids
     * @param year
     * @return
     */
    @RequestMapping(value = "/addDwlxAndGldw", method = RequestMethod.POST)
    public Map<String, String> addDwlxAndGldw(String dwlxMc, Integer[] dwids, Integer year) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            dwlxWhService.addDwlxAndGldw(dwlxMc, dwids, year);
            msgMap.put("flag", "true");
            msgMap.put("msg", "新增成功");
        } catch (RuntimeException e) {
            msgMap.put("flag", "flase");
            msgMap.put("msg", "新增失败");
            e.printStackTrace();
        }
        return msgMap;
    }

    /**
     * 删除单位类型和单位类型关联
     * 
     * @param dwlxIds
     * @return
     */
    @RequestMapping(value = "/removeDwlxAndGldw", method = RequestMethod.POST)
    public Map<String, String> removeDwlxAndGldw(Integer[] dwlxIds) {
        Map<String, String> msgMap = new HashMap<>();

        List<Integer> dwlxList = Arrays.asList(dwlxIds);

        try {
            dwlxWhService.deleteDwlxAndGldw(dwlxList);
            msgMap.put("flag", "true");
            msgMap.put("msg", "删除成功");
        } catch (RuntimeException e) {
            msgMap.put("flag", "flase");
            msgMap.put("msg", "删除失败");
            e.printStackTrace();
        }
        return msgMap;
    }

    /**
     * 修改单位类型和关联单位
     * 
     * @param dwlxId
     * @param dwids
     * @param year
     * @return
     */
    @RequestMapping(value = "/editDwlxAndGldw", method = RequestMethod.POST)
    public Map<String, String> editDwlxAndGldw(Integer dwlxId, Integer[] dwids, Integer year) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            dwlxWhService.editDwlxAndGldw(dwlxId, dwids, year);
            msgMap.put("flag", "true");
            msgMap.put("msg", "修改成功");
        } catch (RuntimeException e) {
            msgMap.put("flag", "flase");
            msgMap.put("msg", "修改失败");
            e.printStackTrace();
        }
        return msgMap;
    }

    /**
     * 查看数据；查看单位简称维护
     * 
     * @param dwlxId
     * @param year
     * @return
     */
    @RequestMapping(value = "/getCheckOrDwjcWh", method = RequestMethod.POST)
    public Map<String, Object> getCheckOrDwjcWh(Integer dwlxId, Integer year) {
        List<DwjcWhDto> dwjcWhDtoList = dwlxWhService.selectDwjcWhDto(year, dwlxId);

        Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(dwjcWhDtoList, null);

        return resultMap;
    }

    /**
     * 导出单位类型维护数据
     * 
     * @param year
     */
    @RequestMapping(value = "/exportDwlxWh", method = RequestMethod.POST)
    public void exportDwlxWh(Integer year, HttpServletResponse response) {

        // 读取模板
        String filePath = "com/sgcc/dwlxwh/controller/export_dwlxwh.xlsx";
        URL resource = this.getClass().getClassLoader().getResource(filePath);
        InputStream openStream = null;
        Workbook workbook = null;
        try {
            openStream = resource.openStream();
            workbook = new XSSFWorkbook(openStream);
            dwlxWhService.exportDwjcWh(workbook, year);
            // 设置文件名
            String fileName = "附件:单位类型与关联单位.xlsx";
            // 设置mime类型
            String mimeType = "application/x-msdownload";
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition",
                "attachment;filename=" + new String(fileName.getBytes("utf-8"), "ISO_8859_1"));
            // 开启响应输出流
            ServletOutputStream outputStream = response.getOutputStream();
            workbook.write(outputStream);
            // 推出输出流，并关闭相关流
            outputStream.flush();
            workbook.close();
            outputStream.close();
            openStream.close();

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    /**
     * 维护和修改单位简称
     * 
     * @param ids
     * @param dwjcs
     * @return
     */
    @RequestMapping(value = "/whDwJc", method = RequestMethod.POST)
    public Map<String, String> whDwJc(Integer[] ids, String[] dwjcs) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            Integer effectedRow = dwlxWhService.updateDwjc(ids, dwjcs);
            if (effectedRow > 0) {
                msgMap.put("flag", "true");
                msgMap.put("msg", "保存成功");
            } else {
                msgMap.put("flag", "false");
                msgMap.put("msg", "保存失败");
            }

        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "保存失败");
            return msgMap;
        }
        return msgMap;
    }

    /**
     * 启用/停用 单位类型
     * 
     * @param year
     * @return
     */
    @RequestMapping(value = "/startOrStopDwlx", method = RequestMethod.POST)
    public Map<String, String> startOrStopDwlx(Integer year, Integer zt) {
        Map<String, String> msgMap = new HashMap<>();
        try {
            dwlxWhService.updateDwlx(year, zt);
            msgMap.put("flag", "true");
            msgMap.put("msg", "启用成功");
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "启用失败");
            return msgMap;
        }
        return msgMap;
    }
    
    /**
     * 根据年份和单位类型获取单位简称信息
     * @return
     */
    @RequestMapping(value = "/getDw", method = RequestMethod.POST)
    public List<DwjcWh> getDw(Integer year,Integer dwlxId){
        List<DwjcWh> resultList = dwlxWhService.selectDwjc(year, dwlxId);
        return resultList;
    }
}
