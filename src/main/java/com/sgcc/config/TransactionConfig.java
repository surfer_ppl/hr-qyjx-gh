package com.sgcc.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.interceptor.NameMatchTransactionAttributeSource;
import org.springframework.transaction.interceptor.RuleBasedTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.transaction.interceptor.TransactionInterceptor;

/**
 * 全局事务配置类，@Configuration，注释的类，相当于之前的xml配置文件
 * 
 * @author Edward
 *
 */
@Configuration
public class TransactionConfig {

    /**
     * 自动注入数据源
     */
    @Autowired
    DataSource dataSource;

    // 事务方法超时时间设置
    //private static final int TX_METHOD_TIMEOUT = 10;
    // AOP切面的切点表达式
    private static final String AOP_POINTCUT_EXPRESSION = "execution (* com.sgcc.*.service.*.*(..))";

    /**
     * 向容器中注入事务管理器 注意，要给该bean取一个名字，名字不要是transactionManager，否则会报错
     * 
     * @return
     */
    @Bean("tsManager")
    public DataSourceTransactionManager transactionManager() {
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        dataSourceTransactionManager.setDataSource(dataSource);
        return dataSourceTransactionManager;
    }

    /**
     * 配置事务拦截器 注意，要给该bean取一个名字，名字不要是transactionInterceptor，否则会报错
     * 
     * @param tsManager
     * @return
     */
    @Bean("txAdvice")
    public TransactionInterceptor transactionInterceptor(TransactionManager tsManager) {

        NameMatchTransactionAttributeSource source = new NameMatchTransactionAttributeSource();

        // 非只读事务
        RuleBasedTransactionAttribute requiredTx = new RuleBasedTransactionAttribute();
        // 设置传播行为
        requiredTx.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        // 只读事务
        RuleBasedTransactionAttribute readOnlyTx = new RuleBasedTransactionAttribute();
        readOnlyTx.setReadOnly(true);

        // 配置加事务的规则,没有匹配到的方法将不会有事务，这些方法指的是Pointcut匹配到的方法
        Map<String, TransactionAttribute> map = new HashMap<>();
        map.put("add*", requiredTx);// Pointcut匹配到的方法中所有add开头的方法
        map.put("save*", requiredTx);// Pointcut匹配到的方法中所有save开头的方法
        map.put("insert*", requiredTx);
        map.put("import*", requiredTx);
        map.put("create*", requiredTx);
        map.put("on*", requiredTx);

        map.put("modify*", requiredTx);
        map.put("update*", requiredTx);

        map.put("delete*", requiredTx);
        //暂时注释掉，因为会导致mybatis使用 <sql>和<includ>标签传参出问题
       /* map.put("select*", readOnlyTx);
        map.put("get*", readOnlyTx);
        map.put("find*", readOnlyTx);*/
        source.setNameMap(map);

        // 事务拦截器
        TransactionInterceptor txAdvice = new TransactionInterceptor(tsManager, source);
        return txAdvice;
    }

    /**
     * 向容器中注入切入点
     */
    @Bean
    public Advisor defaultPointcutAdvisor(TransactionInterceptor txAdvice) {

        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor();
        advisor.setAdvice(txAdvice);
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression(AOP_POINTCUT_EXPRESSION);
        advisor.setPointcut(pointcut);
        return advisor;
    }

}
