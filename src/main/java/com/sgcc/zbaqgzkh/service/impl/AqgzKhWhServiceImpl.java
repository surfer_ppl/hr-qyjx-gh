package com.sgcc.zbaqgzkh.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.common.mapper.SequenceMapper;
import com.sgcc.common.pojo.Sequence;
import com.sgcc.constval.ConstVal;
import com.sgcc.dwlxwh.dto.GkbmDto;
import com.sgcc.dwlxwh.mapper.DwlxWhMapper;
import com.sgcc.utils.NumUtil;
import com.sgcc.utils.SeqUtil;
import com.sgcc.zbaqgzkh.dto.AqgzKhCxDto;
import com.sgcc.zbaqgzkh.dto.DwlxAndDwsDto;
import com.sgcc.zbaqgzkh.mapper.AqgzKhWhMapper;
import com.sgcc.zbaqgzkh.pojo.AqgzKhDwlx;
import com.sgcc.zbaqgzkh.pojo.AqgzKhDwlxGl;
import com.sgcc.zbaqgzkh.pojo.AqgzKhFzb;
import com.sgcc.zbaqgzkh.pojo.AqgzKhGx;
import com.sgcc.zbaqgzkh.pojo.AqgzKhZb;
import com.sgcc.zbaqgzkh.service.AqgzKhWhServiceI;

@Service
public class AqgzKhWhServiceImpl implements AqgzKhWhServiceI {

    @Autowired
    private DwlxWhMapper dwlxWhMapper;

    @Autowired
    private AqgzKhWhMapper aqgzKhWhMapper;

    @Autowired
    private SequenceMapper sequenceMapper;

    /**
     * 安全工作考核首页表格数据查询
     */
    @Override
    public List<AqgzKhCxDto> selectAqgzKhDataGrid(Integer year, Integer dwlxId) {
        List<AqgzKhCxDto> aqgzKhDataGrid = aqgzKhWhMapper.selectAqgzKhDataGrid(year, dwlxId);
        return aqgzKhDataGrid;
    }

    /**
     * 查询安全工作考核单位类型
     */
    @Override
    public List<AqgzKhDwlx> selectAqgzKhDwlxs(Integer year) {
        List<AqgzKhDwlx> aqgzKhDwlxs = aqgzKhWhMapper.selectAqgzKhDwlxs(year);

        return aqgzKhDwlxs;
    }

    /**
     * 插入安全工作考核单位类型
     */
    @Override
    public void insertAqgzKhDwlx(AqgzKhDwlx aqgzKhDwlx) {
        // 获取Id
        Sequence sequence = new Sequence();
        Integer id = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
        aqgzKhDwlx.setId(id);
        aqgzKhWhMapper.insertAqgzKhDwlx(aqgzKhDwlx);
    }

    /**
     * 插入安全工作考核单位类型和对应单位
     */
    @Override
    public void insertAqgzKhDwlxGl(int dwlxId, int[] dwIds) {

        List<AqgzKhDwlxGl> aqgzKhDwlxGlList = new LinkedList<>();

        for (int dwId : dwIds) {
            // 获取Id
            Sequence sequence = new Sequence();
            Integer id = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);

            AqgzKhDwlxGl aqgzKhDwlxGl = new AqgzKhDwlxGl();

            aqgzKhDwlxGl.setId(id);
            aqgzKhDwlxGl.setDwId(dwId);
            aqgzKhDwlxGl.setDwlxId(dwlxId);

            aqgzKhDwlxGlList.add(aqgzKhDwlxGl);
        }

        aqgzKhWhMapper.insertAqgzKhDwlxGl(aqgzKhDwlxGlList);
    }

    /*
     * 查询安全工作考核单位类型和单位关联
     */
    @Override
    public List<Map<String, Object>> selectAqgzKhDwlxGl(Integer year, Integer dwlxId, String dwMc) {
        List<Map<String, Object>> aqgzKhDwlxGlMaps = aqgzKhWhMapper.selectAqgzKhDwlxGl(year, dwlxId, dwMc);

        return aqgzKhDwlxGlMaps;
    }

    /**
     * 导入安全工作考核相关数据
     */
    @Override
    public String imoprtAqgzKhData(int year, int dwlxId, Workbook workbook) {

        String msg = null;
        // 获取sheet页
        Sheet sheet = workbook.getSheetAt(0);
        // 读取第一行表头判断归口部门名称的正确性
        Row row0 = sheet.getRow(0);
        if (null == row0) {
            msg = "请勿导入空文件,请使用下载的模板";
            return msg;
        }
        // 判断第三行是否为空
        Row row2 = sheet.getRow(2);
        if (null == row2) {
            msg = "文件第三行没有数据，请查看是否没有录入数据";
            return msg;
        }

        // 存放归口部门
        Map<String, Integer> gkbmMap = new HashMap<>();

        List<GkbmDto> gkbmList = dwlxWhMapper.selectGkmb();
        for (GkbmDto gkbmDto : gkbmList) {
            gkbmMap.put(gkbmDto.getGkbmMc(), gkbmDto.getId());
        }

        // 验证归口部门名称
        int lastRowNum = sheet.getLastRowNum();
        // 从第四行开始循环
        for (int i = 3; i < lastRowNum + 1; i++) {
            // 先获取第一列单元格,判断是否是父指标行，是就跳过
            Row row = sheet.getRow(i);
            System.out.println(i);
            String numVal = row.getCell(0).getStringCellValue();
            if (NumUtil.isNumeric(numVal)) {
                // 获取第三列归口部门数据
                String gkbmMc = row.getCell(2).getStringCellValue();
                if (!gkbmMap.containsKey(gkbmMc)) {
                    msg = "第" + (i + 1) + "行归口部门名称(" + gkbmMc + ")有错误";
                    return msg;
                }
            } else {
                continue;
            }
        }
        // 创建序列对象
        Sequence sequence = new Sequence();

        List<AqgzKhFzb> aqgzKhFzbList = new LinkedList<>();
        List<AqgzKhZb> aqgzKhZbList = new LinkedList<>();
        List<AqgzKhGx> aqgzKhGxList = new LinkedList<>();

        // 根据单位类型Id获取对应的单位
        DwlxAndDwsDto dwlxAndDwsDto = aqgzKhWhMapper.selectDwlxAndDws(year, dwlxId);
        String dwIds = dwlxAndDwsDto.getDwIds();
        String[] dwIdArr = dwIds.split(",");

        Integer fzbId = null;

        for (int i = 2; i < lastRowNum + 1; i++) {
            // 先获取第一列单元格,判断是否是父指标行，是就跳过
            Row row = sheet.getRow(i);
            String numVal = row.getCell(0).getStringCellValue();

            // 不是阿拉伯数字，那就是父亲指标
            if (!NumUtil.isNumeric(numVal)) {
                // 父指标排号
                String fzbpPhStr = numVal.split("、")[0];
                int fzbPhNum = NumUtil.transNum(fzbpPhStr);
                // 父指标名称
                String fzbMc = numVal.split("、")[1];

                fzbId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);

                AqgzKhFzb khFzb = new AqgzKhFzb();
                khFzb.setId(fzbId);
                khFzb.setPh(fzbPhNum);
                khFzb.setZbMc(fzbMc);
                khFzb.setDwlxId(dwlxId);

                aqgzKhFzbList.add(khFzb);
            } else {

                Integer zbId = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                // 指标名称
                String zbMc = row.getCell(1).getStringCellValue();
                // 归口部门名称
                String gkbmMc = row.getCell(2).getStringCellValue();
                // 考核扣分标准
                String khKfBz = row.getCell(3).getStringCellValue();
                // 考核周期
                String khZq = row.getCell(4).getStringCellValue();

                AqgzKhZb khZb = new AqgzKhZb();
                khZb.setId(zbId);
                /* khZb.setDwId(Integer.valueOf(dwIdStr));
                khZb.setDwlxId(dwlxId);*/
                khZb.setParentId(fzbId);
                khZb.setZbMc(zbMc);
                khZb.setGkbmId(gkbmMap.get(gkbmMc));
                khZb.setKhKfBz(khKfBz);
                khZb.setKhZq(khZq);

                aqgzKhZbList.add(khZb);

                // 考核关系
                for (String dwIdStr : dwIdArr) {
                    Integer id = SeqUtil.getNextSeqVal(sequenceMapper, sequence, ConstVal.QYJX_SEQ_NAME);
                    AqgzKhGx khGx = new AqgzKhGx();

                    khGx.setId(id);
                    khGx.setZbId(zbId);
                    khGx.setDwId(Integer.valueOf(dwIdStr));
                    khGx.setDwlxId(dwlxId);

                    aqgzKhGxList.add(khGx);
                }
            }
        }

        aqgzKhWhMapper.insertAqgzKhFzb(aqgzKhFzbList);
        aqgzKhWhMapper.insertAqgzKhZb(aqgzKhZbList);
        aqgzKhWhMapper.insertAqgzKhGx(aqgzKhGxList);

        return msg;
    }
    
    /*
     * 查询安全工作考核父指标
     */
    @Override
    public List<Map<String, Object>> selectAqgzKhFzb (Integer year) {
        List<Map<String,Object>> aqgzKhFzbs = aqgzKhWhMapper.selectAqgzKhFzb(year);
        return aqgzKhFzbs;
    }

}
