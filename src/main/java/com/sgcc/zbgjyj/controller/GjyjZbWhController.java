package com.sgcc.zbgjyj.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.utils.LayUiTableReponseMapUtil;
import com.sgcc.zbgjyj.dto.GjyjCxDto;
import com.sgcc.zbgjyj.service.GjyjZbWhServiceI;

@RestController
@RequestMapping("/gjyjZbWh")
public class GjyjZbWhController {

    //构造器方式注入
    private final GjyjZbWhServiceI gjyjZbWhService;
    public GjyjZbWhController(GjyjZbWhServiceI gjyjZbWhService) {
        this.gjyjZbWhService = gjyjZbWhService;
    }

    /**
     * 查询关键业绩表格数据
     * 
     * @param paramsMap
     * @return
     */
    @RequestMapping(value = "/getGjyjDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getGjyjDataGrid(@RequestParam Map<String, Object> paramsMap, Integer page,
        Integer limit) {
        /*
         * @RequestParam Map<String, Object> paramsMap
         * 使用Map 接收所有参数要使用@RequestParam，springMVC才会封装参数到Map中
         */

        // 转换paramsMap中的val值为实际对应的类型
        String yearObj = (String)paramsMap.get("year");
        paramsMap.put("year", Integer.parseInt(yearObj));

        // 前台传过来的数据没有值得情况都为"",把参数类型是数字类型的单独加判断
        String dwlxIdObj = (String)paramsMap.get("dwlxId");
        //String dwIdObj = (String)paramsMap.get("dwId");
        //String gkbmIdObj = (String)paramsMap.get("gkbmId");

        if (StringUtils.hasLength(dwlxIdObj)) {
            paramsMap.put("dwlxId", Integer.parseInt(dwlxIdObj));
        }
        /* if (StringUtils.hasLength(dwIdObj)) {
            paramsMap.put("dwId", Integer.parseInt(dwIdObj));
        }
        if (StringUtils.hasLength(gkbmIdObj)) {
            paramsMap.put("gkbmId", Integer.parseInt(gkbmIdObj));
        }*/

        // 分页插件
        PageHelper.startPage(page, limit);

        List<GjyjCxDto> gjyjDataGrid = gjyjZbWhService.selectGjyjDataGrid(paramsMap);

        PageInfo<GjyjCxDto> pageInfo = new PageInfo<GjyjCxDto>(gjyjDataGrid);
        long total = pageInfo.getTotal();

        Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(gjyjDataGrid, total);

        return resultMap;
    }

    /**
     * 导出关键业绩指标模板
     * 
     * @param year
     */
    @RequestMapping(value = "/exportGjyjMuBan", method = RequestMethod.GET)
    public void exportGjyjMuBan(Integer year, HttpServletResponse response) {
        // 读取模板
        String filePath = "com/sgcc/zbgjyj/controller/gjyjMuBan.xlsx";
        URL resource = this.getClass().getClassLoader().getResource(filePath);
        InputStream openStream = null;
        try {
            openStream = resource.openStream();


            // 设置文件名
            String fileName = "附件:" + year + "年关键业绩指标导入模板.xlsx";

            // 设置mime类型
            String mimetype = "application/x-msdownload";
            response.setContentType(mimetype);
            response.addHeader("Content-Disposition",
                "attachment;filename=" + new String(fileName.getBytes("utf-8"), "ISO_8859_1"));
            // 开启响应输出流
            ServletOutputStream outputStream = response.getOutputStream();

            // 使用原始方法
            // 缓冲流
            /* int len = 0;
            byte[] flush = new byte[1024];
            while ((len = (openStream.read(flush))) != -1) {
                outputStream.write(flush, 0, len);
            }*/

            // 使用apach commons-io 下载
            IOUtils.copy(openStream, outputStream);

            // 关闭流
            outputStream.close();
            outputStream.flush();
            outputStream.close();

        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    /**
     * 导入关键业绩指标
     * 
     * @param year
     * @param file
     * @return
     */
    @RequestMapping(value = "/importGjyjZb", method = RequestMethod.POST)
    public Map<String, String> importGjyjZb(Integer year, MultipartFile file) {

        Map<String, String> msgMap = new HashMap<>();

        if (null == year) {
            LocalDate localDate = LocalDate.now();
            year = localDate.getYear();
        }

        Workbook workbook = null;

        try {
            // 获取文件流
            InputStream fileStream = file.getInputStream();
            // 读取上传的Excel文件
            workbook = new XSSFWorkbook(fileStream);
            String msg = gjyjZbWhService.importGjyjZb(year, workbook);

            // 有值证明数据有误
            if (StringUtils.hasLength(msg)) {
                msgMap.put("flag", "false");
                msgMap.put("msg", msg);
            } else {
                msgMap.put("flag", "true");
                msgMap.put("msg", "导入成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "导入异常请重试");
            return msgMap;
        }
        return msgMap;
    }


    /**
     * 一键启动指标
     * @param ids 所有行的ID
     * @return
     */
    @RequestMapping(value = "/startGjyjZb", method = RequestMethod.POST)
    public Map<String, String> startGjyjZb(Integer[] ids) {

        Map<String, String> msgMap = new HashMap<>();
        try {
            gjyjZbWhService.updateZtToStart(ids);

            msgMap.put("flag", "true");
            msgMap.put("msg", "操作成功！");
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "操作失败请重试！");
            return msgMap;
        }
        return msgMap;
    }

    /**
     * 一键停止指标
     * @param ids 所有行的ID
     * @return
     */
    @RequestMapping(value = "/stopGjyjZb", method = RequestMethod.POST)
    public Map<String, String> stopGjyjZb(Integer[] ids) {

        Map<String, String> msgMap = new HashMap<>();
        try {
            gjyjZbWhService.updateZtToStop(ids);

            msgMap.put("flag", "true");
            msgMap.put("msg", "操作成功！");
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "操作失败重试！");
            return msgMap;
        }
        return msgMap;
    }


}
