package com.sgcc.jdkh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.jdkh.pojo.AqgzJdDfb;
import com.sgcc.jdkh.pojo.GjyjJdDfb;
import com.sgcc.jdkh.pojo.ZygzJdDfb;

/**
 * 季度考核待办打分
 * 
 * @author Edward
 * @date 2021/08/09
 */
@Mapper
public interface JdkhDbDfMapper {
    /**
     * 保存季度考核待办打分——关键业绩
     * @param dbJdDfbList
     * @return
     */
    public int updateGjyjJdDf(@Param("dbJdDfbList") List<GjyjJdDfb> dbJdDfbList);
    
    /**
     * 保存季度考核待办打分——专业工作考核
     * @param dbJdDfbList
     * @return
     */
    public int updateZygzJdDf(@Param("dbJdDfbList") List<ZygzJdDfb> dbJdDfbList);
    
    /**
     * 保存季度考核待办打分——安全工作考核
     * @param dbJdDfbList
     * @return
     */
    public int updateAqgzJdDf(@Param("dbJdDfbList") List<AqgzJdDfb> dbJdDfbList);
    
}
