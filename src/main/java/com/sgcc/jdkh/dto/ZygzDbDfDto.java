package com.sgcc.jdkh.dto;

/**
 * 专业工作考核打分Dto
 * 
 * @author Edward
 * @date 2021年6月28日
 */
public class ZygzDbDfDto {
    // ID
    private Integer id;
    // 指标名称
    private String zbMc;
    // 加分或扣分
    private Integer jfOrKf;
    // 归口部门名称
    private String gkbmMc;
    // 单位类型名称
    private String dwlxMc;
    // 单位名称
    private String dwMc;
    // 加分
    private Double jf;
    // 加分理由
    private String jfLy;
    // 扣分
    private Double kf;
    // 扣分理由
    private String kfLy;

    public ZygzDbDfDto() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public Integer getJfOrKf() {
        return jfOrKf;
    }

    public void setJfOrKf(Integer jfOrKf) {
        this.jfOrKf = jfOrKf;
    }

    public String getGkbmMc() {
        return gkbmMc;
    }

    public void setGkbmMc(String gkbmMc) {
        this.gkbmMc = gkbmMc;
    }

    public String getDwlxMc() {
        return dwlxMc;
    }

    public void setDwlxMc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public Double getJf() {
        return jf;
    }

    public void setJf(Double jf) {
        this.jf = jf;
    }

    public String getJfLy() {
        return jfLy;
    }

    public void setJfLy(String jfLy) {
        this.jfLy = jfLy;
    }

    public Double getKf() {
        return kf;
    }

    public void setKf(Double kf) {
        this.kf = kf;
    }

    public String getKfLy() {
        return kfLy;
    }

    public void setKfLy(String kfLy) {
        this.kfLy = kfLy;
    }
}
