package com.sgcc.jcyb.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.constval.ConstVal;
import com.sgcc.jcyb.pojo.JcYbCfZz;
import com.sgcc.jcyb.pojo.JcYbJlCf;
import com.sgcc.jcyb.pojo.JcYbTbRy;
import com.sgcc.jcyb.pojo.JcYbWfLdJl;
import com.sgcc.jcyb.service.JcYbServiceI;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 奖惩月报Controller
 *
 * @Author Edward
 * @Date 2022/3/19
 */
@RestController
@RequestMapping("/jcyb")
public class JcYbController {

    @Autowired
    JcYbServiceI jcYbService;

    /**
     * 导入处罚准则
     *
     * @return Map<String, String>
     */
    @RequestMapping(value = "/importCfZz", method = RequestMethod.POST)
    public Map<String, String> importCfZz(MultipartFile file) {
        Map<String, String> msgMap = new HashMap<>();
        Workbook workbook = null;
        try {
            // 获取文件流
            InputStream fileStream = file.getInputStream();
            // 读取上传的Excel文件
            workbook = new XSSFWorkbook(fileStream);
            jcYbService.importCfZz(workbook);
            msgMap.put("flag", "true");
            msgMap.put("msg", "导入成功");
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "导入异常请重试");
            return msgMap;
        }
        return msgMap;
    }

    /**
     * 查询惩罚准侧（细则）
     *
     * @return
     */
    @RequestMapping(value = "/queryCfZz", method = RequestMethod.POST)
    public List<JcYbCfZz> queryCfZz() {
        List<JcYbCfZz> jcYbCfZzList = jcYbService.selectJcYbCfZz();
        return jcYbCfZzList;
    }

    /**
     * 查询奖惩月填报人员信息Tab
     *
     * @param year 年
     * @param yd   月
     * @param tabFlag   tabFlag
     * @return List<JcYbJlCf>
     */
    @RequestMapping(value = "/queryJcYbTbry", method = RequestMethod.POST)
    public Map<String, Object> queryJcYbTbry(Integer year, Integer yd, String tabFlag, Integer page,
                                             Integer limit) {
        Map<String, Object> resultMap = new HashMap<>();
        PageHelper.startPage(page, limit);
        List<JcYbTbRy> jcYbTbRys= jcYbService.selectTbrys(year, yd, tabFlag);
        PageInfo<JcYbTbRy> pageInfo = new PageInfo<JcYbTbRy>(jcYbTbRys);
        long total = pageInfo.getTotal();
        resultMap.put("result", jcYbTbRys);
        resultMap.put("total", total);
        return resultMap;
    }

    /**
     * 查询奖惩月报纪律处分Tab页面
     *
     * @param zzId 组织ID 树节点ID
     * @param year 年
     * @param yd   月
     * @param xm   姓名
     * @return List<JcYbJlCf>
     */
    @RequestMapping(value = "/queryJcYbJlCf", method = RequestMethod.POST)
    public Map<String, Object> queryJcYbJlCf(Integer zzId, Integer year, Integer yd, String xm, Integer page,
                                             Integer limit) {
        Map<String, Object> resultMap = new HashMap<>();
        PageHelper.startPage(page, limit);
        List<JcYbJlCf> jcYbJlCfs = jcYbService.selectJlcfs(zzId, year, yd, xm);
        PageInfo<JcYbJlCf> pageInfo = new PageInfo<JcYbJlCf>(jcYbJlCfs);
        long total = pageInfo.getTotal();

        resultMap.put("result", jcYbJlCfs);
        resultMap.put("total", total);

        return resultMap;
    }

    /**
     * 查询奖惩月报违反劳动纪律Tab页面
     *
     * @param zzId 组织ID 树节点ID
     * @param year 年
     * @param yd   月
     * @param xm   姓名
     * @return List<JcYbJlCf>
     */
    @RequestMapping(value = "/queryJcYbWfLd", method = RequestMethod.POST)
    public Map<String, Object> queryJcYbWfLd(Integer zzId, Integer year, Integer yd, String xm, Integer page,
                                             Integer limit) {
        Map<String, Object> resultMap = new HashMap<>();
        PageHelper.startPage(page, limit);
        List<JcYbWfLdJl> jcYbWfLdJlLs = jcYbService.selectWfLdJls(zzId, year, yd, xm);
        PageInfo<JcYbWfLdJl> pageInfo = new PageInfo<JcYbWfLdJl>(jcYbWfLdJlLs);
        long total = pageInfo.getTotal();

        resultMap.put("result", jcYbWfLdJlLs);
        resultMap.put("total", total);

        return resultMap;
    }


    /**
     * 保存选中的人员
     * @param ygIds 字符串ids
     * @param tabLx TAB类型（FLAG_JLCF：纪律处分备案；FLAG_LDJL：违反劳动纪律备案；）
     * @param year 年
     * @param yd 月
     * @return Map类型的数据
     */
    @RequestMapping(value = "/saveTbry", method = RequestMethod.POST)
    public Map<String, String> saveTbry(String ygIds, String tabLx,Integer year,Integer yd) {
        Map<String, String> msgMap = new HashMap<>();
        try {
            jcYbService.saveTbryData(ygIds, tabLx, year, yd);
            msgMap.put("flag", "true");
            msgMap.put("msg", "保存成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "保存失败，请重试！");
            return msgMap;
        }
    }

    /**
     * 保存奖惩月报修改的数据数据
     *
     * @param bzSj  json格式的要保存的数据
     * @param tabLx TAB类型（FLAG_JLCF：纪律处分备案；FLAG_LDJL：违反劳动纪律备案；）
     * @return Map类型的数据
     */
    @RequestMapping(value = "/saveJcybData", method = RequestMethod.POST)
    public Map<String, String> saveJcybData(String bzSj, String tabLx) {
        Map<String, String> msgMap = new HashMap<>();
        try {
            if (ConstVal.FLAG_JLCF.equals(tabLx)) {
                List<JcYbJlCf> jcYbJlCfList = JSON.parseArray(bzSj, JcYbJlCf.class);
                jcYbService.saveJlCfData(jcYbJlCfList);

            } else if (ConstVal.FLAG_LDJL.equals(tabLx)) {
                List<JcYbWfLdJl> jcYbWfLdJlList = JSON.parseArray(bzSj, JcYbWfLdJl.class);
                jcYbService.saveWfLdData(jcYbWfLdJlList);
            }
            msgMap.put("flag", "true");
            msgMap.put("msg", "保存成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "保存失败，请重试！");
            return msgMap;
        }
    }


}
