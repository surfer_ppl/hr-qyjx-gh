package com.sgcc.zdkhjl.dto;

/**
 * 重大考核激励项查询页面Dto
 *
 * @author Edward
 * @date 2022-3-1
 */
public class ZdkhjlDwMxDto {
    /**
     * 专题ID
     */
    private Integer ztId;
    /**
     * 专题编号
     */
    private Integer ztBh;
    /**
     * 专题名称
     */
    private String ztMc;
    /**
     * 标准描述
     */
    private String bzMs;
    /**
     * 标准明细
     */
    private String bzMx;
    /**
     * 单位明细表ID
     */
    private Integer dwMxId;
    /**
     * 单位名称
     */
    private String dwMc;
    /**
     * 档位
     */
    private String level;
    /**
     * 评价描述
     */
    private String pjMs;
    /**
     * 评价明细
     */
    private String pjMx;

    public ZdkhjlDwMxDto() {

    }

    public Integer getZtId() {
        return ztId;
    }

    public void setZtId(Integer ztId) {
        this.ztId = ztId;
    }

    public Integer getZtBh() {
        return ztBh;
    }

    public void setZtBh(Integer ztBh) {
        this.ztBh = ztBh;
    }

    public String getZtMc() {
        return ztMc;
    }

    public void setZtMc(String ztMc) {
        this.ztMc = ztMc;
    }

    public String getBzMs() {
        return bzMs;
    }

    public void setBzMs(String bzMs) {
        this.bzMs = bzMs;
    }

    public String getBzMx() {
        return bzMx;
    }

    public void setBzMx(String bzMx) {
        this.bzMx = bzMx;
    }

    public Integer getDwMxId() {
        return dwMxId;
    }

    public void setDwMxId(Integer dwMxId) {
        this.dwMxId = dwMxId;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPjMs() {
        return pjMs;
    }

    public void setPjMs(String pjMs) {
        this.pjMs = pjMs;
    }

    public String getPjMx() {
        return pjMx;
    }

    public void setPjMx(String pjMx) {
        this.pjMx = pjMx;
    }
}
