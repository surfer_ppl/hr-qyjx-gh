package com.sgcc.jdkh.pojo;

/**
 * 专业工作季度打分表实体类（T_JX_ZYGZ_JDDFB）
 * 
 * @author Edward
 * @date 2021年10月29日
 */
public class ZygzJdDfb {
    // ID
    private Integer id;
    // 考核关系ID
    private Integer khgxId;
    // 季度
    private Integer jd;
    // 加分
    private Double jf;
    // 加分理由
    private String jfLy;
    // 扣分
    private Double kf;
    // 扣分理由
    private String kfLy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKhgxId() {
        return khgxId;
    }

    public void setKhgxId(Integer khgxId) {
        this.khgxId = khgxId;
    }

    public Integer getJd() {
        return jd;
    }

    public void setJd(Integer jd) {
        this.jd = jd;
    }

    public Double getJf() {
        return jf;
    }

    public void setJf(Double jf) {
        this.jf = jf;
    }

    public String getJfLy() {
        return jfLy;
    }

    public void setJfLy(String jfLy) {
        this.jfLy = jfLy;
    }

    public Double getKf() {
        return kf;
    }

    public void setKf(Double kf) {
        this.kf = kf;
    }

    public String getKfLy() {
        return kfLy;
    }

    public void setKfLy(String kfLy) {
        this.kfLy = kfLy;
    }

}
