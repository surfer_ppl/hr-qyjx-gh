package com.sgcc.dwlxwh.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.dwlxwh.dto.DwjcWhDto;
import com.sgcc.dwlxwh.dto.DwlxCxDto;
import com.sgcc.dwlxwh.dto.GkbmDto;
import com.sgcc.dwlxwh.pojo.DwjcWh;
import com.sgcc.dwlxwh.pojo.Dwlx;

/**
 * 单位类型维护页面相关
 * 
 * @author Edward
 *
 */
@Mapper
public interface DwlxWhMapper {

    /**
     * 查询单位类型首页表格数据
     * 
     * @return
     */
    public List<DwlxCxDto> selectDwlxDataGrid(@Param(value = "year") Integer year,
        @Param(value = "dwlxId") Integer dwlxId);

    /**
     * 查询查看数据和单位类型维护
     * 
     * @param dwlxid
     * @param year
     * @return
     */
    public List<DwjcWhDto> selectDWjcWhDto(@Param("year") Integer year, @Param("dwlxId") Integer dwlxId);

    /**
     * 插入单位类型数据
     * 
     * @param dwlx
     */
    public int insertDwlx(Dwlx dwlx);

    /**
     * 插入单位简称维护数据
     * 
     * @param dwjcWhList
     */
    public int insertDwjc(@Param("dwjcWhList") List<DwjcWh> dwjcWhList);

    /**
     * 修改单位简称维护数据
     * 
     * @param dwjcWhList
     * @return
     */
    public int updateDwjc(@Param("dwjcWhList") List<DwjcWh> dwjcWhList);

    /**
     * 删除单位简称数据
     * 
     * @param dwlxIdList
     * @return
     */
    public int deleteDwjc(@Param("dwlxIdList") List<Integer> dwlxIdList);

    /**
     * 删除单位类型数据
     * 
     * @param dwlxIdList
     * @return
     */
    public int deleteDwlx(@Param("dwlxIdList") List<Integer> dwlxIdList);

    /**
     * 启用/停用 单位类型，修改单位类型状态
     * 
     * @param year
     * @return
     */
    public int updateDwlx(Integer year, Integer zt);

    /**
     * 根据dwid从组织单元表中获取单位简称
     * 
     * @param dwIdArr
     * @return
     */
    public List<Map<String, Object>> selectPzjc(@Param("dwIdArr") Integer[] dwIdArr);

    /**
     * 根据年度查询单位类型数据
     * 
     * @param year
     * @return
     */
    public List<Dwlx> selectDwlx(Integer year);

    /**
     * 根据年度查询单位
     * 
     * @param year
     * @return
     */
    public List<DwjcWh> selectDwjc(@Param("year") Integer year, @Param("dwlxId") Integer dwlxId);

    /**
     * 查询归口部门 PZJC(GKBMMC) ID ID id,PZJC gkbmMc
     * 
     * @return
     */
    public List<GkbmDto> selectGkmb();

}
