package com.sgcc.zbaqgzkh.pojo;

/**
 * 安全工作考核单位类型，（T_JX_AQGZ_DWLX）
 * @author Edward
 *
 */
public class AqgzKhDwlx {
    private Integer id;
    private String dwlxMc;
    private Integer year;
    
    public AqgzKhDwlx() {
        
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getDwlxMc() {
        return dwlxMc;
    }
    public void setDwlxmc(String dwlxMc) {
        this.dwlxMc = dwlxMc;
    }
    public Integer getYear() {
        return year;
    }
    public void setYear(Integer year) {
        this.year = year;
    }
    
}
