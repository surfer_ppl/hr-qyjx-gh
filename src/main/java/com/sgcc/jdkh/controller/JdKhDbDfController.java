package com.sgcc.jdkh.controller;

/**
 * 季度考核——待办打分
 * 
 * @author Edward
 * @date 2021年5月23日
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sgcc.jdkh.dto.AqgzDbDfDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.jdkh.dto.GjyjDbDfDto;
import com.sgcc.jdkh.dto.ZygzDbDfDto;
import com.sgcc.jdkh.service.JdkhDbDfServiceI;
import com.sgcc.jdkh.service.JdkhServiceI;

@RestController
@RequestMapping("/jdKhDbDf")
public class JdKhDbDfController {

    @Autowired
    JdkhDbDfServiceI jdkhDbDfService;

    @Autowired
    private JdkhServiceI jdkhService;

    /**
     * 季度考核页面查询或季度考核待办打分页面查询(ElementUI)
     * 
     * @param params
     * @param page
     * @param limit
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/getJdKhOrDbDfDataGrid_Elm", method = RequestMethod.POST)
    public Map<String, Object> getJdKhOrDbDfDataGrid_Elm(@RequestParam Map<String, String> params, Integer page,
        Integer limit) {

        Map<String, Object> resultMap = new HashMap<>();
        List<?> resultList = null;

        PageHelper.startPage(page, limit);

        // 获取指标类型，判断查询哪个单位(0:关键业绩指标，1:,2:安全工作考核)
        String zbLx = params.get("zbLx");
        long total = 0;
        if ("0".equals(zbLx)) {
            resultList = (List<GjyjDbDfDto>)jdkhService.selectJdKhOrDbDfDataGrid(params);
            PageInfo<GjyjDbDfDto> pageInfo = new PageInfo<GjyjDbDfDto>((List<GjyjDbDfDto>)resultList);
            total = pageInfo.getTotal();
        } else if ("1".equals(zbLx)) {
            resultList = (List<ZygzDbDfDto>)jdkhService.selectJdKhOrDbDfDataGrid(params);
            PageInfo<ZygzDbDfDto> pageInfo = new PageInfo<ZygzDbDfDto>((List<ZygzDbDfDto>)resultList);
            total = pageInfo.getTotal();
        } else if ("2".equals(zbLx)) {
            resultList = (List<AqgzDbDfDto>)jdkhService.selectJdKhOrDbDfDataGrid(params);
            PageInfo<AqgzDbDfDto> pageInfo = new PageInfo<AqgzDbDfDto>((List<AqgzDbDfDto>)resultList);
            total = pageInfo.getTotal();
        }

        resultMap.put("result", resultList);
        resultMap.put("total", total);

        return resultMap;

    }

    /**
     * 保存打分数据
     * @param dfSj json格式的打分数据
     * @param zblx 指标类型（0：关键业绩；1：专业工作；2：安全工作考核）
     * @return Map类型的数据
     */
    @RequestMapping(value = "/saveDbDf", method = RequestMethod.POST)
    public Map<String, String> saveDbDf(String dfSj, Integer zblx) {
        Map<String, String> msgMap = new HashMap<>();
        try {
            // 解析打分数据
            JSONArray dbDfjSJsonArray = JSON.parseArray(dfSj);
            if (zblx == 0) {
                jdkhDbDfService.saveGjyjDbDf(dbDfjSJsonArray);
            } else if (zblx == 1) {
                jdkhDbDfService.saveZygzDbDf(dbDfjSJsonArray);
            } else if (zblx == 2) {
                jdkhDbDfService.saveAqgzDbDf(dbDfjSJsonArray);
            }
            msgMap.put("flag", "true");
            msgMap.put("msg", "保存成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "保存失败，请重试！");
            return msgMap;
        }

    }

}
