package com.sgcc.common.pojo;

/**
 * 组织单元实体类（T_ZZ_ZZDY）
 * 
 * @author Edward
 *
 */
public class Zzdy {
    // 组织ID
    private Integer id;
    //
    private Integer parentId;
    // 组织名称
    private String deptName;
    // 配置简称
    private String pzjc;
    // 组织编码
    private String code;
    // 性质（0：单位，1：部门，2：班组）
    private Integer xz;
    // 状态，(2：在用，5：废弃)
    private String zt;

    public Zzdy() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getPzjc() {
        return pzjc;
    }

    public void setPzjc(String pzjc) {
        this.pzjc = pzjc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getXz() {
        return xz;
    }

    public void setXz(Integer xz) {
        this.xz = xz;
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt;
    }

}
