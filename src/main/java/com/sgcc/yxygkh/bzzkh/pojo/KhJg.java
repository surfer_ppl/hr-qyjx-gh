package com.sgcc.yxygkh.bzzkh.pojo;

/**
 * 考核结果表（T_JX_YXYG_KHJG）
 * 
 * @author Edward
 * @date 2021年9月6日
 */
public class KhJg {
    private Integer id;
    // 班员ID
    private Integer ygId;
    // 考核周期ID
    private Integer khzqId;
    // 考核得分
    private Double khDf;
    // 年度
    private Integer year;

    public KhJg() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYgId() {
        return ygId;
    }

    public void setYgId(Integer ygId) {
        this.ygId = ygId;
    }

    public Integer getKhzqId() {
        return khzqId;
    }

    public void setKhzqId(Integer khzqId) {
        this.khzqId = khzqId;
    }

    public Double getKhDf() {
        return khDf;
    }

    public void setKhDf(Double khDf) {
        this.khDf = khDf;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

}
