package com.sgcc.utils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 根据分数排序工具类
 * 
 * @author Edward
 * @date 2021年8月13日
 */
public class PxUtil {

    /**
     * 
     * @param targetList
     *            只能传入List集合
     * @param pxZdMc
     *            需要排序的属性（排序字段名称）, 1、使用实体类封装-首字母大写 。 2、使用Map封装-使用key。
     * @param jgZdMc
     *            存放排序结果的属性（结果字段名称）。首字母大写 1、使用实体类封装-首字母大写 。 2、使用Map封装-使用key。
     * @param TObj
     *            集合中泛型类的反射
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static Object getPxResult(Object targetList, Class<?> tClass, String pxZdMc, String jgZdMc)
        throws Exception {

        List<?> tarList = (List<?>)targetList;

        if (!tarList.isEmpty()) {
            if (tClass.newInstance() instanceof Map) {

                int index = 0;
                Double[] pxDataArr = new Double[tarList.size()];
                for (Object object : tarList) {
                    // 先获取需要排序的数据
                    Map<String, Object> rowMap = (Map<String, Object>)object;
                    double pxData = Double.parseDouble(rowMap.get(pxZdMc).toString());
                    pxDataArr[index] = pxData;
                    index++;
                }
                // 调用排名方法获取排名
                Map<Double, Integer> pMap = sortPxDataArr(pxDataArr);
                
                for (Object object : tarList) {
                    // 先获取需要排序的数据
                    Map<String, Object> rowMap = (Map<String, Object>)object;
                    double pxData = Double.parseDouble(rowMap.get(pxZdMc).toString());
                    Integer pm = pMap.get(pxData);
                    rowMap.put(jgZdMc, pm);
                }
            } else {
                // 方法名称
                String getMethodName = "get" + pxZdMc;
                String setMethodNmae = "set" + jgZdMc;
                // 通过反射获取方法
                Method getPxMethod = tClass.getDeclaredMethod(getMethodName);
                Method setPxMethod = tClass.getDeclaredMethod(setMethodNmae,Integer.class);

                // 先获取需要排序的数据
                Double[] pxDataArr = new Double[tarList.size()];
                int index = 0;

                for (Object object : tarList) {
                    // 获取需要排序字段的值
                    Double pxData = (Double)getPxMethod.invoke(object);
                    pxDataArr[index] = pxData;
                    index++;
                }
                // 调用排名方法获取排名
                Map<Double, Integer> pMap = sortPxDataArr(pxDataArr);

                for (Object object : tarList) {
                    // 获取需要排序字段的值
                    Double pxData = (Double)getPxMethod.invoke(object);
                    Integer pm = pMap.get(pxData);
                    setPxMethod.invoke(object, pm);
                }
            }
        }
        return targetList;
    }

    /**
     * 对得分数据进行排序，返回排名
     * 
     * @param pxDataArr
     * @return
     */
    private static Map<Double, Integer> sortPxDataArr(Double[] pxDataArr) {

        Map<Double, Integer> pmMap = new HashMap<>();

        // 倒序排序
        Arrays.sort(pxDataArr, Collections.reverseOrder());

        int pmIndex = 0;
        for (Double dfNum : pxDataArr) {
            // 如果包含相同的打分,排名不变
            if (pmMap.containsKey(dfNum)) {
                continue;
            } else {
                pmIndex++;
                pmMap.put(dfNum, pmIndex);
            }
        }
        return pmMap;
    }

}
