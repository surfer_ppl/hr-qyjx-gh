package com.sgcc.yxygkh.bzzkh.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.yxygkh.bzzkh.mapper.BzzKhMapper;
import com.sgcc.yxygkh.bzzkh.pojo.KhRy;
import com.sgcc.yxygkh.bzzkh.service.BzzKhServiceI;

/**
 * 班组长考核相关SrtviceImpl
 * 
 * @author Edward
 * @date 2021年9月6日
 */
@Service
public class BzzKhServiceImpl implements BzzKhServiceI {

    @Autowired
    BzzKhMapper bzzKhMapper;

    /**
     * 考核设置初始化考核周期和考核结果表
     */
    @Override
    public void initKhZqAndKhJg(Integer bzId, Integer zqLx) {
        // 根据班组ID查询此班组下的班员（班组长除外）
        List<KhRy> khRyList = bzzKhMapper.selectKhRy(bzId);

        // 1 季度，2 月度
        if (1 == zqLx) {
            for (int i = 1; i < 5; i++) {
                String jd = "0" + i;
            }

        } else if (2 == zqLx) {
            for (int i = 0; i < 12; i++) {
                String month = String.valueOf(i + 1);
            }
        }
    }

}
