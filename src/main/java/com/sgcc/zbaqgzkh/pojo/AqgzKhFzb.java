package com.sgcc.zbaqgzkh.pojo;
/**
 * 安全工作考核父指标（`T_JX_AQGZ_FZB`）
 * @author Edward
 *
 */
public class AqgzKhFzb {
    //id
    private Integer id;
    //单位类型ID
    private Integer dwlxId;
    //父指标名称
    private String zbMc;
    //排号
    private Integer ph;
    
    public AqgzKhFzb() {
        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public String getZbMc() {
        return zbMc;
    }

    public void setZbMc(String zbMc) {
        this.zbMc = zbMc;
    }

    public Integer getPh() {
        return ph;
    }

    public void setPh(Integer ph) {
        this.ph = ph;
    }
    
}
