package com.sgcc.jdkh.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.sgcc.jdkh.mapper.JdkhJkZsMapper;
import com.sgcc.jdkh.pojo.GjyjJdKbDfb;
import com.sgcc.jdkh.pojo.JdKhJkZs;
import com.sgcc.jdkh.service.JdKhJkZsServiceI;
import com.sgcc.utils.PxUtil;

/**
 * 季度考核监控展示
 * 
 * @author Edward
 * @date 2021/08/10
 */
@Service
public class JdKhJkZsServiceImpl implements JdKhJkZsServiceI {

    @Autowired
    JdkhJkZsMapper jdkhJkZsMapper;

    /**
     * 查询季度考核监控展示
     * 
     * @throws Exception
     */
    public List<JdKhJkZs> selectJdKhJkZs(Integer year, Integer jd, Integer dwlxId, Integer dwId) throws Exception {
        List<JdKhJkZs> jdKhJkZsList = jdkhJkZsMapper.selectJdKhJkZs(year, jd, dwlxId, dwId);
        // 合计得分排名
        PxUtil.getPxResult(jdKhJkZsList, JdKhJkZs.class, "HjDf", "HjPm");
        // 小计得分排名
        PxUtil.getPxResult(jdKhJkZsList, JdKhJkZs.class, "XjDf", "XjPm");
        // 专业工作考核得分排名
        PxUtil.getPxResult(jdKhJkZsList, JdKhJkZs.class, "ZygzZdf", "ZygzZdfPm");

        return jdKhJkZsList;

    }

    /**
     * 启动季度考核同步
     */
    @Override
    public void onJdKhTb(Integer year, Integer jd) {
        // 先删除旧的关键业绩看板数据
        jdkhJkZsMapper.deleteGjyjKbData(year, jd);

        // 同步关键业绩打分数据到看板表
        jdkhJkZsMapper.insertGjyjKbData(year, jd);

        // 先删除旧的专业工作考核看板数据
        jdkhJkZsMapper.deleteZygzKhKbData(year, jd);

        // 同步专业工作打分数据看板表
        jdkhJkZsMapper.insertZygzKhKbData(year, jd);

        // 查询小计得分
        List<Map<String, Object>> xjDfList = jdkhJkZsMapper.selectXjDf(year, jd);
        Map<Integer, Double> dwXjDfMap = this.fzDwXjDfToMap(xjDfList);

        // 查询关键业绩总加分和总扣分
        List<Map<String, Object>> gjyjZjfAndZkfList = jdkhJkZsMapper.selectGjyjZjfAndZkf(year, jd);
        Map<Integer, Double> dwGjyjZjfMap = this.fzDwGjyjZjfToMap(gjyjZjfAndZkfList);
        Map<Integer, Double> dwGjyjZkfMap = this.fzDwGjyjZkfToMap(gjyjZjfAndZkfList);

        // 查询关键业绩看板打分数据
        List<GjyjJdKbDfb> gjyjKbDfDataList = jdkhJkZsMapper.selectGjyjKbDfData(year, jd);
        Map<Integer, String> dwGjyjDfXqBzMap = this.fzDwGjyjDfXqBzToMap(gjyjKbDfDataList);

        // 查询经过权重计算的专业工作考核
        List<Map<String, Object>> zygzKhZdfList = jdkhJkZsMapper.selectZygzKhZdf(year, jd);
        Map<Integer, Double> dwZygzKhZdfMap = this.fzDwZygzKhZdfToMap(zygzKhZdfList);

        // 查询安全工作总扣分
        List<Map<String, Object>> aqGzZdfList = jdkhJkZsMapper.selectAqGzZdf(year, jd);
        Map<Integer, Double> dwAqGzZdfMap = this.fzDwAqGzZdfToMap(aqGzZdfList);

        // 查询安全工作考核得分细项（各父指标得分）
        List<Map<String, Object>> aqGzFzbDfList = jdkhJkZsMapper.selectAqGzFzbDf(year, jd);
        Map<Integer, String> dwAqGzFzbDataMap = this.fzDwAqGzFzbDataToMap(aqGzFzbDfList);

        // 初始化用于插入监控表的数据
        List<JdKhJkZs> initJkzsList = jdkhJkZsMapper.initJkzs(year, jd);

        for (JdKhJkZs jdKhJkZsObj : initJkzsList) {

            Integer dwId = jdKhJkZsObj.getDwId();
            // 小计得分
            if (dwXjDfMap.containsKey(dwId)) {
                jdKhJkZsObj.setXjDf(dwXjDfMap.get(dwId));
            }
            // 关键业绩总加分值
            if (dwGjyjZjfMap.containsKey(dwId)) {
                jdKhJkZsObj.setZjfz(dwGjyjZjfMap.get(dwId));
            }
            // 关键业绩总减分值
            if (dwGjyjZkfMap.containsKey(dwId)) {
                jdKhJkZsObj.setZkfz(dwGjyjZkfMap.get(dwId));
            }
            // 关键业绩得分备注
            if (dwGjyjDfXqBzMap.containsKey(dwId)) {
                jdKhJkZsObj.setZdfBz(dwGjyjDfXqBzMap.get(dwId));
            }

            // 专业工作考核总得分
            if (dwZygzKhZdfMap.containsKey(dwId)) {
                jdKhJkZsObj.setZygzZdf(dwZygzKhZdfMap.get(dwId));
            }
            // 安全工作考核总得分
            if (dwAqGzZdfMap.containsKey(dwId)) {
                jdKhJkZsObj.setAqgzZdf(dwAqGzZdfMap.get(dwId));
            }

            // 安全工作考核得分细项
            if (dwAqGzFzbDataMap.containsKey(dwId)) {
                jdKhJkZsObj.setAqgzDfXx(dwAqGzFzbDataMap.get(dwId));
            }

        }

        // 先执行删除
        jdkhJkZsMapper.deleteJkZs(year, jd);
        // 再执行插入
        jdkhJkZsMapper.insertJkZs(initJkzsList);

    }

    /**
     * 封装处理小计得分
     * 
     * @param xjDfList
     * @return
     */
    public Map<Integer, Double> fzDwXjDfToMap(List<Map<String, Object>> xjDfList) {
        Map<Integer, Double> dwXjDfMap = new HashMap<>();

        for (Map<String, Object> xjDfMap : xjDfList) {
            Integer dwId = Integer.parseInt(xjDfMap.get("DWID").toString());
            Double xjDf = Double.parseDouble(xjDfMap.get("XJDF").toString());

            dwXjDfMap.put(dwId, xjDf);
        }

        return dwXjDfMap;
    }

    /**
     * 封装处理关键业绩总加分
     * 
     * @param gjyjZjfAndZkfList
     * @return
     */
    public Map<Integer, Double> fzDwGjyjZjfToMap(List<Map<String, Object>> gjyjZjfAndZkfList) {
        Map<Integer, Double> dwZjfMap = new HashMap<>();

        for (Map<String, Object> zjfAndZkfMap : gjyjZjfAndZkfList) {
            Integer dwId = Integer.parseInt(zjfAndZkfMap.get("DWID").toString());
            Double gjyjZjf = null;
            Object zjf = zjfAndZkfMap.get("ZJF");
            if (null != zjf) {
                gjyjZjf = Double.parseDouble(zjf.toString());
            }
            dwZjfMap.put(dwId, gjyjZjf);
        }

        return dwZjfMap;
    }

    /**
     * 封装处理关键业绩总扣分
     * 
     * @param gjyjZjfAndZkfList
     * @return
     */
    public Map<Integer, Double> fzDwGjyjZkfToMap(List<Map<String, Object>> gjyjZjfAndZkfList) {
        Map<Integer, Double> dwZkfMap = new HashMap<>();

        for (Map<String, Object> zjfAndZkfMap : gjyjZjfAndZkfList) {
            Integer dwId = Integer.parseInt(zjfAndZkfMap.get("DWID").toString());
            Double gjyjZkf = null;
            Object zkf = zjfAndZkfMap.get("ZKF");
            if (null != zkf) {
                gjyjZkf = Double.parseDouble(zkf.toString());
            }
            dwZkfMap.put(dwId, gjyjZkf);
        }

        return dwZkfMap;
    }

    /**
     * 处理关键业绩得分（加减分）详情备注
     * 
     * @param gjyjKbDfDataList
     * @return
     */
    public Map<Integer, String> fzDwGjyjDfXqBzToMap(List<GjyjJdKbDfb> gjyjKbDfDataList) {
        Map<Integer, String> dwGjyjDfXqBzMap = new HashMap<>();

        for (GjyjJdKbDfb gjyjJdKbDfb : gjyjKbDfDataList) {
            Integer dwId = gjyjJdKbDfb.getDwId();
            String gjzbMc = gjyjJdKbDfb.getGjzbMc();
            String zbMc = gjyjJdKbDfb.getZbMc();
            Double jf = gjyjJdKbDfb.getJf();
            String jfLy = gjyjJdKbDfb.getJfLy();
            Double kf = gjyjJdKbDfb.getKf();
            String kfLy = gjyjJdKbDfb.getKfLy();
            String bz = gjyjJdKbDfb.getBz();

            // 一条指标的打分细项
            String dfXxStr = null;
            StringBuffer dfXxBuffer = new StringBuffer();

            if (StringUtils.hasLength(jfLy)) {
                dfXxBuffer.append(jfLy + " 加" + jf + "分;");
            }

            if (StringUtils.hasLength(kfLy)) {
                dfXxBuffer.append(kfLy + " 减" + kf + "分;");
            }

            if (StringUtils.hasLength(bz)) {
                dfXxBuffer.append(bz);
            }

            if (StringUtils.hasLength(dfXxBuffer.toString())) {
                dfXxStr = gjzbMc + "——" + zbMc + "：" + dfXxBuffer.toString();
            }

            int index = 1;
            if (null != dfXxStr) {
                if (dwGjyjDfXqBzMap.containsKey(dwId)) {
                    index++;
                    dfXxStr = dwGjyjDfXqBzMap.get(dwId) + "\n" + index + "、" + dfXxStr;
                    dwGjyjDfXqBzMap.put(dwId, dfXxStr);
                } else {
                    dfXxStr = index + "、" + dfXxStr;
                    dwGjyjDfXqBzMap.put(dwId, dfXxStr);
                }
            } else {
                continue;
            }
        }

        return dwGjyjDfXqBzMap;

    }

    /**
     * 封装专业工作考核权重计算后的总得分
     * 
     * @param gjyjZjfAndZkfList
     * @return
     */
    public Map<Integer, Double> fzDwZygzKhZdfToMap(List<Map<String, Object>> zygzKhZdfList) {
        Map<Integer, Double> dwZygzZdfMap = new HashMap<>();

        for (Map<String, Object> zygzKhZdfMap : zygzKhZdfList) {
            Integer dwId = Integer.parseInt(zygzKhZdfMap.get("DWID").toString());
            Double zygzkhZdf = Double.parseDouble(zygzKhZdfMap.get("ZSDF").toString());
            dwZygzZdfMap.put(dwId, zygzkhZdf);
        }

        return dwZygzZdfMap;
    }

    /**
     * 封装安全工作考核总扣分
     * 
     * @param gjyjZjfAndZkfList
     * @return
     */
    public Map<Integer, Double> fzDwAqGzZdfToMap(List<Map<String, Object>> aqGzZdfList) {
        Map<Integer, Double> dwAqgzZdfMap = new HashMap<>();

        for (Map<String, Object> aqGzZdfMap : aqGzZdfList) {
            Integer dwId = Integer.parseInt(aqGzZdfMap.get("DWID").toString());
            Double qgzZdf = Double.parseDouble(aqGzZdfMap.get("AQGZZDF").toString());
            dwAqgzZdfMap.put(dwId, qgzZdf);
        }

        return dwAqgzZdfMap;
    }

    /**
     * 封装单位各个安全工作父指标得分
     * 
     * @param aqGzFzbDfList
     * @return
     */
    public Map<Integer, String> fzDwAqGzFzbDataToMap(List<Map<String, Object>> aqGzFzbDfList) {
        Map<Integer, String> dwAqGzFzbDataMap = new HashMap<>();

        for (Map<String, Object> aqGzFzbDfMap : aqGzFzbDfList) {
            Integer dwId = Integer.parseInt(aqGzFzbDfMap.get("DWID").toString());
            String fzbZkf = aqGzFzbDfMap.get("FZBZKF").toString();
            if (dwAqGzFzbDataMap.containsKey(dwId)) {
                String preFzbZkf = dwAqGzFzbDataMap.get(dwId);
                fzbZkf = preFzbZkf + "," + fzbZkf;
                dwAqGzFzbDataMap.put(dwId, fzbZkf);
            } else {
                dwAqGzFzbDataMap.put(dwId, fzbZkf);
            }
        }

        return dwAqGzFzbDataMap;
    }

}
