package com.sgcc.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sgcc.common.dto.Loginer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgcc.common.service.LoginServiceI;

/**
 * 登录
 * @author Terrans Force
 * @date 2021/08/31
 */
@RestController
@RequestMapping("/login")
public class LoginController {
    
    @Autowired
    LoginServiceI loginService;
    
    @RequestMapping(value = "/loginCheck", method = RequestMethod.POST)
    public Map<String, String> loginCheck(String account,String passWord){
        Map<String,String> resultMap = new HashMap<>();

        List<Loginer> loginers = loginService.selectLoginer(account, passWord);
        if(!loginers.isEmpty()){
            String xm = loginers.get(0).getXm();
            resultMap.put("flag","true");
            resultMap.put("msg","登陆成功!");
            resultMap.put("xm",xm);
        }else{
            resultMap.put("flag","false");
            resultMap.put("msg","账号或密码错误");
        }
        return resultMap;
    }
    
    /**
     * 查询待办信息
     * @return
     */
    @RequestMapping(value = "/getDbXx", method = RequestMethod.POST)
    public List<Map<String,Object>> getDbXx(){
        List<Map<String,Object>> dbXxs = loginService.selectDbXx();
        return dbXxs;
    }
}
