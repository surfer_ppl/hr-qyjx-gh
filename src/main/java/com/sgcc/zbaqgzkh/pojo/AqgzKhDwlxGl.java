package com.sgcc.zbaqgzkh.pojo;

/**
 * 安全工作考核单位类型和单位关联表（T_JX_AQGZ_DWLXGL）
 * @author Edward
 *
 */
public class AqgzKhDwlxGl {
    //ID
    private Integer id;
    //单位类型ID
    private Integer dwlxId;
    //单位ID
    private Integer dwId;
    
    public AqgzKhDwlxGl() {
        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDwlxId() {
        return dwlxId;
    }

    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }
}
