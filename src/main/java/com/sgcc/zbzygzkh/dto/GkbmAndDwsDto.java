package com.sgcc.zbzygzkh.dto;

/**
 * 生成评价指标，查询归口部门和单位的对应关系
 * 
 * @author Edward
 *
 */
public class GkbmAndDwsDto {

    // 归口部门Id
    private Integer gkbmId;
    // 对应的单位ID
    private String dwIds;

    public GkbmAndDwsDto() {

    }

    public Integer getGkbmId() {
        return gkbmId;
    }

    public void setGkbmId(Integer gkbmId) {
        this.gkbmId = gkbmId;
    }

    public String getDwIds() {
        return dwIds;
    }

    public void setDwIds(String dwIds) {
        this.dwIds = dwIds;
    }
}
