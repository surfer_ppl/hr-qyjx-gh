package com.sgcc.zbzygzkh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.zbzygzkh.dto.ZygzKhZbCxDto;

/**
 * 专业工作考核指标Mapper
 * @author Edward
 * @date 2021年6月20日
 */
@Mapper
public interface ZygzKhZbMapper {
    /**
     * 查询专业工作考核详情表格数据
     * 
     * @param year
     * @return
     */
    public List<ZygzKhZbCxDto> selectZygzKhZbDataGrid(@Param("year") int year,@Param("zbMc") String zbMc);
}
