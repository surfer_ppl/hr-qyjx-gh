package com.sgcc.yjzrs.service;

public interface YjzrsServiceI {

    /**
     * 根据年度创建打分表数据
     * 
     * @param year
     */
    public void createDfbData(int year);
}
