package com.sgcc.zdkhjl.dto;



import java.util.List;

/**
 * 重大考核奖励看板展示Dto
 * @Author Edward
 * @Date 2022/3/20
 */
public class ZdkhjlKbZsDto {
    /**
     * 专题ID
     */
    private Integer ztId;
    /**
     * 专题编号
     */
    private Integer ztBh;
    /**
     * 专题名称
     */
    private String ztMc;

    /**
     * 单位信息
     */
    List<ZdkhjlDwMxDto> dwMxList;

    public Integer getZtId() {
        return ztId;
    }

    public void setZtId(Integer ztId) {
        this.ztId = ztId;
    }

    public Integer getZtBh() {
        return ztBh;
    }

    public void setZtBh(Integer ztBh) {
        this.ztBh = ztBh;
    }

    public String getZtMc() {
        return ztMc;
    }

    public void setZtMc(String ztMc) {
        this.ztMc = ztMc;
    }

    public List<ZdkhjlDwMxDto> getDwMxList() {
        return dwMxList;
    }

    public void setDwMxList(List<ZdkhjlDwMxDto> dwMxList) {
        this.dwMxList = dwMxList;
    }
}
