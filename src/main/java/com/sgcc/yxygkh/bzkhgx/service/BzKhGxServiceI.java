package com.sgcc.yxygkh.bzkhgx.service;

import java.util.List;

import com.sgcc.yxygkh.bzkhgx.dto.ByXx;
import com.sgcc.yxygkh.bzkhgx.dto.BzXx;
import com.sgcc.yxygkh.bzkhgx.dto.GqXx;

/**
 * 班组工区考核关系
 * 
 * @author Terrans Force
 * @date 2021/08/30
 */
public interface BzKhGxServiceI {
    
    /**
     * 添加班组到工区
     * @param gqId
     * @param bzidArr
     * @return
     */
    public void insertBz(Integer gqId,Integer[] bzidArr);
    /**
     * 删除工区
     * @param bzidArr
     * @return
     */
    public void deleteBz(Integer[] bzidArr);
    
    /**
     * 修改工区名称
     * @return
     */
    public void updateGqMc(Integer gqId,String gqMc);
    
    /**
     * 添加自定义工区 
     * @return
     */
    public void insertZdyGq(Integer dwId,String gqMc);
    
    /**
     * 查询工区信息
     * 
     * @param dwId
     * @return
     */
    public List<GqXx> selectGqXxs(int dwId);

    /**
     * 查询班组信息
     * 
     * @param gqId
     * @return
     */
    public List<BzXx> selectBzXxs(int gqId);

    /**
     * 查询班员信息
     * 
     * @param bzId
     * @return
     */
    public List<ByXx> selectByXxs(int bzId);
}
