package com.sgcc.zbaqgzkh.dto;

/**
 * 根据单位类型查对应的单位 dwlxId-dwIds
 * @author Edward
 *
 */
public class DwlxAndDwsDto {
    private Integer dwlxId;
    private String dwIds;
    
    
    public DwlxAndDwsDto() {
        
    }
    
    public Integer getDwlxId() {
        return dwlxId;
    }
    public void setDwlxId(Integer dwlxId) {
        this.dwlxId = dwlxId;
    }
    public String getDwIds() {
        return dwIds;
    }
    public void setDwIds(String dwIds) {
        this.dwIds = dwIds;
    }
           
}
