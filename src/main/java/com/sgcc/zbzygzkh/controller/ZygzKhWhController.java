package com.sgcc.zbzygzkh.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import com.sgcc.utils.LayUiTableReponseMapUtil;
import com.sgcc.zbzygzkh.pojo.ZygzKhZbPz;
import com.sgcc.zbzygzkh.service.ZygzKhWhServiceI;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/zygzKhWh")
public class ZygzKhWhController {

    @Autowired
    private ZygzKhWhServiceI zygzKhWhService;

    /**
     * 查询专业工作考核权重DataGrid表头数据
     *
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/getZygzKhQzDataGridTh", method = RequestMethod.POST)
    public List<Map<String, Object>> getZygzKhQzDataGridTh(Integer year, Integer jd) {

        if (null == jd) {
            jd = 0;
        }

        List<Map<String, Object>> resultMap = zygzKhWhService.selectZygzKhQzDataGridTh(year, jd);
        return resultMap;
    }


    /**
     * 验证专业工作考核是否按照季度导入过
     *
     * @param year
     * @return
     */
    @RequestMapping(value = "/checkZygzKhQzIsJd", method = RequestMethod.POST)
    public Map<String, Object> checkZygzKhQzIsJd(Integer year) {
        Map<String, Object> resuleDataGrid = zygzKhWhService.checkZygzKhQzIsJd(year);
        return resuleDataGrid;
    }

    /**
     * 查询专业工作考核权重DataGrid表格数据
     *
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/getZygzKhQzDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getZygzKhQzDataGrid(Integer year, Integer jd) {

        if (null == jd) {
            jd = 0;
        }

        List<Map<String, Object>> resuleDataGrid = zygzKhWhService.selectZygzKhQzDataGrid(year, jd);
        Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resuleDataGrid, null);

        return resultMap;
    }


    /**
     * 导入专业工作考核权重
     *
     * @param year
     * @param jd
     * @param ndOrJd
     * @param file
     * @return
     */
    @RequestMapping(value = "/importZygzKhQz", method = RequestMethod.POST)
    public Map<String, String> importZygzKhQz(Integer year, Integer jd, Integer ndOrJd, MultipartFile file) {

        Map<String, String> msgMap = new HashMap<>();

        Workbook workbook = null;

        try {
            // 获取文件流
            InputStream fileStream = file.getInputStream();
            if (null == fileStream) {
                msgMap.put("flag", "false");
                msgMap.put("msg", "文件读取失败，请重试");
                return msgMap;
            }
            // 读取上传的Excel文件
            workbook = new XSSFWorkbook(fileStream);
            //jd传入0 代表不按照季度导入
            String msg = zygzKhWhService.importZygzKhQz(year, 0, ndOrJd, workbook);

            // 有值证明数据有误
            if (StringUtils.hasLength(msg)) {
                msgMap.put("flag", "false");
                msgMap.put("msg", msg);
            } else {
                msgMap.put("flag", "true");
                msgMap.put("msg", "导入成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "导入异常请重试");
            return msgMap;
        }
        return msgMap;
    }


    /**
     * 导入专业工作考核权重——按季度
     *
     * @param year
     * @param file
     * @return
     */
    @RequestMapping(value = "/importZygzKhQzByJd", method = RequestMethod.POST)
    public Map<String, String> importZygzKhQzByJd(Integer year, Integer jd, Integer ndOrJd, MultipartFile file) {

        Map<String, String> msgMap = new HashMap<>();

        Workbook workbook = null;

        try {
            // 获取文件流
            InputStream fileStream = file.getInputStream();
            if (null == fileStream) {
                msgMap.put("flag", "false");
                msgMap.put("msg", "文件读取失败，请重试");
                return msgMap;
            }
            // 读取上传的Excel文件
            workbook = new XSSFWorkbook(fileStream);
            String msg = zygzKhWhService.importZygzKhQz(year, jd, ndOrJd, workbook);

            // 有值证明数据有误
            if (StringUtils.hasLength(msg)) {
                msgMap.put("flag", "false");
                msgMap.put("msg", msg);
            } else {
                msgMap.put("flag", "true");
                msgMap.put("msg", "导入成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "导入异常请重试");
            return msgMap;
        }
        return msgMap;
    }

    /**
     * 新增专业工作考核指标配置
     *
     * @param zygzKhZbPz
     * @return
     */
    @RequestMapping(value = "/addZygzKhZbPz", method = RequestMethod.POST)
    public Map<String, String> addZygzKhZbPz(ZygzKhZbPz zygzKhZbPz) {

        Map<String, String> msgMap = new HashMap<>();
        try {
            zygzKhWhService.insertZygzKhZbpz(zygzKhZbPz);
            msgMap.put("flag", "true");
            msgMap.put("msg", "新增成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "新增失败请重试");
            return msgMap;
        }
    }

    /**
     * 删除专业工作考核
     *
     * @return
     */
    @RequestMapping(value = "/removeZygzKhZbPz", method = RequestMethod.POST)
    public Map<String, String> removeZygzKhZbPz(int[] ids) {
        Map<String, String> msgMap = new HashMap<>();
        try {
            zygzKhWhService.deleteZygzKhZbpz(ids);
            msgMap.put("flag", "true");
            msgMap.put("msg", "删除成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "删除失败请重试");
            return msgMap;
        }

    }

    /**
     * 查询专业工作考核指标配置数据
     *
     * @param year
     * @return
     */
    @RequestMapping(value = "/getZbPzDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getZbPzDataGrid(Integer year) {

        List<ZygzKhZbPz> zbPzList = zygzKhWhService.selectZbPz(year);

        Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(zbPzList, null);
        return resultMap;
    }

    /**
     * 生成专业工作考核评价指标
     *
     * @param year
     */
    @RequestMapping(value = "/createZygzkhZbPj", method = RequestMethod.POST)
    public Map<String, String> createZygzkhZbPj(Integer year) {

        Map<String, String> msgMap = new HashMap<>();
        try {
            zygzKhWhService.createZygzkhZbPj(year);
            msgMap.put("flag", "true");
            msgMap.put("msg", "生成评价指标成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "生成评价指标失败，请重试");
            return msgMap;
        }
    }

    /**
     * 导出专业工作考核权重
     *
     * @param year
     */
    @RequestMapping(value = "/exportZygzKhQz", method = RequestMethod.GET)
    public void exportZygzKhQz(Integer year, Integer jd, HttpServletResponse response) {

        // 读取模板
        String filePath = "com/sgcc/zbzygzkh/controller/export_zygzkhQz.xlsx";
        URL resource = this.getClass().getClassLoader().getResource(filePath);
        InputStream openStream = null;
        Workbook workbook = null;
        try {
            openStream = resource.openStream();
            workbook = new XSSFWorkbook(openStream);

            // 设置文件名
            String fileName = null;
            if (jd == null) {
                jd = 0;
                fileName = "附件:" + year + "年专业工作考核权重.xlsx";
            } else {
                fileName = "附件:" + year + "年第" + jd + "季度专业工作考核权重.xlsx";
            }
            zygzKhWhService.exportZygzQz(workbook, year, jd);
            // 设置mime类型
            String mimeType = "application/x-msdownload";
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition",
                    "attachment;filename=" + new String(fileName.getBytes("utf-8"), "ISO_8859_1"));
            // 开启响应输出流
            ServletOutputStream outputStream = response.getOutputStream();
            workbook.write(outputStream);
            // 推出输出流，并关闭相关流
            outputStream.flush();
            workbook.close();
            outputStream.close();
            openStream.close();

        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
