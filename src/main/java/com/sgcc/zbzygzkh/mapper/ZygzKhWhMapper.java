package com.sgcc.zbzygzkh.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.sgcc.zbzygzkh.dto.GkbmAndDwsDto;
import com.sgcc.zbzygzkh.dto.ZygzKhQzCxDto;
import com.sgcc.zbzygzkh.pojo.ZygzKhGx;
import com.sgcc.zbzygzkh.pojo.ZygzKhQz;
import com.sgcc.zbzygzkh.pojo.ZygzKhZb;
import com.sgcc.zbzygzkh.pojo.ZygzKhZbPz;

/**
 * 主页工作考核维护页面相关
 *
 * @author Edward
 */
@Mapper
public interface ZygzKhWhMapper {
    /**
     * 查询专业工作考核涉及到所有归口部门 GKBMBH（归口部门编号）GKBMID(归口部门ID) GKBMMC(归口部门名称)
     *
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectZygzKhGkbm(int year, int jd);

    /**
     * 查询专业工作考核涉及到的单位类型和对应单位
     *
     * @param year
     * @param jd
     * @return
     */
    public List<Map<String, Object>> selectZygzKhDwlxAndDw(int year, int jd);

    /**
     * 查询专业工作考核是否有通过季度导入过
     *
     * @param year
     * @return
     */
    public List<Map<String, Object>> selectZygzKhIsByJd(int year);

    /**
     * 查询专业工作考核权限
     *
     * @param year
     * @param jd
     * @return
     */
    public List<ZygzKhQzCxDto> selectZygzKhQz(int year, int jd);

    /**
     * 插入权重表数据
     *
     * @param zygzKhQzList
     * @return
     */
    public int insertZygzKhQz(@Param("zygzKhQzList") List<ZygzKhQz> zygzKhQzList, @Param("ndOrJd") Integer ndOrJd);

    /**
     * 删除专业工作考核权重数据
     * @param year
     * @param jd
     * @param ndOrJd
     * @return
     */
    public int deleteZygzKhQz(@Param("year") Integer year, @Param("jd") Integer jd, @Param("ndOrJd") Integer ndOrJd);


    /**
     * 根据年度查询指标配置
     *
     * @param year
     * @return
     */
    public List<ZygzKhZbPz> selectZbPz(int year);

    /**
     * 插入专业工作考核指标配置
     *
     * @param zygzKhZbPz
     * @return
     */
    public int insertZygzKhZbpz(ZygzKhZbPz zygzKhZbPz);

    /**
     * 删除专业工作考核指标配置
     *
     * @param ids
     * @return
     */
    public int deleteZygzKhZbpz(@Param("ids") int[] ids);

    /**
     * 查询归口部门和对应的单位
     *
     * @param year
     * @return
     */
    public List<GkbmAndDwsDto> selectGkbmAndDws(int year);

    /**
     * 插入专业工作考核评价指标
     *
     * @param zygzKhZbList
     * @return
     */
    public int insertZygzkhZb(@Param("zygzKhZbList") List<ZygzKhZb> zygzKhZbList);

    /**
     * 插入专业工作考核关系表
     *
     * @param zygzKhGxList
     * @return
     */
    public int insertZygzkhGx(@Param("zygzKhGxList") List<ZygzKhGx> zygzKhGxList);

    /**
     * 删除专业工作考核关系表
     *
     * @param year
     * @return
     */
    public int deleteZygzkhGx(int year);

    /**
     * 删除专业工作考核指标数据
     *
     * @param yera
     * @return
     */
    public int deleteZygzkhZb(int year);

}
