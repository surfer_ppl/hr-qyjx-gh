package com.sgcc.common.pojo;

/**
 * 公共代码实体类（T_JX_COMMON_CODE）
 * @Author Edward
 * @Date 2022/3/21
 */
public class CommonCode {
    private Integer id;
    private String code;
    private String codeName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }
}
