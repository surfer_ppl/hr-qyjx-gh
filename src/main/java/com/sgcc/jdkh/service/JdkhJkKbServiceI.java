package com.sgcc.jdkh.service;

import java.util.List;
import java.util.Map;

/**
 * 看板service
 * 
 * @author Edward
 * @date 2021年12月6日
 */
public interface JdkhJkKbServiceI {
    /**
     * 查询关键业绩看板数据
     * @param year
     * @param jd
     * @param dwid
     * @return
     */
    public List<Map<String, Object>> selectGjyjKbData(Integer year, Integer jd, Integer dwid);

    /**
     * 查询专业工作考核看板数据
     * @param year
     * @param jd
     * @param dwid
     * @return
     */
    public List<Map<String, Object>> selectZygzKbData(Integer year, Integer jd, Integer dwid);
    

    
    
}
