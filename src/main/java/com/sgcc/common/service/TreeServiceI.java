package com.sgcc.common.service;

import java.util.List;
import java.util.Map;

import com.sgcc.common.dto.ElmTreeObj;
import com.sgcc.common.dto.TreeObj;
import com.sgcc.common.pojo.RsYgJbxx;

/**
 * 树结构Service
 *
 * @author Edward
 * @date 2021年5月24日
 */
public interface TreeServiceI {

    /**
     * 查询单位树
     *
     * @param orgId
     * @return
     */
    public List<TreeObj> selectDwTree(Integer orgId);

    /**
     * 查询归口部门树
     *
     * @param orgId
     * @return
     */
    public List<TreeObj> selectGkbmTree(Integer orgId);

    /**
     * 查询ElementUi树结构根节点数据（懒加载）
     *
     * @param spOrgId
     * @return
     */
    public List<ElmTreeObj> selectElmZzRootTreeLazy(Integer spOrgId);

    /**
     * 查询ElementUi树结构子节点数据（懒加载）
     *
     * @param spOrgId
     * @return
     */
    public List<ElmTreeObj> selectElmChildTreeLazy(Integer spOrgId);


    /**
     * 查询ElementUi树结构子节点数据（懒加载，右侧树）
     *
     * @param spOrgId
     * @return
     */
    public List<ElmTreeObj> selectElmChildTreeLazy_Right(Integer spOrgId, String spOrgCode, Integer level);


    /**
     * 查询饿了么根节点（单位类型）
     *
     * @param year
     * @return
     */
    public List<Map<String, Object>> selectElmRootNode_Dwlx(Integer year);

    /**
     * 查询饿了么子节点（单位）
     *
     * @param year
     * @param dwlxId
     * @return
     */
    public List<Map<String, Object>> selectElmChildNode_Dw(Integer year, Integer dwlxId);


    /**
     * 根据组织树查询对应的人
     *
     * @param zzId
     * @param xm
     * @return
     */
    public List<RsYgJbxx> selectRsYgJbxx(Integer zzId, String xm);
}
