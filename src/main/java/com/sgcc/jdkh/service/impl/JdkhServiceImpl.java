package com.sgcc.jdkh.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sgcc.dwlxwh.mapper.DwlxWhMapper;
import com.sgcc.dwlxwh.pojo.Dwlx;
import com.sgcc.jdkh.dto.AqgzDbDfDto;
import com.sgcc.jdkh.pojo.DflPz;
import com.sgcc.jdkh.pojo.ZsDf;
import com.sgcc.utils.ZsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.jdkh.dto.GjyjDbDfDto;
import com.sgcc.jdkh.dto.ZygzDbDfDto;
import com.sgcc.jdkh.mapper.JdkhMapper;
import com.sgcc.jdkh.service.JdkhServiceI;

/**
 * 
 * @author Edward
 * @date 2021/08/09
 */
@Service
public class JdkhServiceImpl implements JdkhServiceI {

    @Autowired
    private JdkhMapper jdkhMapper;

    @Autowired
    private DwlxWhMapper dwlxWhMapper;

    /**
     * 获取季度考核首页或者获取待办打分页面数据
     */
    @Override
    public List<?> selectJdKhOrDbDfDataGrid(Map<String, String> params) {
        // 获取指标类型，判断查询哪个单位(0:关键业绩指标，1:,2:安全工作考核)
        String zbLx = params.get("zbLx");

        List<?> resultList = null;

        if ("0".equals(zbLx)) {
            List<GjyjDbDfDto> gjyjJdkhList = jdkhMapper.selectGjyjJdkh(params);

            resultList = gjyjJdkhList;

        } else if ("1".equals(zbLx)) {
            List<ZygzDbDfDto> zygzJdkhList = jdkhMapper.selectZygzJdkh(params);

            resultList = zygzJdkhList;
        } else if ("2".equals(zbLx)) {
            List<AqgzDbDfDto> aqgzJdkhList = jdkhMapper.selectAqgzJdkh(params);

            resultList = aqgzJdkhList;
        }

        return resultList;

    }

    /**
     * 生成考核待办
     */
    @Override
    public void createKhDb(Integer year, Integer jd) {

        // 删除季度考核待办部门
        jdkhMapper.deleteJdkhDb(year, jd);

        // 删除折算数据
        jdkhMapper.deleteZsDf(year, jd, null);

        // 初始化季度考核待办
        jdkhMapper.insertJdkhDb(year, jd);
        // 初始化化季度考核打分表打分字段
        jdkhMapper.updateInitJdkhDf(year, jd);
        // 初始化折算表
        jdkhMapper.insertInitZsDf(year, jd);

    }

    /**
     * 季度考核折算维护(更新总得分)
     * 
     * @param year 年份
     * @param jd   季度
     * @param gkbmId 归口部门ID
     */
    public void onJdKhZsWh(Integer year, Integer jd, Integer gkbmId) {
        jdkhMapper.whZdf(year, jd, gkbmId);
    }

    /**
     * 开始季度考核折算
     *
     * @param year
     * @param jd
     */
    public void onJdKhZs(Integer year, Integer jd, Integer gkbmId) {
        // 先进行折算数据维护
        jdkhMapper.whZdf(year, jd, gkbmId);

        // 需要使用折算公式进行折算的单位类型名称
        String dwlxMc = "供电公司类";
        // 需要使用折算公式进行折算的单位类型ID
        int dwlxId = 0;
        List<Dwlx> dwlxList = dwlxWhMapper.selectDwlx(year);
        for (Dwlx dwlx : dwlxList) {
            if (dwlxMc.equals(dwlx.getDwlxMc())) {
                dwlxId = dwlx.getId();
            }
        }

        // 查询得分率上下限
        List<DflPz> dflPzList = jdkhMapper.selectDfl(year, jd);

        DflPz dflPz = dflPzList.get(0);
        // 得分率上限
        double dflSx = dflPz.getDflSx();
        // 得分率下限
        double dflXx = dflPz.getDflXx();

        // 查询最高分和最低分,并进行Map分装处理
        Map<Integer, Double> zgfMap = new HashMap<>();
        Map<Integer, Double> zdfMap = new HashMap<>();
        List<Map<String, Object>> zgfAndZdfList = jdkhMapper.selectZgfAndZdf(year, jd, gkbmId, dwlxMc);

        for (Map<String, Object> zgfZdfObjMap : zgfAndZdfList) {
            Integer gkbmId_1 = Integer.parseInt(zgfZdfObjMap.get("GKBMID").toString());
            Double zgf = Double.parseDouble(zgfZdfObjMap.get("ZGF").toString());
            Double zdf = Double.parseDouble(zgfZdfObjMap.get("ZDF").toString());

            zgfMap.put(gkbmId_1, zgf);
            zdfMap.put(gkbmId_1, zdf);
        }

        // 定义需要更新的数据集合
        List<ZsDf> updateDataList = new LinkedList<ZsDf>();

        // 查询需要折算的数据
        List<ZsDf> zsDataList = jdkhMapper.selectZsData(year, jd, gkbmId);
        for (ZsDf zsdfObj : zsDataList) {

            double zsdf = 0D;

            int zsDwlxId = zsdfObj.getDwlxId();
            // 判断是否需要公式折算
            if (dwlxId == zsDwlxId) {
                int gkbmid = zsdfObj.getGkbmId();
                double zgf = zgfMap.get(gkbmid);
                double zdf = zdfMap.get(gkbmid);
                double khfzH = zsdfObj.getKhfzZf();
                double dfh = zsdfObj.getZdf();

                zsdf = ZsUtil.jdKhDfZs(dflSx, dflXx, zgf, zdf, khfzH, dfh);
            } else {
                double dfh = zsdfObj.getZdf();
                zsdf = dfh;
            }

            zsdfObj.setZsf(zsdf);

            updateDataList.add(zsdfObj);

        }
        // 跟新数据
        jdkhMapper.updateZsDf(updateDataList);

    }

}
