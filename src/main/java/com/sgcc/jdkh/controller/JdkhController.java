package com.sgcc.jdkh.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.jdkh.dto.GjyjDbDfDto;
import com.sgcc.jdkh.dto.ZygzDbDfDto;
import com.sgcc.jdkh.service.JdkhServiceI;
import com.sgcc.utils.LayUiTableReponseMapUtil;

/**
 * 季度考核流程
 * 
 * @author Edward
 *
 */
@RestController
@RequestMapping("/jdkh")
public class JdkhController {

    @Autowired
    private JdkhServiceI jdkhService;

    /**
     * 季度考核页面查询或季度考核待办打分页面查询（layUI）
     * 
     * @param params
     * @param page
     * @param limit
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/getJdKhOrDbDfDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getJdKhOrDbDfDataGrid(@RequestParam Map<String, String> params, Integer page,
        Integer limit) {

        Map<String, Object> resultMap = null;

        PageHelper.startPage(page, limit);

        // 获取指标类型，判断查询哪个单位(0:关键业绩指标，1:,2:安全工作考核)
        String zbLx = params.get("zbLx");
        long total;
        if ("0".equals(zbLx)) {
            List<GjyjDbDfDto> resultList = (List<GjyjDbDfDto>)jdkhService.selectJdKhOrDbDfDataGrid(params);
            PageInfo<GjyjDbDfDto> pageInfo = new PageInfo<GjyjDbDfDto>(resultList);
            total = pageInfo.getTotal();
            resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, total);
        } else if ("1".equals(zbLx)) {
            List<ZygzDbDfDto> resultList = (List<ZygzDbDfDto>)jdkhService.selectJdKhOrDbDfDataGrid(params);
            PageInfo<ZygzDbDfDto> pageInfo = new PageInfo<ZygzDbDfDto>(resultList);
            total = pageInfo.getTotal();
            resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, total);
        } else if ("2".equals(zbLx)) {
            List<ZygzDbDfDto> resultList = (List<ZygzDbDfDto>)jdkhService.selectJdKhOrDbDfDataGrid(params);
            PageInfo<ZygzDbDfDto> pageInfo = new PageInfo<ZygzDbDfDto>(resultList);
            total = pageInfo.getTotal();
            resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(resultList, total);
        }

        return resultMap;

    }

    /**
     * 生成季度考核待办，把三大指标涉及到到的所有归口部门，发送待办弹出提示框
     * 
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/generateJdkhDb", method = RequestMethod.POST)
    public Map<String, String> generateJdkhDb(Integer year, Integer jd) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            jdkhService.createKhDb(year, jd);
            msgMap.put("flag", "true");
            msgMap.put("msg", "生成考核待办成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "生成考核待办失败,请重试！");
            return msgMap;
        }

    }

    /**
     * 发送考核待办，发给生成考核待办中存入的归口部门
     * 
     * @param year
     * @param jd
     * @return
     */
    public Map<String, String> publishJdkhDb(Integer year, Integer jd) {
        Map<String, String> msgMap = new HashMap<>();
        return msgMap;
    }

    /**
     * 折算数据维护（暂时前台页面不直接调用）
     * 
     * @param year
     * @param jd
     * @return
     */
    /* @RequestMapping(value = "/onjdKhDfZsWh", method = RequestMethod.POST)
    public Map<String, String> onjdKhDfZsWh(Integer year, Integer jd, Integer gkbmId) {
        Map<String, String> msgMap = new HashMap<>();
    
        try {
            jdkhService.onJdKhZsWh(year, jd, gkbmId);
            msgMap.put("flag", "true");
            msgMap.put("msg", "维护成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "维护失败,请重试！");
            return msgMap;
        }
    
    }*/

    /**
     * 折算季度考核得分
     * 
     * @param year
     * @param jd
     * @return
     */
    @RequestMapping(value = "/onjdKhDfZs", method = RequestMethod.POST)
    public Map<String, String> onjdKhDfZs(Integer year, Integer jd, Integer gkbmId) {
        Map<String, String> msgMap = new HashMap<>();

        try {
            jdkhService.onJdKhZs(year, jd, gkbmId);
            msgMap.put("flag", "true");
            msgMap.put("msg", "折算成功");
            return msgMap;
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "false");
            msgMap.put("msg", "折算失败,请重试！");
            return msgMap;
        }

    }

    /**
     * 按照归口部门折算季度考核得分
     * 
     * @param year
     * @param jd
     * @return
     */
    public Map<String, String> onjdKhDfZsByGkbm(Integer year, Integer jd, Integer gkbmId) {
        Map<String, String> msgMap = new HashMap<String, String>();
        return msgMap;
    }
}
