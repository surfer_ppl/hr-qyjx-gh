package com.sgcc.jdkh.service;

import java.util.List;
import java.util.Map;

public interface JdkhServiceI {

    /**
     * 获取季度考核首页或者获取待办打分页面数据
     * 
     * @param params
     * @return
     */
    public List<?> selectJdKhOrDbDfDataGrid(Map<String, String> params);

    /**
     * 生成考核待办
     * 
     * @param year
     * @param jd
     */
    public void createKhDb(Integer year, Integer jd);

    /**
     * 季度考核折算维护
     * 
     * @param year
     * @param jd
     * @param gkbmId
     */
    public void onJdKhZsWh(Integer year, Integer jd, Integer gkbmId);

    /**
     * 开始季度考核折算
     * 
     * @param year
     * @param jd
     */
    public void onJdKhZs(Integer year, Integer jd, Integer gkbmId);
}
