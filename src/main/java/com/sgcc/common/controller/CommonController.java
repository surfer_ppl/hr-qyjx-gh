package com.sgcc.common.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgcc.common.pojo.CommonCode;
import com.sgcc.common.service.CommonCodeServiceI;
import com.sgcc.constval.ConstVal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgcc.common.dto.ElmTreeObj;
import com.sgcc.common.dto.TreeObj;
import com.sgcc.common.pojo.RsYgJbxx;
import com.sgcc.common.service.TreeServiceI;
import com.sgcc.common.service.YgXxServiceI;
import com.sgcc.common.pojo.Zzdy;

@RestController
// @CrossOrigin
@RequestMapping("/common")
public class CommonController {

    @Autowired
    private TreeServiceI treeService;

    @Autowired
    private YgXxServiceI ygXxService;

    @Autowired
    private CommonCodeServiceI commonCodeService;

    /**
     * 根据登录人所属单位ID查询单位结构树
     *
     * @param orgId 组织角色所在单位或部门ID
     * @return
     */
    @RequestMapping(value = "/getDwTree", method = RequestMethod.POST)
    public List<TreeObj> getDwTree(Integer orgId) {

        List<TreeObj> dwTreeList = treeService.selectDwTree(orgId);

        return dwTreeList;
    }

    /**
     * 获取归口部门树结构
     *
     * @param orgId
     * @return
     */
    @RequestMapping(value = "/getGkbmTree", method = RequestMethod.POST)
    public List<TreeObj> getGkbmTree(Integer orgId) {
        List<TreeObj> gkbmTreeList = treeService.selectGkbmTree(orgId);

        return gkbmTreeList;
    }

    /**
     * 获取树结构子节点
     *
     * @param parentId
     * @return
     */
    @RequestMapping(value = "/getChildNode", method = RequestMethod.POST)
    public List<Zzdy> getChildNode(Integer parentId) {
        List<Zzdy> childNodes = null;
        return childNodes;
    }

    /**
     * 获取饿了么根节点
     *
     * @param spOrgId
     * @return
     */
    @RequestMapping(value = "/getEleRootTreeNode", method = RequestMethod.POST)
    public List<ElmTreeObj> getEleRootTreeNode(Integer spOrgId) {
        List<ElmTreeObj> rootTerrNode = treeService.selectElmZzRootTreeLazy(spOrgId);
        return rootTerrNode;

    }

    /**
     * 获取饿了么根节点(右侧)
     *
     * @param spOrgId
     * @return
     */
    @RequestMapping(value = "/getEleRootTreeNode_Right", method = RequestMethod.POST)
    public List<ElmTreeObj> getEleRootTreeNode_Right(Integer spOrgId) {
        List<ElmTreeObj> rootTerrNode = treeService.selectElmZzRootTreeLazy(spOrgId);
        return rootTerrNode;

    }

    /**
     * 获取饿了么子节点
     *
     * @param parentId
     * @return
     */
    @RequestMapping(value = "/getEleChildTreeNode", method = RequestMethod.POST)
    public Map<String, List<?>> getEleChildTreeNode(Integer parentId) {
        Map<String, List<?>> treeMap = new HashMap<>();
        List<ElmTreeObj> childTreeNodes = treeService.selectElmChildTreeLazy(parentId);

        List<Integer> selectedNodes = new ArrayList<>();
        for (ElmTreeObj elmTreeObj : childTreeNodes) {
            Boolean disabled = elmTreeObj.getDisabled();
            if (disabled) {
                selectedNodes.add(elmTreeObj.getId());
            }
        }
        treeMap.put("childTreeNodes", childTreeNodes);
        treeMap.put("selectedNodes", selectedNodes);

        return treeMap;
    }

    /**
     * 获取饿了么子节点(右侧树)
     *
     * @param parentId
     * @param spOrgCode
     * @param level
     * @return
     */
    @RequestMapping(value = "/getEleChildTreeNode_Right", method = RequestMethod.POST)
    public Map<String, List<?>> getEleChildTreeNode_Right(Integer parentId, String spOrgCode, Integer level) {
        Map<String, List<?>> treeMap = new HashMap<>();
        List<ElmTreeObj> childTreeNodes = treeService.selectElmChildTreeLazy_Right(parentId, spOrgCode, level);

        treeMap.put("childTreeNodes", childTreeNodes);

        return treeMap;
    }

    /**
     * 获取饿了么根节点(单位类型)
     *
     * @param year
     * @return
     */
    @RequestMapping(value = "/getEleRootTreeNode_Dwlx", method = RequestMethod.POST)
    public List<Map<String, Object>> getEleRootTreeNode_Dwlx(Integer year) {
        List<Map<String, Object>> rootTerrNode = treeService.selectElmRootNode_Dwlx(year);
        return rootTerrNode;

    }

    /**
     * 获取饿了么子节点(单位类型)
     *
     * @param year
     * @param dwlxId
     * @return
     */
    @RequestMapping(value = "/getEleChildTreeNode_Dw", method = RequestMethod.POST)
    public List<Map<String, Object>> getEleChildTreeNode_Dw(Integer year, Integer dwlxId) {
        List<Map<String, Object>> childTerrNode = treeService.selectElmChildNode_Dw(year, dwlxId);
        return childTerrNode;
    }

    /**
     * 查询员工基本信息
     *
     * @param zzId
     * @param xm
     * @return
     */
    @RequestMapping(value = "/queryRsYgJbxx", method = RequestMethod.POST)
    public Map<String, Object> queryRsYgJbxx(Integer zzId, String xm, Integer page,
                                             Integer limit) {
        Map<String, Object> resultMap = new HashMap<>();
        PageHelper.startPage(page, limit);
        List<RsYgJbxx> rsYgJbxxs = treeService.selectRsYgJbxx(zzId, xm);
        PageInfo<RsYgJbxx> pageInfo = new PageInfo<RsYgJbxx>(rsYgJbxxs);
        long total = pageInfo.getTotal();
        resultMap.put("result", rsYgJbxxs);
        resultMap.put("total", total);
        return resultMap;
    }

    /**
     * 获取当前登录人的角色信息
     *
     * @return
     */
    @RequestMapping(value = "/getRoleXx", method = RequestMethod.POST)
    public Map<String, Object> getRoleXx() {
        Map<String, Object> roleMap = new HashMap<>();

        // 从session中获取登录人的员工ID和姓名
        Integer loginerId = 1056828;
        String loginerXmString = "易词石";

        List<Map<String, String>> roleList = ygXxService.selectRoleXx(loginerId);

        roleMap.put("loginerId", loginerId);
        roleMap.put("loginerXm", loginerXmString);
        roleMap.put("loginerRole", roleList);

        return roleMap;
    }

    /**
     * 查询公共代码
     *
     * @return
     */
    @RequestMapping(value = "/queryCommonCode", method = RequestMethod.POST)
    public List<CommonCode> queryCommonCode(String codeTypeBm) {
        List<CommonCode> commonCodes = commonCodeService.selectCommonCode(codeTypeBm);
        return commonCodes;
    }

}
