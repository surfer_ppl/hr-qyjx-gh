package com.sgcc.common.service;

import com.sgcc.common.dto.Loginer;

import java.util.List;
import java.util.Map;

/**
 * 登录服务
 *
 * @author Edward
 * @date 2021年10月25日
 */
public interface LoginServiceI {

    /**
     * 查询登录人
     *
     * @param account  账号
     * @param passWrod 密码
     * @return
     */
    public List<Loginer> selectLoginer(String account, String passWrod);

    /**
     * 查询待办信息
     *
     * @return
     */
    public List<Map<String, Object>> selectDbXx();
}
