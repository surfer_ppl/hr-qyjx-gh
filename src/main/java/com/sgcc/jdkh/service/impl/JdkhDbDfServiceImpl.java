package com.sgcc.jdkh.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sgcc.jdkh.mapper.JdkhDbDfMapper;
import com.sgcc.jdkh.pojo.AqgzJdDfb;
import com.sgcc.jdkh.pojo.GjyjJdDfb;
import com.sgcc.jdkh.pojo.ZygzJdDfb;
import com.sgcc.jdkh.service.JdkhDbDfServiceI;

/**
 * 季度考核待办打分
 * 
 * @author Edward
 * @date 2021/08/09
 */
@Service
public class JdkhDbDfServiceImpl implements JdkhDbDfServiceI {

    @Autowired
    JdkhDbDfMapper jdkhDbDfMapper;

    /**
     * 保存季度考核打分——关键业绩
     */
    @Override
    public void saveGjyjDbDf(JSONArray dbDfjSj) {

        List<GjyjJdDfb> gjyjJdDfblList = new LinkedList<>();

        for (int i = 0; i < dbDfjSj.size(); i++) {
            JSONObject dfSjObj = dbDfjSj.getJSONObject(i);
            GjyjJdDfb gjyjJdDfb = JSON.parseObject(dfSjObj.toString(), GjyjJdDfb.class);
            gjyjJdDfblList.add(gjyjJdDfb);
        }

        // 不是空的再执行
        if (!gjyjJdDfblList.isEmpty()) {
            jdkhDbDfMapper.updateGjyjJdDf(gjyjJdDfblList);
        }

    }
    
    /**
     * 保存季度考核打分——专业工作考核
     */
    @Override
    public void saveZygzDbDf(JSONArray dbDfjSj) {

        List<ZygzJdDfb> zygzJdDfblList = new LinkedList<>();

        for (int i = 0; i < dbDfjSj.size(); i++) {
            JSONObject dfSjObj = dbDfjSj.getJSONObject(i);
            ZygzJdDfb zygzJdDfb = JSON.parseObject(dfSjObj.toString(), ZygzJdDfb.class);
            zygzJdDfblList.add(zygzJdDfb);
        }
        // 不是空的再执行
        if (!zygzJdDfblList.isEmpty()) {
            jdkhDbDfMapper.updateZygzJdDf(zygzJdDfblList);
        }
    }
    
    /**
     * 保存季度考核打分——安全工作考核
     */
    @Override
    public void saveAqgzDbDf(JSONArray dbDfjSj) {

        List<AqgzJdDfb> aqgzJdDfblList = new LinkedList<>();

        for (int i = 0; i < dbDfjSj.size(); i++) {
            JSONObject dfSjObj = dbDfjSj.getJSONObject(i);
            AqgzJdDfb aqgzJdDfb = JSON.parseObject(dfSjObj.toString(), AqgzJdDfb.class);
            aqgzJdDfblList.add(aqgzJdDfb);
        }
        // 不是空的再执行
        if (!aqgzJdDfblList.isEmpty()) {
            jdkhDbDfMapper.updateAqgzJdDf(aqgzJdDfblList);
        }
    }

}
