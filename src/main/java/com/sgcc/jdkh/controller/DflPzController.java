package com.sgcc.jdkh.controller;

import java.util.List;

/**
 * 得分率配置
 * 
 * @author Edward
 *
 */

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sgcc.jdkh.pojo.DflPz;
import com.sgcc.jdkh.service.DflPzServiceI;
import com.sgcc.utils.LayUiTableReponseMapUtil;

@RestController
@RequestMapping("/dflPz")
public class DflPzController {

    @Autowired
    DflPzServiceI dflPzService;

    /**
     * 查询得分率配置
     * 
     * @param jdOrNd
     * @param year
     * @return
     */
    @RequestMapping(value = "/getDflPzDataGrid", method = RequestMethod.POST)
    public Map<String, Object> getDflPzDataGrid(Integer jdOrNd, Integer year, Integer dwlxId) {
        List<DflPz> dflPzs = dflPzService.selectDflPzs(jdOrNd, year, dwlxId);

        Map<String, Object> resultMap = LayUiTableReponseMapUtil.getLyUITableReonseMap(dflPzs, null);

        return resultMap;
    }

    /**
     * 新增得分率配置
     * 
     * @param paramsMap
     * @return
     */
    @RequestMapping(value = "/addDflPz", method = RequestMethod.POST)
    public Map<String, String> addDflPz(@RequestParam Map<String, Object> paramsMap) {
        Map<String, String> msgMap = new HashedMap<>();

        String year = (String)paramsMap.get("year");
        paramsMap.put("year", Integer.parseInt(year));
        String jdOrNd = (String)paramsMap.get("jdOrNd");

        String jd = (String)paramsMap.get("jd");
        paramsMap.put("jd", Integer.parseInt(jd));
        if ("1".equals(jdOrNd)) {
            paramsMap.put("jd", 5);
        }
        String dwlxId = (String)paramsMap.get("dwlxid");
        paramsMap.put("dwlxId", Integer.parseInt(dwlxId));
        String dflSx = (String)paramsMap.get("dflSx");
        paramsMap.put("dflSx", Double.parseDouble(dflSx));
        String dflXx = (String)paramsMap.get("dflXx");
        paramsMap.put("dflXx", Double.parseDouble(dflXx));

        try {
            dflPzService.insertDflPz(paramsMap);
            msgMap.put("flag", "true");
            msgMap.put("msg", "保存成功");
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "true");
            msgMap.put("msg", "保存失败");
            return msgMap;
        }

        return msgMap;
    }

    /**
     * 新增得分率配置
     * 
     * @param paramsMap
     * @return
     */
    @RequestMapping(value = "/removeDflPz", method = RequestMethod.POST)
    public Map<String, String> removeDflPz(Integer[] idsArr) {
        Map<String, String> msgMap = new HashedMap<>();

        try {
            dflPzService.deleteDflPz(idsArr);
            msgMap.put("flag", "true");
            msgMap.put("msg", "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            msgMap.put("flag", "true");
            msgMap.put("msg", "删除失败");
            return msgMap;
        }

        return msgMap;
    }
}
