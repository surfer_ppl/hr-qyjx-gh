package com.sgcc.common.service;

import java.util.List;
import java.util.Map;

/**
 * 
 * 员工信息相关
 * @author Edward
 * @date 2021年9月21日
 */
public interface YgXxServiceI {
    
    /**
     * 获取登录人角色信息
     * @param loginerId
     * @return
     */
    public List<Map<String, String>> selectRoleXx(Integer loginerId); 
}
