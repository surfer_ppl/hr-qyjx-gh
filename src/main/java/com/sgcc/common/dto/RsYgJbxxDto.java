package com.sgcc.common.dto;

/**
 * 员工基本信息Dto
 *
 * @Author Edward
 * @Date 2022/3/28
 */
public class RsYgJbxxDto {

    // id
    private Integer id;
    // 姓名
    private String xm;
    // 单位ID
    private Integer dwId;
    //单位名称
    private String dwMc;
    // 部门ID
    private Integer bmId;
    // 部门名称
    private String bmMc;
    // 岗位名称
    private String gwMc;
    // 员工性质
    private String ygXz;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public Integer getDwId() {
        return dwId;
    }

    public void setDwId(Integer dwId) {
        this.dwId = dwId;
    }

    public String getDwMc() {
        return dwMc;
    }

    public void setDwMc(String dwMc) {
        this.dwMc = dwMc;
    }

    public Integer getBmId() {
        return bmId;
    }

    public void setBmId(Integer bmId) {
        this.bmId = bmId;
    }

    public String getBmMc() {
        return bmMc;
    }

    public void setBmMc(String bmMc) {
        this.bmMc = bmMc;
    }

    public String getGwMc() {
        return gwMc;
    }

    public void setGwMc(String gwMc) {
        this.gwMc = gwMc;
    }

    public String getYgXz() {
        return ygXz;
    }

    public void setYgXz(String ygXz) {
        this.ygXz = ygXz;
    }
}
